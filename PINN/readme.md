The following markdown briefly explains the user how to run "PINN_Laplace_AMG.py", which solves using PINN the PDE
uxx + uyy = 0 on a square shaped domain subject to some specific boundary conditions. 
PINN is trained using a double optimization process, first with the ADAM Optimizer. Next, with LBFGS.

Inputs:
	
	-d --data : Selects the file containing the ground truth for the square domain wished to solve. The format of the file must be ".h5" and have the following attributes: 
		'nSample' = 1
		'domainSize' = 32*Width + 2
		'LaplaceSolution' = "True Values" 
	Note that samples satisfying this structure can be easily generated with the file "../data_AMG/data_AMG.py".

	-l --arch : Set of integers that define the architecture of the model. They must correspond to the specified model from the first input.

	-c --collocation : Amount of collocation points to be found in the domain of 1x1. The bigger the domain the more collocation points there are going to be. 

	-nIter --nIter : Number of Epochs performed by the ADAM Optimizer

	-DataType --DataType : Selection of the data type "float32" or "float64".

	-print --print : Bool that prints the results to a file if true.

	-reproduce --reproduce : Reproduces all the comparisons in the paper


Requirements:

	-This code uses TensorFlow 1.

Reproducing the results:
	
	To reproduce the results for the PINN network shown in the paper, one must run the following line of code:

	python PINN_Laplace_AMG.py -reproduce


Running the code:

	Running the code with some other parameters is an easy task. The following line demonstrates how to run it specifying the default options:

	python PINN_Laplace_AMG.py -d "../data_AMG/Sin_BC_AMG_64.h5" -l 2 60 60 60 60 1 -DataType "float32" -c 20000 -nIter 40000 -print True

	The outputs will be the MAE and Time information. If plots == True, then a file names "PINN_Tests.txt" is going to be generated. This file will contain all the results. 

The following markdown briefly explains the user how to run "XPINN_Laplace.py", which solves using XPINN the PDE
uxx + uyy = 0 on a square domain subject to some specific boundary conditions. 

Inputs:

	-d --data : Selects the file containing the ground truth for the square domain wished to solve. The format of the file must be ".h5" and have the following attributes: 
		'nSample' = 1
		'domainSize' = 32*Width + 2
		'LaplaceSolution' = "True Values" 
	Note that samples satisfying this structure can be easily generated with the file "../data_AMG/data_AMG.py".
	Currently the XPINN can only analyze the data file: "../data_AMG/Sin_BC_AMG_128.h5"

	-l --arch : Set of integers that define the architecture of the model. They must correspond to the specified model from the first input.

	-N_ub --N_Boundary : Number of points to be found on each side of the boundaries for a subdomain.

	-N_f --N_Collocation : Number of collocation points inside each subdomain.

	-N_I --N_Intersection : Number of points found in the intersecting borders of each subdomain.

	-nIter --nIter : Number of iterations of the ADAM Optimizer

	-DataType --DataType : Selection of the data type "float32" or "float64". It must match the data type of the model. 

	-reproduce --reproduce : Reproduces all the comparisons in the paper

Requirements:

	-This code uses TensorFlow 1.

Reproducing the results:
	
	To reproduce the results for the single cavity shown in the paper, one must run the following line of code:

	python XPINN_Laplace.py -reproduce


Running the code:

	Running the code with some other parameters is an easy task. The following line demonstrates how to run it specifying the default options:

	python XPINN_Laplace.py -d "../data_AMG/Sin_BC_AMG_128.h5" -l 2 20 20 20 20 1 -DataType "float32" -N_ub 64 -N_f 4000 -N_I 64 -nIter 5000 

	The output will generate a file with the MAE and Time information named "X_PINN_128.txt"The following markdown briefly explains the user how to run "NS.py", which solves using PINN the NS PDE on a square shaped cavity with a top moving lid. 
PINN is trained using the ADAM Optimizer.

Inputs:
	
	-d --data : Selects the file containing the ground truth for the square domain wished to solve. The format of the file must be ".vtk", contain the fields 'p' and 'U' and the points where these fields are recorded. Note that the python script automatically coarses the grid by a factor of two, so the original data must have (129,129) samples uniformly distributed on [0,1]x[0,1].

	-l --arch : Set of integers that define the architecture of the model.

	-c --collocation : Amount of collocation points to be found in the domain of 1x1. The bigger the domain the more collocation points there are going to be. 

	-nIter --nIter : Number of Epochs performed by the ADAM Optimizer

	-DataType --DataType : Selection of the data type "float32" or "float64".

	-print --print : Bool that prints the results to a file if true.

	-plot --plot : Bool that plots the results with streamline and magnitude if true. 

	-reproduce --reproduce : Reproduces all the comparisons in the paper

Requirements:

	-This code uses TensorFlow 1.

Reproducing the results:
	
	To reproduce the results for the PINN network shown in the paper, one must run the following line of code:

	python NS.py -reproduce


Running the code:
 
	Running the code with some other parameters is an easy task. The following line demonstrates how to run it specifying the default options:

	python NS.py -d "../data_NS/single_cavity/U_2003_960.vtk" -l 2 50 50 50 50 50 2 -DataType "float32" -c 20000 -nIter 40000 -print True -plot True

	The outputs will be the MAE and Time information. If plots == True and print == True, then a a folder will be created with the name "../res_PINN_U_2003_960/". Inside this folder three files can be found: 
	- 'NS_Velocity_Magnitude_single_cavity_U_2003_960.pdf'
	- 'NS_Streamlines_single_cavity_U_2003_960.pdf'
	- 'Test_PINN_NS_single_cavity.txt'