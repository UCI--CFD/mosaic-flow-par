#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 09:56:01 2021

@author: robertplanas
"""
GPU = None
import sys
if(GPU is not None):
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = GPU
    

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-d', '--data', default="../Laplace/data/Sin_BC_AMG_64.h5",
                    help='Select a data file')
parser.add_argument('-l', '--arch', type=int, nargs='*', help='architecture',
                    default = [2, 60,60,60,60, 1 ])

parser.add_argument('-c', '--collocation', type=int, default=20000,
                    help ='Amount of collocation points in the domain 1x1')

parser.add_argument('-nIter', '--nIter', type=int, default=40000,
                    help ='Epochs of the ADAM optimizer')
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')
parser.add_argument('-print', '--print', default=True,
                    help='Print to a file if true')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')

args = parser.parse_args()


if(args.reproduce):
    args.collocation = 20000
    args.arch = [2, 60,60,60,60, 1 ]
    args.nIter = 40000
    args.DataType = "float32"
    args.print = True
    
    Data = ["../Laplace/data/Sin_BC_AMG_32.h5",
        "../Laplace/data/Sin_BC_AMG_64.h5",
        "../Laplace/data/Sin_BC_AMG_96.h5",
        "../Laplace/data/Sin_BC_AMG_128.h5",
        "../Laplace/data/Sin_BC_AMG_160.h5",
        "../Laplace/data/Sin_BC_AMG_192.h5",
        "../Laplace/data/Sin_BC_Period_1_AMG_32.h5",
        "../Laplace/data/Sin_BC_Period_1_AMG_64.h5",
        "../Laplace/data/Sin_BC_Period_1_AMG_96.h5",
        "../Laplace/data/Sin_BC_Period_1_AMG_128.h5",
        "../Laplace/data/Sin_BC_Period_1_AMG_192.h5"]
else:
    Data = [args.data]

   
    

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from scipy.interpolate import griddata
from pyDOE import lhs
import time
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import h5py as h5

# =============================================================================
# PINN Codes
# =============================================================================


class PhysicsInformedNN:
    # Initialize the class
    def __init__(self, X_u, u, X_f, layers, lb, ub):

        self.lb = lb
        self.ub = ub

        self.x_u = X_u[:, 0:1]
        self.y_u = X_u[:, 1:2]

        self.x_f = X_f[:, 0:1]
        self.y_f = X_f[:, 1:2]

        self.u = u

        self.layers = layers

        # Initialize NNs
        self.weights, self.biases = self.initialize_NN(layers)

        # tf placeholders and graph
        self.sess = tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(allow_soft_placement=True,
                                                     log_device_placement=True))

        self.x_u_tf = tf.compat.v1.placeholder(
            tf.float32, shape=[None, self.x_u.shape[1]])
        self.y_u_tf = tf.compat.v1.placeholder(
            tf.float32, shape=[None, self.y_u.shape[1]])
        self.u_tf = tf.compat.v1.placeholder(tf.float32, shape=[None, self.u.shape[1]])

        self.x_f_tf = tf.compat.v1.placeholder(
            tf.float32, shape=[None, self.x_f.shape[1]])
        self.y_f_tf = tf.compat.v1.placeholder(
            tf.float32, shape=[None, self.y_f.shape[1]])

        self.u_pred = self.net_u(self.x_u_tf, self.y_u_tf)
        self.f_pred = self.net_f(self.x_f_tf, self.y_f_tf)

        self.loss = tf.reduce_mean(tf.square(self.u_tf - self.u_pred)) + \
            tf.reduce_mean(tf.square(self.f_pred))

        
        self.optimizer_refine = tf.contrib.opt.ScipyOptimizerInterface(self.loss,
                                                                method='L-BFGS-B',
                                                                options={'maxiter': 3000000,
                                                                         'maxfun': 3000000,
                                                                         'maxcor': 100,
                                                                         'maxls': 100,
                                                                         'ftol': 1.0 * np.finfo(float).eps*1e-4})
        
        self.optimizer= tf.train.AdamOptimizer()
        self.train_op_Adam = self.optimizer.minimize(self.loss) 
        init = tf.global_variables_initializer()
        self.sess.run(init)

    def initialize_NN(self, layers):
        weights = []
        biases = []
        num_layers = len(layers)
        for l in range(0, num_layers - 1):
            W = self.xavier_init(size=[layers[l], layers[l + 1]])
            b = tf.Variable(
                tf.zeros([1, layers[l + 1]], dtype=tf.float32), dtype=tf.float32)
            weights.append(W)
            biases.append(b)
        return weights, biases

    def xavier_init(self, size):
        in_dim = size[0]
        out_dim = size[1]
        xavier_stddev = np.sqrt(2 / (in_dim + out_dim))
        return tf.Variable(tf.random.truncated_normal([in_dim, out_dim], stddev=xavier_stddev), dtype=tf.float32)

    def neural_net(self, X, weights, biases):
        num_layers = len(weights) + 1

        H = 2.0 * (X - self.lb) / (self.ub - self.lb) - 1.0
        for l in range(0, num_layers - 2):
            W = weights[l]
            b = biases[l]
            H = tf.tanh(tf.add(tf.matmul(H, W), b))
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y

    def net_u(self, x, y):
        u = self.neural_net(tf.concat([x, y], 1), self.weights, self.biases)
        return u

    def net_f(self, x, y):
        u = self.net_u(x, y)
        u_y = tf.gradients(u, y)[0]
        u_yy = tf.gradients(u_y, y)[0]
        u_x = tf.gradients(u, x)[0]
        u_xx = tf.gradients(u_x, x)[0]
        f = u_yy + u_xx

        return f

    def callback(self, loss):
        print('Loss:', loss)

    def train(self,nIter):

        tf_dict = {self.x_u_tf: self.x_u, self.y_u_tf: self.y_u, self.u_tf: self.u,
                   self.x_f_tf: self.x_f, self.y_f_tf: self.y_f}
        
        Loss_history =[]
        for it in range(nIter):
            self.sess.run(self.train_op_Adam, tf_dict)
            if it %20 == 0:
                #elapsed = time.time() - start_time
                loss_value = self.sess.run(self.loss, tf_dict)

                print('It: %d, Loss: %.3e' %
                    (it, loss_value))

                #start_time = time.time()
                Loss_history.append(loss_value)
        
        return Loss_history
                      
    def refine(self):
        tf_dict = {self.x_u_tf: self.x_u, self.y_u_tf: self.y_u, self.u_tf: self.u,
                   self.x_f_tf: self.x_f, self.y_f_tf: self.y_f}

        self.optimizer_refine.minimize(self.sess,
                                feed_dict=tf_dict,
                                fetches=[self.loss],
                                loss_callback=self.callback)

    def predict(self, X_star):

        u_star = self.sess.run(
            self.u_pred, {self.x_u_tf: X_star[:, 0:1], self.y_u_tf: X_star[:, 1:2]})
        f_star = self.sess.run(
            self.f_pred, {self.x_f_tf: X_star[:, 0:1], self.y_f_tf: X_star[:, 1:2]})

        return u_star, f_star
    
# =============================================================================
# Print File
# =============================================================================

def Print_vector_File(File_Name, Vector, Vector_Name):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt,"a") 
    file1.write(Vector_Name + " = [")
    for index,element in enumerate(Vector):
        if( index == len(Vector)-1):
            file1.write(str(element) + "]" + "\n")
        else:
            file1.write(str(element) + ',')
    file1.close()
    
def Print_String_File(File_Name, String):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt,"a") 
    file1.write(String + " \n")
    file1.close()
    

if __name__ == "__main__":
    
    # =============================================================================
    # Load data
    # =============================================================================
    
    TIMES = []
    MAEs = []
    Names = []
    
    for index, File in enumerate(Data): 
    
        psnFile = h5.File(File, 'r')
        nSample = psnFile.attrs['nSample']
        nCell = psnFile.attrs['domainSize'][0]
        psnData = psnFile['LaplaceSolution']
        print('# Samples: {}'.format(nSample))
        print('# Cells: {}*{}'.format(nCell, nCell))
        print('data shape: ', end='')
        print(psnData.shape)
        p = np.zeros(psnData.shape)
        p = psnData[:, :, :]
        pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
        print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))
        psnFile.close()
        
        Side = int((len(p[0,:,0]) - 2 )/32)
        Area = int(Side*Side)
        h = 1/32
        N_u = int(4*np.sqrt(Area)*32)
        N_f = int(Area*args.collocation)
        layers = args.arch
        SEED= 1
        nIter = args.nIter
            
        Width = int(np.sqrt(Area))
        Side = int(np.sqrt(Area))
        x = np.linspace(h/2, Side - h/2, num=32*Side)
        y = np.linspace(h/2, Side - h/2, num=32*Side)
        Exact = p[0,1:-1,1:-1].T
    
        X, Y = np.meshgrid(x, y)
    
        X_star = np.hstack((X.flatten()[:, None], Y.flatten()[:, None]))
        u_star = Exact.flatten()[:, None]
    
        # Doman bounds
        lb = X_star.min(0)
        ub = X_star.max(0)
        
        X0 = np.linspace(0,0,num = 32*Side)[:, None]
        Y0 = np.linspace(0,0,num = 32*Side)[:, None]
        X_W = np.linspace(Width,Width, num =32*Side)[:, None]
        Y_W = np.linspace(Width,Width, num =32*Side)[:, None]
        X_S = x[:,None]
        Y_S = y[:,None]
        
        xx1 = np.hstack((X0, Y_S))
        uu1 = np.sin(2*np.pi*Y_S/Width)
        
        xx2 = np.hstack((X_S, Y_W))
        uu2 = np.sin(2*np.pi*X_S/Width)
        
        xx3 = np.hstack((X_W, np.flip(Y_S)))
        uu3 = np.sin(2*np.pi*Y_S/Width)
        
        xx4 = np.hstack((np.flip(X_S), X0))
        uu4 = np.sin(2*np.pi*X_S/Width)
    
        X_u_train = np.vstack([xx1, xx2, xx3, xx4])
        X_f_train = lb + (ub - lb) * lhs(2, N_f)
        X_f_train = np.vstack((X_f_train, X_u_train))
        u_train = np.vstack([uu1, uu2, uu3, uu4])
    
        idx = np.random.choice(X_u_train.shape[0], N_u, replace=False)
        X_u_train = X_u_train[idx, :]
        u_train = u_train[idx, :]
    
        
    
        np.random.seed(1234 + SEED)
        tf.set_random_seed(1234 + SEED)
    
        model = PhysicsInformedNN(
            X_u_train, u_train, X_f_train, layers, lb, ub)
        start_time = time.time()
        Loss_history = model.train(nIter)
        model.refine()
        elapsed = time.time() - start_time
        print('Training time: %.4f' % (elapsed))
    
        u_pred, f_pred = model.predict(X_star)
    
        error_u = np.linalg.norm(u_star - u_pred, 2) / np.linalg.norm(u_star, 2)
        print('Relative Squarred Error u: %e' % (error_u))
    
        MAE = np.mean(np.abs(u_star - u_pred))
        print('MAE: %e' % (MAE))
    
        MSE = np.mean((u_star - u_pred)** 2)
        print('MSE: %e' % (MSE))
    
        U_pred = griddata(X_star, u_pred.flatten(), (X, Y), method='cubic')
        Error = np.abs(Exact - U_pred)
        
        Current = "Area = " + str(Area) + " Iter = " + str(nIter) + "Arch" + str(layers) + " Collocation = " +str(N_f) + ":------"
        
        TIMES.append(elapsed)
        MAEs.append(MAE)
        Names.append(Current)

        if(args.print):
            Current = "Area = " + str(Area) + " Iter = " + str(nIter) + "Arch" + str(layers) + " Collocation = " +str(N_f) + ":------"
            Print_String_File('PINN_Tests', Current)
            Print_vector_File('PINN_Tests', MAEs, "MAE")
            Print_vector_File('PINN_Tests', TIMES, "Times")
        

    for index, MAE in MAEs:
        print("-----", Names[index] , "------")
        print("MAE = ", MAE)
        print("Time = ", TIMES[index])
        

        
        
    
