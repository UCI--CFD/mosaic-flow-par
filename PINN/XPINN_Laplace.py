import sys
sys.path.insert(0, '../Utilities/')
import os 
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import tensorflow as tf
import numpy as np
#import pandas as pd
import matplotlib.pyplot as plt
import scipy.io
# from scipy.interpolate import griddata
# from pyDOE import lhs
# from mpl_toolkits.mplot3d import Axes3D
import time
import matplotlib.tri as tri
import matplotlib.gridspec as gridspec
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.patches import Polygon
import h5py as h5

np.random.seed(1234)
tf.set_random_seed(1234)


import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-d', '--data', default="../Laplace/data/Sin_BC_AMG_128.h5",
                    help='Select a data file')
parser.add_argument('-l', '--arch', type=int, nargs='*', help='architecture',
                    default = [2, 20, 20, 20, 20, 1])
parser.add_argument('-N_ub', '--N_Boundary', type=int, help='Number of points in the boundary',
                    default = 64)
parser.add_argument('-N_f', '--N_Collocation', type=int, help='Number of collocation points inside the subdomain',
                    default = 4000)
parser.add_argument('-N_I', '--N_Intersection', type=int, help='Number of points found in each intersected boundary',
                    default = 64)
parser.add_argument('-nIter', '--nIter', type=int, default=5000,
                    help ='Epochs of the ADAM optimizer')
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')

args = parser.parse_args()


if(args.reproduce):
    args.data = "../Laplace/data/Sin_BC_AMG_128.h5"
    args.arch = [2, 20, 20, 20, 20, 1]
    args.N_ub = 64
    args.N_f = 4000
    args.N_I = 64
    args.nIter = 5000
    args.DataType = "float32"





# =============================================================================
# Print File
# =============================================================================

def Print_vector_File(File_Name, Vector, Vector_Name):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt,"a") 
    file1.write(Vector_Name + " = [")
    for index,element in enumerate(Vector):
        if( index == len(Vector)-1):
            file1.write(str(element) + "]" + "\n")
        else:
            file1.write(str(element) + ',')
    file1.close()
    
def Print_String_File(File_Name, String):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt,"a") 
    file1.write(String + " \n")
    file1.close()

class XPINN:
    # Initialize the class
    def __init__(self, X_ub1 ,ub1, X_ub2 ,ub2,X_ub3 ,ub3,X_ub4 ,ub4,
            X_f1, X_f2, X_f3, X_f4, X_fi1, X_fi2, X_fi3, X_fi4,
            layers1, layers2, layers3, layers4):

    
        self.x_ub1 = X_ub1[:,0:1]
        self.y_ub1 = X_ub1[:,1:2]
        self.ub1 = ub1
        self.x_ub2 = X_ub2[:,0:1]
        self.y_ub2 = X_ub2[:,1:2]
        self.ub2 = ub2
        self.x_ub3 = X_ub3[:,0:1]
        self.y_ub3 = X_ub3[:,1:2]
        self.ub3 = ub3
        self.x_ub4 = X_ub4[:,0:1]
        self.y_ub4 = X_ub4[:,1:2]
        self.ub4 = ub4

        self.x_f1 = X_f1[:,0:1]
        self.y_f1 = X_f1[:,1:2]
        self.x_f2 = X_f2[:,0:1]
        self.y_f2 = X_f2[:,1:2]
        self.x_f3 = X_f3[:,0:1]
        self.y_f3 = X_f3[:,1:2]
        self.x_f4 = X_f4[:,0:1]
        self.y_f4 = X_f4[:,1:2]

        self.x_fi1 = X_fi1[:,0:1]
        self.y_fi1 = X_fi1[:,1:2]
        self.x_fi2 = X_fi2[:,0:1]
        self.y_fi2 = X_fi2[:,1:2]
        self.x_fi3 = X_fi3[:,0:1]
        self.y_fi3 = X_fi3[:,1:2]
        self.x_fi4 = X_fi4[:,0:1]
        self.y_fi4 = X_fi4[:,1:2]

        self.layers1 = layers1
        self.layers2 = layers2
        self.layers3 = layers3
        self.layers4 = layers4

        # Initialize NNs
        self.weights1, self.biases1, self.A1 = self.initialize_NN(layers1)
        self.weights2, self.biases2, self.A2 = self.initialize_NN(layers2)    
        self.weights3, self.biases3, self.A3 = self.initialize_NN(layers3)
        self.weights4, self.biases4, self.A4 = self.initialize_NN(layers4)
        
        # tf placeholders and graph
        self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True,
                                                     log_device_placement=True))
        
        self.x_ub1_tf  = tf.placeholder(tf.float64, shape=[None, self.x_ub1.shape[1]])
        self.y_ub1_tf  = tf.placeholder(tf.float64, shape=[None, self.y_ub1.shape[1]])
        self.x_ub2_tf  = tf.placeholder(tf.float64, shape=[None, self.x_ub2.shape[1]])
        self.y_ub2_tf  = tf.placeholder(tf.float64, shape=[None, self.y_ub2.shape[1]])
        self.x_ub3_tf  = tf.placeholder(tf.float64, shape=[None, self.x_ub3.shape[1]])
        self.y_ub3_tf  = tf.placeholder(tf.float64, shape=[None, self.y_ub3.shape[1]])
        self.x_ub4_tf  = tf.placeholder(tf.float64, shape=[None, self.x_ub4.shape[1]])
        self.y_ub4_tf  = tf.placeholder(tf.float64, shape=[None, self.y_ub4.shape[1]])

        self.x_f1_tf  = tf.placeholder(tf.float64, shape=[None, self.x_f1.shape[1]])
        self.y_f1_tf  = tf.placeholder(tf.float64, shape=[None, self.y_f1.shape[1]])
        self.x_f2_tf  = tf.placeholder(tf.float64, shape=[None, self.x_f2.shape[1]])
        self.y_f2_tf  = tf.placeholder(tf.float64, shape=[None, self.y_f2.shape[1]]) 
        self.x_f3_tf  = tf.placeholder(tf.float64, shape=[None, self.x_f3.shape[1]])
        self.y_f3_tf  = tf.placeholder(tf.float64, shape=[None, self.y_f3.shape[1]]) 
        self.x_f4_tf  = tf.placeholder(tf.float64, shape=[None, self.x_f4.shape[1]])
        self.y_f4_tf  = tf.placeholder(tf.float64, shape=[None, self.y_f4.shape[1]]) 
        
        self.x_fi1_tf = tf.placeholder(tf.float64, shape=[None, self.x_fi1.shape[1]])
        self.y_fi1_tf = tf.placeholder(tf.float64, shape=[None, self.y_fi1.shape[1]]) 
        self.x_fi2_tf = tf.placeholder(tf.float64, shape=[None, self.x_fi2.shape[1]])
        self.y_fi2_tf = tf.placeholder(tf.float64, shape=[None, self.y_fi2.shape[1]])   
        self.x_fi3_tf = tf.placeholder(tf.float64, shape=[None, self.x_fi3.shape[1]])
        self.y_fi3_tf = tf.placeholder(tf.float64, shape=[None, self.y_fi3.shape[1]])  
        self.x_fi4_tf = tf.placeholder(tf.float64, shape=[None, self.x_fi4.shape[1]])
        self.y_fi4_tf = tf.placeholder(tf.float64, shape=[None, self.y_fi4.shape[1]])        
                
        self.ub1_pred  = self.net_u1(self.x_ub1_tf, self.y_ub1_tf)
        self.ub2_pred  = self.net_u2(self.x_ub2_tf, self.y_ub2_tf)
        self.ub3_pred  = self.net_u3(self.x_ub3_tf, self.y_ub3_tf)
        self.ub4_pred  = self.net_u4(self.x_ub4_tf, self.y_ub4_tf)
        
        self.u1_pred = self.net_u1(self.x_f1_tf, self.y_f1_tf)
        self.u2_pred = self.net_u2(self.x_f2_tf, self.y_f2_tf)
        self.u3_pred = self.net_u3(self.x_f3_tf, self.y_f3_tf)
        self.u4_pred = self.net_u4(self.x_f4_tf, self.y_f4_tf)
        
        self.f1_pred, self.f2_pred,self.f3_pred,self.f4_pred,\
        self.fi1_pred, self.fi2_pred,self.fi3_pred, self.fi4_pred,\
        self.uavgi1_pred,self.uavgi2_pred, self.uavgi3_pred, self.uavgi4_pred,\
        self.u1i4_pred,  self.u1i1_pred, self.u2i1_pred, self.u2i2_pred, self.u3i2_pred, self.u3i3_pred, self.u4i3_pred, self.u4i4_pred\
            = self.net_f(self.x_f1_tf, self.y_f1_tf, self.x_f2_tf, self.y_f2_tf, self.x_f3_tf, self.y_f3_tf,self.x_f4_tf, self.y_f4_tf,\
             self.x_fi1_tf, self.y_fi1_tf, self.x_fi2_tf, self.y_fi2_tf, self.x_fi3_tf, self.y_fi3_tf, self.x_fi4_tf, self.y_fi4_tf)         
        
        self.loss1 = 20*tf.reduce_mean(tf.square(self.ub1 - self.ub1_pred)) \
                        + tf.reduce_mean(tf.square(self.f1_pred)) + 1*tf.reduce_mean(tf.square(self.fi1_pred))\
                        + 1*tf.reduce_mean(tf.square(self.fi2_pred))\
                        + 20*tf.reduce_mean(tf.square(self.u1i1_pred-self.uavgi1_pred))\
                        + 20*tf.reduce_mean(tf.square(self.u1i4_pred-self.uavgi4_pred))
                
        self.loss2 = 20*tf.reduce_mean(tf.square(self.ub2 - self.ub2_pred)) \
                        + tf.reduce_mean(tf.square(self.f2_pred)) + 1*tf.reduce_mean(tf.square(self.fi2_pred))\
                        + 1*tf.reduce_mean(tf.square(self.fi3_pred))\
                        + 20*tf.reduce_mean(tf.square(self.u2i2_pred-self.uavgi2_pred))\
                        + 20*tf.reduce_mean(tf.square(self.u2i1_pred-self.uavgi1_pred))  
                            
        self.loss3 = 20*tf.reduce_mean(tf.square(self.ub3 - self.ub3_pred)) \
                        + tf.reduce_mean(tf.square(self.f3_pred)) + 1*tf.reduce_mean(tf.square(self.fi3_pred))\
                        + 1*tf.reduce_mean(tf.square(self.fi4_pred))\
                        + 20*tf.reduce_mean(tf.square(self.u3i3_pred-self.uavgi3_pred))\
                        + 20*tf.reduce_mean(tf.square(self.u3i2_pred-self.uavgi2_pred))
        self.loss4 = 20*tf.reduce_mean(tf.square(self.ub4 - self.ub4_pred)) \
                        + tf.reduce_mean(tf.square(self.f4_pred)) + 1*tf.reduce_mean(tf.square(self.fi4_pred))\
                        + 1*tf.reduce_mean(tf.square(self.fi1_pred))\
                        + 20*tf.reduce_mean(tf.square(self.u4i4_pred-self.uavgi4_pred))\
                        + 20*tf.reduce_mean(tf.square(self.u4i3_pred-self.uavgi3_pred))              
                
        # self.optimizer = tf.contrib.opt.ScipyOptimizerInterface(self.loss, 
        #                                                         method = 'L-BFGS-B', 
        #                                                         options = {'maxiter': 5000,
        #                                                                    'maxfun': 5000,
        #                                                                    'maxcor': 50,
        #                                                                    'maxls': 50,
        #                                                                    'ftol' : 1.0 * np.finfo(float).eps})
        self.optimizer_Adam = tf.train.AdamOptimizer(0.0008)
        self.train_op_Adam1 = self.optimizer_Adam.minimize(self.loss1) 
        self.train_op_Adam2 = self.optimizer_Adam.minimize(self.loss2)   
        self.train_op_Adam3 = self.optimizer_Adam.minimize(self.loss3)
        self.train_op_Adam4 = self.optimizer_Adam.minimize(self.loss4)
        
        init = tf.global_variables_initializer()
        self.sess.run(init)

                
    def initialize_NN(self, layers):        
        weights = []
        biases = []
        A = []
        num_layers = len(layers) 
        for l in range(0,num_layers-1):
            W = self.xavier_init(size=[layers[l], layers[l+1]])
            b = tf.Variable(tf.zeros([1,layers[l+1]], dtype=tf.float64), dtype=tf.float64)
            a = tf.Variable(0.05, dtype=tf.float64)
            weights.append(W)
            biases.append(b)  
            A.append(a)
            
        return weights, biases, A
        
    def xavier_init(self, size):
        in_dim = size[0]
        out_dim = size[1]        
        xavier_stddev = np.sqrt(2/(in_dim + out_dim))
        return tf.Variable(tf.to_double(tf.truncated_normal([in_dim, out_dim], stddev=xavier_stddev)), dtype=tf.float64)
    
    def neural_net_tanh(self, X, weights, biases, A):
        num_layers = len(weights) + 1
        
        H = X 
        for l in range(0,num_layers-2):
            W = weights[l]
            b = biases[l]
            H = tf.tanh(20*A[l]*tf.add(tf.matmul(H, W), b)) 
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y
    
    def neural_net_sin(self, X, weights, biases, A):
        num_layers = len(weights) + 1
        
        H = X 
        for l in range(0,num_layers-2):
            W = weights[l]
            b = biases[l]
            H = tf.sin(20*A[l]*tf.add(tf.matmul(H, W), b))
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y
    
    def neural_net_cos(self, X, weights, biases, A):
        num_layers = len(weights) + 1
        
        H = X 
        for l in range(0,num_layers-2):
            W = weights[l]
            b = biases[l]
            H = tf.cos(20*A[l]*tf.add(tf.matmul(H, W), b))
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y
            
    def net_u1(self, x, y):
        u = self.neural_net_tanh(tf.concat([x,y],1), self.weights1, self.biases1, self.A1)
        return u
    
    def net_u2(self, x, y):
        u = self.neural_net_tanh(tf.concat([x,y],1), self.weights2, self.biases2, self.A2)
        return u
    
    def net_u3(self, x, y):
        u = self.neural_net_tanh(tf.concat([x,y],1), self.weights3, self.biases3, self.A3)
        return u
    def net_u4(self, x, y):
        u = self.neural_net_tanh(tf.concat([x,y],1), self.weights4, self.biases4, self.A4)
        return u
    
    def net_f(self, x1, y1, x2, y2, x3, y3, x4,y4,
     xi1, yi1, xi2, yi2, xi3, yi3, xi4, yi4):
        
        # Sub-Net1
        u1    = self.net_u1(x1,y1)
        u1_x  = tf.gradients(u1, x1)[0]
        u1_y  = tf.gradients(u1, y1)[0]
        u1_xx = tf.gradients(u1_x, x1)[0]
        u1_yy = tf.gradients(u1_y, y1)[0]
        
        # Sub-Net2
        u2    = self.net_u2(x2,y2)
        u2_x  = tf.gradients(u2, x2)[0]
        u2_y  = tf.gradients(u2, y2)[0]
        u2_xx = tf.gradients(u2_x, x2)[0]
        u2_yy = tf.gradients(u2_y, y2)[0]
        
        # Sub-Net3
        u3    = self.net_u3(x3,y3)
        u3_x  = tf.gradients(u3, x3)[0]
        u3_y  = tf.gradients(u3, y3)[0]
        u3_xx = tf.gradients(u3_x, x3)[0]
        u3_yy = tf.gradients(u3_y, y3)[0]

        # Sub-Net4
        u4    = self.net_u4(x4,y4)
        u4_x  = tf.gradients(u4, x4)[0]
        u4_y  = tf.gradients(u4, y4)[0]
        u4_xx = tf.gradients(u4_x, x4)[0]
        u4_yy = tf.gradients(u4_y, y4)[0]
        
        # Sub-Net1, Interface 1
        u1i1    = self.net_u1(xi1,yi1)
        u1i1_x  = tf.gradients(u1i1, xi1)[0]
        u1i1_y  = tf.gradients(u1i1, yi1)[0]
        u1i1_xx = tf.gradients(u1i1_x, xi1)[0]
        u1i1_yy = tf.gradients(u1i1_y, yi1)[0]

        # Sub-Net1, Interface 4
        u1i4    = self.net_u1(xi4,yi4)
        u1i4_x  = tf.gradients(u1i4, xi4)[0]
        u1i4_y  = tf.gradients(u1i4, yi4)[0]
        u1i4_xx = tf.gradients(u1i4_x, xi4)[0]
        u1i4_yy = tf.gradients(u1i4_y, yi4)[0]

        
        # Sub-Net2, Interface 2
        u2i2    = self.net_u2(xi2,yi2)
        u2i2_x  = tf.gradients(u2i2, xi2)[0]
        u2i2_y  = tf.gradients(u2i2, yi2)[0]
        u2i2_xx = tf.gradients(u2i2_x, xi2)[0]
        u2i2_yy = tf.gradients(u2i2_y, yi2)[0]

        # Sub-Net2, Interface 1
        u2i1    = self.net_u2(xi1,yi1)
        u2i1_x  = tf.gradients(u2i1, xi1)[0]
        u2i1_y  = tf.gradients(u2i1, yi1)[0]
        u2i1_xx = tf.gradients(u2i1_x, xi1)[0]
        u2i1_yy = tf.gradients(u2i1_y, yi1)[0]

        # Sub-Net3, Interface 3
        u3i3    = self.net_u3(xi3,yi3)
        u3i3_x  = tf.gradients(u3i3, xi3)[0]
        u3i3_y  = tf.gradients(u3i3, yi3)[0]
        u3i3_xx = tf.gradients(u3i3_x, xi3)[0]
        u3i3_yy = tf.gradients(u3i3_y, yi3)[0]

        # Sub-Net3, Interface 2
        u3i2    = self.net_u3(xi2,yi2)
        u3i2_x  = tf.gradients(u3i2, xi2)[0]
        u3i2_y  = tf.gradients(u3i2, yi2)[0]
        u3i2_xx = tf.gradients(u3i2_x, xi2)[0]
        u3i2_yy = tf.gradients(u3i2_y, yi2)[0]

        # Sub-Net4, Interface 4
        u4i4    = self.net_u4(xi4,yi4)
        u4i4_x  = tf.gradients(u4i4, xi4)[0]
        u4i4_y  = tf.gradients(u4i4, yi4)[0]
        u4i4_xx = tf.gradients(u4i4_x, xi4)[0]
        u4i4_yy = tf.gradients(u4i4_y, yi4)[0]

        # Sub-Net4, Interface 3
        u4i3    = self.net_u4(xi3,yi3)
        u4i3_x  = tf.gradients(u4i3, xi3)[0]
        u4i3_y  = tf.gradients(u4i3, yi3)[0]
        u4i3_xx = tf.gradients(u4i3_x, xi3)[0]
        u4i3_yy = tf.gradients(u4i3_y, yi3)[0]
        

    
        
        # Average value (Required for enforcing the average solution along the interface)
        uavgi1 = (u2i1 + u1i1)/2  
        uavgi2 = (u3i2 + u2i2)/2
        uavgi3 = (u4i3 + u3i3)/2
        uavgi4 = (u1i4 + u4i4)/2

        # Residuals
        f1 = u1_xx + u1_yy
        f2 = u2_xx + u2_yy 
        f3 = u3_xx + u3_yy 
        f4 = u4_xx + u4_yy 
        
        # Residual continuity conditions on the interfaces
        fi1 = (u1i1_xx + u1i1_yy) - (u2i1_xx + u2i1_yy) 
        fi2 = (u2i2_xx + u2i2_yy) - (u3i2_xx + u3i2_yy)
        fi3 = (u3i3_xx + u3i3_yy) - (u4i3_xx + u4i3_yy)
        fi4 = (u4i4_xx + u4i4_yy) - (u1i4_xx + u1i4_yy) 

        return   f1, f2, f3,f4, fi1, fi2, fi3, fi4, uavgi1, uavgi2,uavgi3, uavgi4, u1i4, u1i1, u2i1, u2i2, u3i2, u3i3, u4i3, u4i4
    
    def callback(self, loss):
        print(loss)
  
    def train(self,nIter, X_star1,X_star2,X_star3,X_star4, u_exact1, u_exact2, u_exact3, u_exact4):
          
 
        tf_dict = {self.x_ub1_tf: self.x_ub1, self.y_ub1_tf: self.y_ub1,
                    self.x_ub2_tf: self.x_ub2, self.y_ub2_tf: self.y_ub2,
                    self.x_ub3_tf: self.x_ub3, self.y_ub3_tf: self.y_ub3,
                    self.x_ub4_tf: self.x_ub4, self.y_ub4_tf: self.y_ub4,
                    self.x_f1_tf: self.x_f1, self.y_f1_tf: self.y_f1, 
                    self.x_f2_tf: self.x_f2, self.y_f2_tf: self.y_f2,
                   self.x_f3_tf: self.x_f3, self.y_f3_tf: self.y_f3, 
                   self.x_f4_tf: self.x_f4, self.y_f4_tf: self.y_f4, 
                   self.x_fi1_tf: self.x_fi1, self.y_fi1_tf: self.y_fi1, 
                   self.x_fi2_tf: self.x_fi2, self.y_fi2_tf: self.y_fi2,
                   self.x_fi3_tf: self.x_fi3, self.y_fi3_tf: self.y_fi3,
                   self.x_fi4_tf: self.x_fi4, self.y_fi4_tf: self.y_fi4,
                   }
        
        MSE_history1=[]
        MSE_history2=[]
        MSE_history3=[]
        MSE_history4=[]
        l2_err1=[]
        l2_err2=[]
        l2_err3=[]
        l2_err4=[]

        for it in range(nIter):
            self.sess.run(self.train_op_Adam1, tf_dict)
            self.sess.run(self.train_op_Adam2, tf_dict)
            self.sess.run(self.train_op_Adam3, tf_dict)
            self.sess.run(self.train_op_Adam4, tf_dict)
            
            if it %20 == 0:
                #elapsed = time.time() - start_time
                loss1_value = self.sess.run(self.loss1, tf_dict)
                loss2_value = self.sess.run(self.loss2, tf_dict)
                loss3_value = self.sess.run(self.loss3, tf_dict)
                loss4_value = self.sess.run(self.loss4, tf_dict)
 
                # Predicted solution
                u_pred1, u_pred2, u_pred3,u_pred4 = model.predict(X_star1,X_star2,X_star3,X_star4)

                # Relative L2 error in subdomains 2 and 3
                l2_error1 =  np.linalg.norm(u_exact1-u_pred1,2)/np.linalg.norm(u_exact1,2)
                l2_error2 =  np.linalg.norm(u_exact2-u_pred2,2)/np.linalg.norm(u_exact2,2)
                l2_error3 =  np.linalg.norm(u_exact3-u_pred3,2)/np.linalg.norm(u_exact3,2)
                l2_error4 =  np.linalg.norm(u_exact4-u_pred4,2)/np.linalg.norm(u_exact4,2)

                print('It: %d, Loss1: %.3e, Loss2: %.3e, Loss3: %.3e, Loss4: %.3e, L2_err1: %.3e, L2_err2: %.3e, L2_err3: %.3e, L2_err4: %.3e ' %
                    (it, loss1_value, loss2_value, loss3_value,loss4_value,
                       l2_error1, l2_error2, l2_error3, l2_error4))

               #start_time = time.time()
                MSE_history1.append(loss1_value)
                MSE_history2.append(loss2_value)
                MSE_history3.append(loss3_value)
                MSE_history4.append(loss4_value)
                l2_err1.append(l2_error1)
                l2_err2.append(l2_error2)
                l2_err3.append(l2_error3)
                l2_err4.append(l2_error4)
        
        return MSE_history1, MSE_history2, MSE_history3,MSE_history4, l2_err1, l2_err2, l2_err3, l2_err4
                    
        # self.optimizer.minimize(self.sess, 
        #                         feed_dict = tf_dict,         
        #                         fetches = [self.loss], 
        #                         loss_callback = self.callback)        
                                    
    
    def predict(self, X_star1, X_star2, X_star3, X_star4):
                
        u_star1 = self.sess.run(self.u1_pred, {self.x_f1_tf: X_star1[:,0:1], self.y_f1_tf: X_star1[:,1:2]})  
        u_star2 = self.sess.run(self.u2_pred, {self.x_f2_tf: X_star2[:,0:1], self.y_f2_tf: X_star2[:,1:2]})
        u_star3 = self.sess.run(self.u3_pred, {self.x_f3_tf: X_star3[:,0:1], self.y_f3_tf: X_star3[:,1:2]})
        u_star4 = self.sess.run(self.u4_pred, {self.x_f4_tf: X_star4[:,0:1], self.y_f4_tf: X_star4[:,1:2]})
        
        return u_star1, u_star2, u_star3, u_star4
    
if __name__ == "__main__": 
     
    # I will create 4 subdomains of 64x64

    # Boundary points from subdomian 
    N_ub   = args.N_Boundary
    
    # Residual points in subdomains
    N_f1   = args.N_Collocation
    N_f2   = args.N_Collocation
    N_f3   = args.N_Collocation
    N_f4   = args.N_Collocation
    
    # Interface points along the two interfaces
    N_I1   = args.N_Intersection
    N_I2   = args.N_Intersection
    N_I3   = args.N_Intersection
    N_I4   = args.N_Intersection

    
    # NN architecture in each subdomain
    layers1 = args.arch
    layers2 = args.arch
    layers3 = args.arch
    layers4 = args.arch
    
    # Load training data (boundary points), residual and interface points from .mat file
    
    File = args.data
    h = 1/32
    Width = 4
    psnFile = h5.File(File, 'r')
    nSample = psnFile.attrs['nSample']
    nCell = psnFile.attrs['domainSize'][0]
    psnData = psnFile['LaplaceSolution']
    print('# Samples: {}'.format(nSample))
    print('# Cells: {}*{}'.format(nCell, nCell))
    print('data shape: ', end='')
    print(psnData.shape)
    p = np.zeros(psnData.shape)
    p = psnData[:, :, :]
    pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
    print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))
    psnFile.close()

    Side = 1
    Exact = p[0,1:-1,1:-1]


    x_f1_aux = np.linspace(h/2, 2*Side - h/2, num=2*32*Side)
    y_f1_aux = np.flip(np.linspace(h/2, 2*Side - h/2, num=2*32*Side))
    u_exact1 = Exact[64:,0:64].flatten()[:, None]
    x_f1, y_f1 = np.meshgrid(x_f1_aux, y_f1_aux)
    x_f1 = x_f1.flatten()
    y_f1 = y_f1.flatten()

    x_f2_aux = np.linspace(h/2 + 2*Side, 4*Side - h/2, num=2*32*Side)
    y_f2_aux = np.flip(np.linspace(h/2, 2*Side - h/2, num=2*32*Side))
    u_exact2 = Exact[64:,64:].flatten()[:, None]
    x_f2, y_f2 = np.meshgrid(x_f2_aux, y_f2_aux)
    x_f2 = x_f2.flatten()
    y_f2 = y_f2.flatten()

    x_f3_aux = np.linspace(h/2 + 2*Side, 4*Side - h/2, num=2*32*Side)
    y_f3_aux = np.flip(np.linspace(h/2 + 2*Side, 4*Side - h/2, num=2*32*Side))
    u_exact3 = Exact[0:64,64:].flatten()[:, None]
    x_f3, y_f3 = np.meshgrid(x_f3_aux, y_f3_aux)
    x_f3 = x_f3.flatten()
    y_f3 = y_f3.flatten()

    x_f4_aux = np.linspace(h/2, 2*Side - h/2, num=2*32*Side)
    y_f4_aux = np.flip(np.linspace(h/2 + 2*Side, 4*Side - h/2, num=2*32*Side))
    u_exact4 = Exact[0:64,0:64].flatten()[:, None]
    x_f4, y_f4 = np.meshgrid(x_f4_aux, y_f4_aux)
    x_f4 = x_f4.flatten()
    y_f4 = y_f4.flatten()
    
    u_star = np.vstack([u_exact1, u_exact2, u_exact3, u_exact4])

    X_f1_train = np.hstack((x_f1.flatten()[:,None], y_f1.flatten()[:,None]))
    X_f2_train = np.hstack((x_f2.flatten()[:,None], y_f2.flatten()[:,None]))
    X_f3_train = np.hstack((x_f3.flatten()[:,None], y_f3.flatten()[:,None]))
    X_f4_train = np.hstack((x_f4.flatten()[:,None], y_f4.flatten()[:,None]))

    xi1 = np.linspace(2, 2, num=64)
    yi1 = np.linspace(2,0,num = 64)
    xi2 = np.linspace(2, 4, num=64)
    yi2 = np.linspace(2,2,num = 64)
    xi3 = np.linspace(2, 2, num=64)
    yi3 = np.linspace(4,2,num = 64)
    xi4 = np.linspace(0, 2, num=64)
    yi4 = np.linspace(2,2,num = 64)

    X_fi1_train = np.hstack((xi1.flatten()[:,None], yi1.flatten()[:,None]))
    X_fi2_train = np.hstack((xi2.flatten()[:,None], yi2.flatten()[:,None]))
    X_fi3_train = np.hstack((xi3.flatten()[:,None], yi3.flatten()[:,None]))
    X_fi4_train = np.hstack((xi4.flatten()[:,None], yi4.flatten()[:,None]))

    # Define boundaries for each subdomain
    xb = np.linspace(h/2,4-h/2,num = 128)
    yb = np.linspace(h/2,4-h/2,num = 128)
    xb_aux, yb_aux = np.meshgrid(xb, yb)
    

    xb1_a = np.linspace(0,0,num = 64)
    yb1_a = np.flip(np.linspace(h/2, 2 - h/2, num=64))
    X_b1_a = np.vstack([xb1_a, yb1_a]).T
    u_exact_1_a = np.sin(2*np.pi*(yb1_a)/Width)[:,None]
    xb1_b = np.linspace(h/2, 2 - h/2, num=64)
    yb1_b = np.linspace(0,0,num = 64)
    X_b1_b = np.vstack([xb1_b, yb1_b]).T
    u_exact_1_b = np.sin(2*np.pi*(xb1_b+2)/Width)[:,None]
    X_b1 = np.vstack([X_b1_a, X_b1_b])
    U_b1 = np.vstack([u_exact_1_a, u_exact_1_b])


    xb2_a = np.linspace(h/2+ 2*Side, 4*Side - h/2, num=2*32*Side)
    yb2_a = np.linspace(0,0,num = 64)
    X_b2_a = np.vstack([xb2_a, yb2_a]).T
    u_exact_2_a = np.sin(2*np.pi*(xb2_a+2)/Width)[:,None]
    xb2_b = np.linspace(4,4,num = 64)
    yb2_b = np.flip(np.linspace(h/2, 2 - h/2, num=64))
    X_b2_b = np.vstack([xb2_b, yb2_b]).T
    u_exact_2_b = np.sin(2*np.pi*(yb2_b+2)/Width)[:,None]
    X_b2 = np.vstack([X_b2_a, X_b2_b])
    U_b2 = np.vstack([u_exact_2_a, u_exact_2_b])

    xb3_a = np.linspace(4,4,num = 64)
    yb3_a = np.flip(np.linspace(h/2 + 2*Side, 4*Side - h/2, num=2*32*Side))
    X_b3_a = np.vstack([xb3_a, yb3_a]).T
    u_exact_3_a = np.sin(2*np.pi*(yb3_a+2)/Width)[:,None]
    xb3_b = np.linspace(h/2 + 2*Side, 4*Side - h/2, num=2*32*Side)
    yb3_b = np.linspace(4,4,num = 64)
    X_b3_b = np.vstack([xb3_b, yb3_b]).T
    u_exact_3_b = np.sin(2*np.pi*xb3_b/Width)[:,None]
    X_b3 = np.vstack([X_b3_a, X_b3_b])
    U_b3 = np.vstack([u_exact_3_a, u_exact_3_b])

    xb4_a = np.linspace(h/2, 2*Side - h/2, num=2*32*Side)
    yb4_a = np.linspace(4,4,num = 64)
    X_b4_a = np.vstack([xb4_a, yb4_a]).T
    u_exact_4_a = np.sin(2*np.pi*(xb4_a)/Width)[:,None]
    xb4_b = np.linspace(0,0,num = 64)
    yb4_b = np.flip(np.linspace(h/2 + 2*Side, 4*Side - h/2, num=2*32*Side))
    X_b4_b = np.vstack((xb4_b, yb4_b)).T
    u_exact_4_b = np.sin(2*np.pi*yb4_b/Width)[:,None]
    X_b4 = np.vstack([X_b4_a, X_b4_b])
    U_b4 = np.vstack([u_exact_4_a, u_exact_4_b])
    

    # Points in the whole  domain

    X_star = np.hstack((xb_aux.flatten()[:, None], yb_aux.flatten()[:, None]))

    X_star1 = np.hstack((x_f1.flatten()[:,None], y_f1.flatten()[:,None]))
    X_star2 = np.hstack((x_f2.flatten()[:,None], y_f2.flatten()[:,None]))
    X_star3 = np.hstack((x_f3.flatten()[:,None], y_f3.flatten()[:,None]))
    X_star4 = np.hstack((x_f4.flatten()[:,None], y_f4.flatten()[:,None]))
    
    
    # Randomly select the residual points from sub-domains
    idx1 = np.random.choice(X_f1_train.shape[0], N_f1, replace=False)    
    X_f1_train = X_f1_train[idx1,:]
    
    idx2 = np.random.choice(X_f2_train.shape[0], N_f2, replace=False)    
    X_f2_train = X_f2_train[idx2,:]
    
    idx3 = np.random.choice(X_f3_train.shape[0], N_f3, replace=False)    
    X_f3_train = X_f3_train[idx3,:]

    idx4 = np.random.choice(X_f4_train.shape[0], N_f4, replace=False)    
    X_f4_train = X_f4_train[idx4,:]
    
    # Randomly select boundary points
    idxb1 = np.random.choice(X_b1.shape[0], N_ub, replace=False)
    X_ub1_train = X_b1[idxb1,:]
    ub1_train   = U_b1[idxb1,:] 
    
    idxb2 = np.random.choice(X_b2.shape[0], N_ub, replace=False)
    X_ub2_train = X_b2[idxb2,:]
    ub2_train   = U_b2[idxb2,:] 

    idxb3 = np.random.choice(X_b3.shape[0], N_ub, replace=False)
    X_ub3_train = X_b3[idxb3,:]
    ub3_train   = U_b3[idxb3,:] 

    idxb4 = np.random.choice(X_b4.shape[0], N_ub, replace=False)
    X_ub4_train = X_b4[idxb4,:]
    ub4_train   = U_b4[idxb4,:] 
    
    # Randomly select the interface points along two interfaces
    idxi1 = np.random.choice(X_fi1_train.shape[0], N_I1, replace=False)    
    X_fi1_train = X_fi1_train[idxi1,:]
    
    idxi2 = np.random.choice(X_fi2_train.shape[0], N_I2, replace=False)    
    X_fi2_train = X_fi2_train[idxi2,:]

    idxi3 = np.random.choice(X_fi3_train.shape[0], N_I3, replace=False)    
    X_fi3_train = X_fi3_train[idxi3,:]

    idxi4 = np.random.choice(X_fi4_train.shape[0], N_I4, replace=False)    
    X_fi4_train = X_fi4_train[idxi4,:]
   
    # XPINN model
    model = XPINN(X_ub1_train ,ub1_train, X_ub2_train ,ub2_train, X_ub3_train ,ub3_train, X_ub4_train ,ub4_train,
        X_f1_train,X_f2_train,X_f3_train,X_f4_train, X_fi1_train, X_fi2_train,X_fi3_train,X_fi4_train, layers1, layers2, layers3,layers4)
    
    # Training
    Max_iter = args.nIter
    start_time = time.time()                
    MSE_hist1, MSE_hist2, MSE_hist3,MSE_hist4, l2_err1, l2_err2, l2_err3, l2_err4 = model.train(Max_iter, X_star1,X_star2,X_star3, X_star4,
     u_exact1, u_exact2, u_exact3, u_exact4)
    elapsed = time.time() - start_time                
    print('Training time: %.4f' % (elapsed))
    
    # Solution prediction
    u_pred1, u_pred2, u_pred3, u_pred4 = model.predict(X_star1,X_star2,X_star3, X_star4)
            
    # Concatenating the solution from subdomains
    u_pred = np.concatenate([u_pred1, u_pred2, u_pred3, u_pred4])

    error_u = np.linalg.norm(u_star - u_pred, 2) / np.linalg.norm(u_star, 2)
    print('Relative Squarred Error u: %e' % (error_u))
    
    MAE = np.mean(np.abs(u_star - u_pred))
    print('MAE: %e' % (MAE))
    
    MSE = np.mean((u_star - u_pred)** 2)
    print('MSE: %e' % (MSE))
    x = np.linspace(h/2, 4-h/2, num=128)
    y = np.linspace(h/2, 4-h/2, num=128)
    X, Y = np.meshgrid(x, y)
    
    X_star = np.vstack([X_star1,X_star2,X_star3, X_star4])
    U_pred = griddata(X_star, u_pred.flatten(), (X, Y), method='cubic')
    Error = np.abs(Exact - U_pred)
    
    Current = "Xpinn results :------"
    Name = "Xpinn results :------"
    Print_String_File('X_PINN_128', Current)
    Print_vector_File('X_PINN_128', [MAE], "MAE")
    Print_vector_File('X_PINN_128', [MSE], "MSE")
    Print_vector_File('X_PINN_128', [elapsed], "Times")

