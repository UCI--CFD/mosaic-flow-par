#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 12:46:16 2021

@author: robertplanas
"""

Input, Output = next(TrainGen)
BC, xy_dat, xy_col = Input
U_dat = Output
with tf.GradientTape(persistent=False) as tape0:
    if(DON_Model.alpha != 0):
        with tf.GradientTape(persistent=True, watch_accessed_variables=False) as tape2:
            tape2.watch(xy_col)
            with tf.GradientTape(persistent=False,  watch_accessed_variables=False ) as tape1:
                tape1.watch(xy_col)
                B_Pred = DON_Model.Predict_mlp_B(BC)
                T_col_Pred = DON_Model.Predict_mlp_T(xy_col)
                T_col_Pred = tf.repeat(T_col_Pred, repeats = DON_Model.Batch, axis = 0 )
                U_col = tf.reduce_sum(tf.multiply(B_Pred, T_col_Pred), axis = -1)
            
            U_col_grad = tape1.gradient(U_col, xy_col)
            U_col_grad_x = U_col_grad[:,0]
            U_col_grad_y = U_col_grad[:,1]
        U_col_grad_xx = tape2.gradient(U_col_grad_x, xy_col)[:,0]
        U_col_grad_yy = tape2.gradient(U_col_grad_y, xy_col)[:,1]
        Res = tf.reduce_mean(tf.square(U_col_grad_xx + U_col_grad_yy ))
        del tape2
    else:
        B_Pred = DON_Model.Predict_mlp_B(BC)
        B_Pred_dat = tf.repeat(B_Pred, repeats = DON_Model.Dat, axis = 0 )
        print(B_Pred_dat.shape)
        Res = 0
                        
    T_dat_Pred = DON_Model.Predict_mlp_T(xy_dat)
    
    T_dat_Pred_B = tf.tile(T_dat_Pred, multiples = [DON_Model.Batch, 1])
    U_dat_Pred = tf.reduce_sum(tf.multiply(B_Pred_dat, T_dat_Pred_B), axis = -1)
    U_dat_Pred = tf.reshape(U_dat_Pred, (DON_Model.Batch, DON_Model.Dat))
    print(U_dat.shape)
    print(U_dat_Pred.shape)
    MSE = tf.reduce_mean(tf.square(U_dat - U_dat_Pred ))
    print(MSE.shape)
    loss = MSE + DON_Model.alpha*Res + tf.reduce_sum(DON_Model.losses)
    
lossGrad = tape0.gradient(loss, DON_Model.trainable_variables)
del tape0
DON_Model.optimizer.apply_gradients(zip(lossGrad, DON_Model.trainable_variables))
        