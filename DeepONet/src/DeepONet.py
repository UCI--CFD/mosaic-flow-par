#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 12:13:27 2021

@author: robertplanas
"""

import tensorflow as tf
import math as math
from tensorflow import keras
import numpy as np
from matplotlib import pyplot as plt

# =============================================================================
# Metrics
# =============================================================================


#Usual metrics computed at the end of the model 

# Laplace ---
Mae = tf.keras.metrics.MeanAbsoluteError(name='mae')
Loss = tf.keras.metrics.Mean(name='loss')
Predict_mse = tf.keras.metrics.Mean(name='Pred_Mse')
Residual_mse = tf.keras.metrics.Mean(name='Res_Mse')


# NS ---
Mae_u = tf.keras.metrics.MeanAbsoluteError(name='mae_u')
Mae_v = tf.keras.metrics.MeanAbsoluteError(name='mae_v')
Mae_p = tf.keras.metrics.MeanAbsoluteError(name='mae_p')

Mse_u = tf.keras.metrics.Mean(name='mse_u')
Mse_v = tf.keras.metrics.Mean(name='mse_v')
Mse_p = tf.keras.metrics.Mean(name='mse_p')

Pde_0 = tf.keras.metrics.Mean(name='pde_0')
Pde_1 = tf.keras.metrics.Mean(name='pde_1')
Pde_2 = tf.keras.metrics.Mean(name='pde_2')



# =============================================================================
# Models, DeepONet
# =============================================================================

class DeepONet_Unstacked_Lap(tf.keras.Model):
    
    def __init__(self, HL_B = 5, Units_B = 100, P = 100, 
                 HL_T = 5, Units_T = 100, 
                alpha = 0, activation_B = 'tanh', activation_T = 'tanh', 
                reg = 0, nCell = 32, Batch = 128, Dat = 1024, nCol = 500,
                data_type = 'float32', **kwargs ):
    
        super(DeepONet_Unstacked_Lap, self).__init__(**kwargs)    
        
        # Structure
        self.nCell = nCell 
        self.HL_B = HL_B
        self.HL_T = HL_T
        self.Units_B = Units_B
        self.Units_T = Units_T
        self.act_B = activation_T
        self.act_T = activation_B
        self.P = P
        
        # Training parameters       
        self.Batch = Batch
        self.Dat = Dat
        self.nCol = nCol
        
        self.alpha = alpha
        if(data_type =='float32'):
            self.data_type = tf.float32
        elif(data_type =='float64'):
            self.data_type = tf.float64
        else:
            assert 1 == 0, "Invalid data type. Options: 'float32' or 'float64'"
            
        self.reg = tf.keras.regularizers.l2(reg)
        
        # Define the Branch Net
        self.mlp_B = []
        for H in range(self.HL_B):
            self.mlp_B.append(tf.keras.layers.Dense(self.Units_B, activation=self.act_B, 
                                                 kernel_regularizer=self.reg, 
                                                 bias_regularizer=self.reg,
                                                 name = "HL_B_" + str(H),
                                                 ))
        self.mlp_B.append(tf.keras.layers.Dense(self.P, activation='linear', 
                                             kernel_regularizer=self.reg, 
                                             bias_regularizer=self.reg,
                                             name = "HL_B_Final",
                                             ))
        # Define the Trunk Net
        self.mlp_T = []
        for H in range(self.HL_T):
            self.mlp_T.append(tf.keras.layers.Dense(self.Units_T, activation=self.act_T, 
                                                 kernel_regularizer=self.reg, 
                                                 bias_regularizer=self.reg,
                                                 name = "HL_T_" + str(H),
                                                 ))
        self.mlp_T.append(tf.keras.layers.Dense(self.P, activation='linear', 
                                             kernel_regularizer=self.reg, 
                                             bias_regularizer=self.reg,
                                             name = "HL_T_Final",
                                             ))
    
    def call(self, Inputs):
        BC, xy, *_ = Inputs
        # Compute the BC
        BC = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        for layer in self.mlp_B:
            BC = layer(BC)
        for layer in self.mlp_T:
            xy = layer(xy)
        Dat_Points = xy.shape[0]
        BC_Cond = BC.shape[0]
        BC = tf.repeat(BC, repeats = Dat_Points, axis = 0 )
        xy = tf.tile(xy, multiples = [BC_Cond, 1])
        Out = tf.reduce_sum(tf.multiply(BC, xy), axis = -1)
        Out_reshape = tf.reshape(Out, (BC_Cond, Dat_Points))
        return Out_reshape
    
    def Predict_Shape(self, Inputs):
        BC, xy = Inputs
        BC = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        # Compute the BC
        print("BC = ", BC.shape)
        for layer in self.mlp_B:
            BC = layer(BC)
            print("BC = ", BC.shape , " ", layer.name)
            
        
        
        print("xy = ", xy.shape )
        for layer in self.mlp_T:
            xy = layer(xy)
            print("xy = ", xy.shape , " ", layer.name)
        Dat_Points = xy.shape[0]
        BC_Cond = BC.shape[0]
        BC = tf.repeat(BC, repeats = Dat_Points, axis = 0 )
        xy = tf.tile(xy, multiples = [BC_Cond, 1])
        print("BC Repeat = ", BC.shape ,)
        print("xy Repeat = ", xy.shape ,)
        Out = tf.reduce_sum(tf.multiply(BC, xy), axis = -1)
        print("Output = ", Out.shape)
        Out_reshape = tf.reshape(Out, (BC_Cond, Dat_Points))
        print(Out_reshape)
        return Out_reshape
    
    def Predict_mlp_B(self, BC):
        BC_x = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        #print(BC_x.shape)
        for layer in self.mlp_B:
            BC_x = layer(BC_x)
        #    print(BC_x.shape, "  ", layer.name)
        #print(BC_x.shape)
        return BC_x
    
    def Predict_mlp_T(self, xy_p):
        for layer in self.mlp_T:
            xy_p = layer(xy_p)
        #print(xy_p.shape)
        return xy_p
    
    
    def Predict_mlp_B_1(self, BC):
        BC_x = BC 
        #print(BC_x.shape)
        for layer in self.mlp_B:
            BC_x = layer(BC_x)
        #    print(BC_x.shape, "  ", layer.name)
        #print(BC_x.shape)
        return BC_x
        
    @tf.function
    def Generate_BC(self, BC, Points):
        BC = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        BC = tf.repeat(BC, repeats = Points, axis = 0 )
        return BC

    
    def train_step(self, data):
        Input, Output = data
        BC, xy_dat, xy_col = Input
        U_dat = Output
        with tf.GradientTape(persistent=False) as tape0:
            B_Pred = self.Predict_mlp_B(BC)
            if(self.alpha != 0):
                xy_col = tf.tile(xy_col, multiples = [self.Batch, 1])
                B_Pred_col= tf.repeat(B_Pred, repeats = self.nCol, axis = 0 ) 
                with tf.GradientTape(persistent=True, watch_accessed_variables=False) as tape2:
                    tape2.watch(xy_col)
                    with tf.GradientTape(persistent=False,  watch_accessed_variables=False ) as tape1:
                        tape1.watch(xy_col)
                        T_col_Pred = self.Predict_mlp_T(xy_col)
                        U_col = tf.reduce_sum(tf.multiply(B_Pred_col, T_col_Pred), axis = -1)
                    
                    U_col_grad = tape1.gradient(U_col, xy_col)
                    U_col_grad_x = U_col_grad[:,0]
                    U_col_grad_y = U_col_grad[:,1]
                U_col_grad_xx = tape2.gradient(U_col_grad_x, xy_col)[:,0]
                U_col_grad_yy = tape2.gradient(U_col_grad_y, xy_col)[:,1]
                Res = tf.reduce_mean(tf.square(U_col_grad_xx + U_col_grad_yy ))
                del tape2
            else:
                Res = 0
                
            B_Pred_dat = tf.repeat(B_Pred, repeats = self.Dat, axis = 0 )                   
            T_dat_Pred = self.Predict_mlp_T(xy_dat)
            
            T_dat_Pred_B = tf.tile(T_dat_Pred, multiples = [self.Batch, 1])
            U_dat_Pred = tf.reduce_sum(tf.multiply(B_Pred_dat, T_dat_Pred_B), axis = -1)
            U_dat_Pred = tf.reshape(U_dat_Pred, (self.Batch, self.Dat))
            MSE = tf.reduce_mean(tf.square(U_dat - U_dat_Pred ))
            loss = MSE + self.alpha*Res + tf.reduce_sum(self.losses)
            
        lossGrad = tape0.gradient(loss, self.trainable_variables)
        del tape0
        self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))
        
        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss) 
        Mae.update_state(U_dat_Pred, U_dat)
        Residual_mse.update_state(Res)
        Predict_mse.update_state(MSE)
        
        return {'loss':Loss.result(), 'Pred_Mse':Predict_mse.result(),
                'mae': Mae.result(), 'Res_Mse':Residual_mse.result()}
    
    def test_step(self, data):
        Input, Output = data
        BC, xy_dat, xy_col = Input
        U_dat = Output

        B_Pred = self.Predict_mlp_B(BC)
        if(self.alpha != 0):
            xy_col = tf.tile(xy_col, multiples = [self.Batch, 1])
            B_Pred_col= tf.repeat(B_Pred, repeats = self.nCol, axis = 0 ) 
            with tf.GradientTape(persistent=True, watch_accessed_variables=False) as tape2:
                tape2.watch(xy_col)
                with tf.GradientTape(persistent=False,  watch_accessed_variables=False ) as tape1:
                    tape1.watch(xy_col)
                    T_col_Pred = self.Predict_mlp_T(xy_col)
                    U_col = tf.reduce_sum(tf.multiply(B_Pred_col, T_col_Pred), axis = -1)
                
                U_col_grad = tape1.gradient(U_col, xy_col)
                U_col_grad_x = U_col_grad[:,0]
                U_col_grad_y = U_col_grad[:,1]
            U_col_grad_xx = tape2.gradient(U_col_grad_x, xy_col)[:,0]
            U_col_grad_yy = tape2.gradient(U_col_grad_y, xy_col)[:,1]
            Res = tf.reduce_mean(tf.square(U_col_grad_xx + U_col_grad_yy ))
            del tape2
        else:
            Res = 0
            
            
        B_Pred_dat = tf.repeat(B_Pred, repeats = self.Dat, axis = 0 )
        T_dat_Pred = self.Predict_mlp_T(xy_dat)
        T_dat_Pred_B = tf.tile(T_dat_Pred, multiples = [self.Batch, 1])
        U_dat_Pred = tf.reduce_sum(tf.multiply(B_Pred_dat, T_dat_Pred_B), axis = -1)
        U_dat_Pred = tf.reshape(U_dat_Pred, (self.Batch, self.Dat))
        MSE = tf.reduce_mean(tf.square(U_dat - U_dat_Pred ))
        loss = MSE + self.alpha*Res + tf.reduce_sum(self.losses)
        
        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss) 
        Mae.update_state(U_dat_Pred, U_dat)
        Residual_mse.update_state(Res)
        Predict_mse.update_state(MSE)
        return {'loss':Loss.result(), 'Pred_Mse':Predict_mse.result(),
                'mae': Mae.result(), 'Res_Mse':Residual_mse.result()}
    
    @property
    def metrics(self):
        return [Loss,Predict_mse, Mae, Residual_mse]
    
                    

# =============================================================================
# Non Linear approximator, 
# =============================================================================

class DeepONet_Stacked_Lap(tf.keras.Model):
    
    def __init__(self, HL_B = 2, Units_B = 100, P = 100, 
                 HL_T = 3, Units_T = 100, 
                alpha = 0, activation_B = 'sigmoid', activation_T = 'sigmoid', 
                reg = 0, nCell = 32, Batch = 128, Dat = 1024, nCol = 500,
                data_type = 'float32', **kwargs ):
    
        super(DeepONet_Stacked_Lap, self).__init__(**kwargs)    
        
        # Structure
        self.nCell = nCell 
        self.HL_B = HL_B
        self.HL_T = HL_T
        self.Units_B = Units_B
        self.Units_T = Units_T
        self.act_B = activation_T
        self.act_T = activation_B
        self.P = P
        
        # Training parameters       
        self.Batch = Batch
        self.Dat = Dat
        self.nCol = nCol
        
        self.alpha = alpha
        if(data_type =='float32'):
            self.data_type = tf.float32
        elif(data_type =='float64'):
            self.data_type = tf.float64
        else:
            assert 1 == 0, "Invalid data type. Options: 'float32' or 'float64'"
            
        self.reg = tf.keras.regularizers.l2(reg)
        
        # Define the Branch Nets Stacked
        self.BN = []
        for i in range(self.P):
            mlp_B = []
            for H in range(self.HL_B):
                mlp_B.append(tf.keras.layers.Dense(self.Units_B, activation=self.act_B, 
                                                         kernel_regularizer=self.reg, 
                                                         bias_regularizer=self.reg,
                                                         name = "HL_B_" + str(H) + "_" + str(i),
                                                         ))
            mlp_B.append(tf.keras.layers.Dense(1, activation='linear', 
                                                     kernel_regularizer=self.reg, 
                                                     bias_regularizer=self.reg,
                                                     name = "HL_B_Final" + "_" + str(i),
                                                     ))
            self.BN.append(mlp_B)
            
        # Define the Trunk Net
        self.mlp_T = []
        for H in range(self.HL_T):
            self.mlp_T.append(tf.keras.layers.Dense(self.Units_T, activation=self.act_T, 
                                                 kernel_regularizer=self.reg, 
                                                 bias_regularizer=self.reg,
                                                 name = "HL_T_" + str(H),
                                                 ))
        self.mlp_T.append(tf.keras.layers.Dense(self.P, activation='linear', 
                                             kernel_regularizer=self.reg, 
                                             bias_regularizer=self.reg,
                                             name = "HL_T_Final",
                                             ))
    
    def call(self, Inputs):
        BC, xy, *_ = Inputs
        # Compute the BC
        BC = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        BN = []
        for index_n, net in enumerate(self.BN):
            BC_i = BC
            for layer in net:
                BC_i = layer(BC_i)
            if(index_n == 0):
                BN = BC_i
            else: 
                BN = tf.concat([BN,BC_i], axis = -1)
        for layer in self.mlp_T:
            xy = layer(xy)
        Dat_Points = xy.shape[0]
        BN_Cond = BN.shape[0]
        BN = tf.repeat(BN, repeats = Dat_Points, axis = 0 )
        xy = tf.tile(xy, multiples = [BN_Cond, 1])
        Out = tf.reduce_sum(tf.multiply(BN, xy), axis = -1)
        Out_reshape = tf.reshape(Out, (BN_Cond, Dat_Points))
        return Out_reshape
    
    def Predict_Shape(self, Inputs):
        BC, xy, *_ = Inputs
        # Compute the BC
        BC = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        print("BC = ", BC.shape)
        BN = []
        for index_n, net in enumerate(self.BN):
            BC_i = BC
            for layer in net:
                BC_i = layer(BC_i)
            if(index_n == 0):
                BN = BC_i
            else: 
                BN = tf.concat([BN,BC_i], axis = -1)
        print("BN = ", BN.shape)
        print("xy = ", xy.shape)
        
        for layer in self.mlp_T:
            xy = layer(xy)
            
        
        Dat_Points = xy.shape[0]
        BN_Cond = BN.shape[0]
        BN = tf.repeat(BN, repeats = Dat_Points, axis = 0 )
        xy = tf.tile(xy, multiples = [BN_Cond, 1])
        print("BN = ", BN.shape)
        print("xy = ", xy.shape)
        Out = tf.reduce_sum(tf.multiply(BN, xy), axis = -1)
        Out_reshape = tf.reshape(Out, (BN_Cond, Dat_Points))
        print("Out Reshaped = ", Out_reshape.shape)
        return Out_reshape
    
    def Predict_mlp_B(self, BC):
        BC_x = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        BN = []
        for index_n, net in enumerate(self.BN):
            BC_i = BC_x
            for layer in net:
                BC_i = layer(BC_i)
            if(index_n == 0):
                BN = BC_i
            else: 
                BN = tf.concat([BN,BC_i], axis = -1)
        return BN
    
    def Predict_mlp_T(self, xy_p):
        for layer in self.mlp_T:
            xy_p = layer(xy_p)
        #print(xy_p.shape)
        return xy_p
    
    def train_step(self, data):
        Input, Output = data
        BC, xy_dat, xy_col = Input
        U_dat = Output
        with tf.GradientTape(persistent=False) as tape0:
            B_Pred = self.Predict_mlp_B(BC)
            if(self.alpha != 0):
                xy_col = tf.tile(xy_col, multiples = [self.Batch, 1])
                B_Pred_col= tf.repeat(B_Pred, repeats = self.nCol, axis = 0 ) 
                with tf.GradientTape(persistent=True, watch_accessed_variables=False) as tape2:
                    tape2.watch(xy_col)
                    with tf.GradientTape(persistent=False,  watch_accessed_variables=False ) as tape1:
                        tape1.watch(xy_col)
                        T_col_Pred = self.Predict_mlp_T(xy_col)
                        U_col = tf.reduce_sum(tf.multiply(B_Pred_col, T_col_Pred), axis = -1)
                    
                    U_col_grad = tape1.gradient(U_col, xy_col)
                    U_col_grad_x = U_col_grad[:,0]
                    U_col_grad_y = U_col_grad[:,1]
                U_col_grad_xx = tape2.gradient(U_col_grad_x, xy_col)[:,0]
                U_col_grad_yy = tape2.gradient(U_col_grad_y, xy_col)[:,1]
                Res = tf.reduce_mean(tf.square(U_col_grad_xx + U_col_grad_yy ))
                del tape2
            else:
                Res = 0
                    
            B_Pred_dat = tf.repeat(B_Pred, repeats = self.Dat, axis = 0 )                   
            T_dat_Pred = self.Predict_mlp_T(xy_dat)
            
            T_dat_Pred_B = tf.tile(T_dat_Pred, multiples = [self.Batch, 1])
            U_dat_Pred = tf.reduce_sum(tf.multiply(B_Pred_dat, T_dat_Pred_B), axis = -1)
            U_dat_Pred = tf.reshape(U_dat_Pred, (self.Batch, self.Dat))
            MSE = tf.reduce_mean(tf.square(U_dat - U_dat_Pred ))
            loss = MSE + self.alpha*Res + tf.reduce_sum(self.losses)
            
        lossGrad = tape0.gradient(loss, self.trainable_variables)
        del tape0
        self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))
        
        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss) 
        Mae.update_state(U_dat_Pred, U_dat)
        Residual_mse.update_state(Res)
        Predict_mse.update_state(MSE)
        
        return {'loss':Loss.result(), 'Pred_Mse':Predict_mse.result(),
                'mae': Mae.result(), 'Res_Mse':Residual_mse.result()}
    
    def test_step(self, data):
        Input, Output = data
        BC, xy_dat, xy_col = Input
        U_dat = Output
        
        B_Pred = self.Predict_mlp_B(BC)
        if(self.alpha != 0):
            xy_col = tf.tile(xy_col, multiples = [self.Batch, 1])
            B_Pred_col= tf.repeat(B_Pred, repeats = self.nCol, axis = 0 ) 
            with tf.GradientTape(persistent=True, watch_accessed_variables=False) as tape2:
                tape2.watch(xy_col)
                with tf.GradientTape(persistent=False,  watch_accessed_variables=False ) as tape1:
                    tape1.watch(xy_col)
                    T_col_Pred = self.Predict_mlp_T(xy_col)
                    U_col = tf.reduce_sum(tf.multiply(B_Pred_col, T_col_Pred), axis = -1)
                
                U_col_grad = tape1.gradient(U_col, xy_col)
                U_col_grad_x = U_col_grad[:,0]
                U_col_grad_y = U_col_grad[:,1]
            U_col_grad_xx = tape2.gradient(U_col_grad_x, xy_col)[:,0]
            U_col_grad_yy = tape2.gradient(U_col_grad_y, xy_col)[:,1]
            Res = tf.reduce_mean(tf.square(U_col_grad_xx + U_col_grad_yy ))
            del tape2
        else:
            Res = 0
            
        B_Pred_dat = tf.repeat(B_Pred, repeats = self.Dat, axis = 0 )
        T_dat_Pred = self.Predict_mlp_T(xy_dat)
        T_dat_Pred_B = tf.tile(T_dat_Pred, multiples = [self.Batch, 1])
        U_dat_Pred = tf.reduce_sum(tf.multiply(B_Pred_dat, T_dat_Pred_B), axis = -1)
        U_dat_Pred = tf.reshape(U_dat_Pred, (self.Batch, self.Dat))
        MSE = tf.reduce_mean(tf.square(U_dat - U_dat_Pred ))
        loss = MSE + self.alpha*Res + tf.reduce_sum(self.losses)
        
        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss) 
        Mae.update_state(U_dat_Pred, U_dat)
        Residual_mse.update_state(Res)
        Predict_mse.update_state(MSE)
        return {'loss':Loss.result(), 'Pred_Mse':Predict_mse.result(),
                'mae': Mae.result(), 'Res_Mse':Residual_mse.result()}
    
    @property
    def metrics(self):
        return [Loss,Predict_mse, Mae, Residual_mse]
    
    

# =============================================================================
# MF Predictor model 
# =============================================================================

class MF_DeepONet_Stacked_Lap(DeepONet_Stacked_Lap):
    
    def __init__(self, **kwargs ):
        super(MF_DeepONet_Stacked_Lap, self).__init__(**kwargs)   
        
        X_points = np.linspace(0,1, num = self.nCell )[None, :]
        Y_points = np.linspace(0,1, num = self.nCell)[None, :]
        self.xy_dat = np.reshape(np.asarray(np.meshgrid(X_points, Y_points)).T, ((self.nCell)**2, 2)) 
        self.xy_T = None
        
    def call(self, Inputs):
        BC, xy, *_ = Inputs
        # Compute the BC
        BC = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        BN = []
        for index_n, net in enumerate(self.BN):
            BC_i = BC
            for layer in net:
                BC_i = layer(BC_i)
            if(index_n == 0):
                BN = BC_i
            else: 
                BN = tf.concat([BN,BC_i], axis = -1)
        for layer in self.mlp_T:
            xy = layer(xy)
        
        Dat_Points = xy.shape[0]
        BN_Cond = BN.shape[0]
        BN = tf.repeat(BN, repeats = Dat_Points, axis = 0 )
        xy = tf.tile(xy, multiples = [BN_Cond, 1])
        Out = tf.reduce_sum(tf.multiply(BN, xy), axis = -1)
        Out_reshape = tf.reshape(Out, (BN_Cond, Dat_Points))
        return Out_reshape
    
    def Predict(self, Inputs):
        BC = Inputs
        # Compute the BC
        BC = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        BN = []
        for index_n, net in enumerate(self.BN):
            BC_i = BC
            for layer in net:
                BC_i = layer(BC_i)
            if(index_n == 0):
                BN = BC_i
            else: 
                BN = tf.concat([BN,BC_i], axis = -1)
                
        if(self.xy_T == None):
            xy = self.xy_dat
            for layer in self.mlp_T:
                xy = layer(xy)
            self.xy_T = xy 
            
        
        Dat_Points = self.xy_T.shape[0]
        BN_Cond = BN.shape[0]
        BN = tf.repeat(BN, repeats = Dat_Points, axis = 0 )
        xy = tf.tile(self.xy_T, multiples = [BN_Cond, 1])
        Out = tf.reduce_sum(tf.multiply(BN, xy), axis = -1)
        Out_reshape = tf.reshape(Out, (BN_Cond, self.nCell, self.nCell))
        return Out_reshape
    


# =============================================================================
# MF Predictor model 
# =============================================================================

class MF_DeepONet_UnStacked_Lap(DeepONet_Unstacked_Lap):
    
    def __init__(self,  HL_B = 5, Units_B = 100, P = 100, 
                 HL_T = 5, Units_T = 100, 
                alpha = 0, activation_B = 'swish', activation_T = 'tanh', 
                reg = 0, nCell = 32, Batch = 128, Dat = 1024, nCol = 500, **kwargs ):
        super(MF_DeepONet_UnStacked_Lap, self).__init__(HL_B = HL_B, Units_B = Units_B, P = P, 
                     HL_T = HL_T, Units_T = Units_T, 
                    alpha = 0, activation_B = activation_B, activation_T = activation_T,
                    reg = reg, nCell = nCell, Batch = Batch, Dat = Dat, nCol = nCol, **kwargs)   
        
        X_points = np.linspace(0,1, num = self.nCell )[None, :]
        Y_points = np.linspace(0,1, num = self.nCell)[None, :]
        self.xy_dat = np.reshape(np.asarray(np.meshgrid(X_points, Y_points)).T, ((self.nCell)**2, 2)) 
        self.xy_T = None
        
    def Predict(self, Inputs):
        BC = Inputs
        # Compute the BC
        BC = tf.concat([BC[:, 0:self.nCell -1, 0], BC[:, -1, 0:self.nCell -1], BC[:, 1:self.nCell, -1], BC[:,0, 1:self.nCell]],axis = -1  ) 
        for index, layer in enumerate(self.mlp_B):
            BC = layer(BC) 
                
        if(self.xy_T == None):
            xy = self.xy_dat
            for layer in self.mlp_T:
                xy = layer(xy)
            self.xy_T = xy 
            
        
        Dat_Points = self.xy_T.shape[0]
        BN_Cond = BC.shape[0]
        BN = tf.repeat(BC, repeats = Dat_Points, axis = 0 )
        xy = tf.tile(self.xy_T, multiples = [BN_Cond, 1])
        Out = tf.reduce_sum(tf.multiply(BN, xy), axis = -1)
        Out_reshape = tf.reshape(Out, (BN_Cond, self.nCell, self.nCell))
        return Out_reshape
    
        