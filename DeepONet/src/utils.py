#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 15:12:47 2021

@author: robertplanas
"""
import numpy as np
from pyDOE import lhs 

def Generator(X, Y, Batch, nCol = 500, nCell = 32, width = 1):
    
    X_points = np.linspace(0,width, num = nCell)[None, :]
    Y_points = np.linspace(0,width, num = nCell)[None, :]
    
    xy_dat = np.reshape(np.asarray(np.meshgrid(X_points, Y_points)).T, (nCell*nCell, 2))
    xy_Col = lhs(2, samples = nCol)
    X_Input = np.zeros((Batch, nCell, nCell))
    Y_Output = np.zeros((Batch, nCell* nCell))
    # print basic info
    print('--------------------------------')
    print('generator:')
    print('{} Boundary points'.format((nCell - 1)*4))
    print('{} data points'.format(xy_dat.shape[0]))
    print('{} collocation points'.format(xy_Col.shape[0]))
    print('--------------------------------')
   
    # set bc and label per solution
    gBegin = 0
    gBatchBegin = gBegin
    gEnd = X.shape[0]
    while 1:
      if gBatchBegin + Batch > gEnd: gBatchBegin = gBegin
      X_Input[:,:,:] = X[gBatchBegin: gBatchBegin + Batch, :, : ]
      Y_Output[:,:] = np.reshape(Y[gBatchBegin: gBatchBegin + Batch, :, : ] , (Batch, nCell*nCell))
      gBatchBegin += Batch
      yield [X_Input, xy_dat, xy_Col], Y_Output
    
def Generator_NS(X, Y, Batch, nCol = 500, nCell = 33, width = 0.5, Variables = 3):
    
    X_points = np.linspace(0,width, num = nCell)[None, :]
    Y_points = np.linspace(0,width, num = nCell)[None, :]
    
    xy_dat = np.flip(np.reshape(np.asarray(np.meshgrid(X_points, Y_points)).T, (nCell*nCell, 2)), axis = -1)
    xy_Col = lhs(2, samples = nCol)
    X_Input = np.zeros((Batch, nCell, nCell, Variables ))
    Y_Output = np.zeros((Batch, nCell* nCell, Variables))
    # print basic info
    print('--------------------------------')
    print('generator:')
    print('{} Boundary points'.format((nCell - 1)*4))
    print('{} data points'.format(xy_dat.shape[0]))
    print('{} collocation points'.format(xy_Col.shape[0]))
    print('--------------------------------')
   
    # set bc and label per solution
    gBegin = 0
    gBatchBegin = gBegin
    gEnd = X.shape[0]
    while 1:
      if gBatchBegin + Batch > gEnd: gBatchBegin = gBegin
      X_Input[:,:,:,:] = X[gBatchBegin: gBatchBegin + Batch, :, : ,:Variables]
      Y_Output[:,:,:] = np.reshape(Y[gBatchBegin: gBatchBegin + Batch, :, :, :Variables ] , (Batch, nCell*nCell,Variables))
      gBatchBegin += Batch
      yield [X_Input, xy_dat, xy_Col], Y_Output
    
    