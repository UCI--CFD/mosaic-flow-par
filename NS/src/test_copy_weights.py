import sys
sys.path.append('../src')
import numpy as np
import tensorflow as tf
from tensorflow import keras
import NS_model as NSModel
from NS_dataset import *
import argparse

# load data
dataSet = NSDataSet()
#dataSet.add_file('../data/channel_genome_r1.h5')
dataSet.add_file('../data/cavity_genome_64_Re500-1500.h5')
dataSet.load_data()
dataSet.summary()

# create training and validation set
nUsed  = dataSet.num_genome()
nValid = int(nUsed*0.1) 
nTrain = nUsed - nValid
nGenomePerBatch = 64
print('{} genomes in training, {} in validation, {} per batch'.format(\
      nTrain, nValid, nGenomePerBatch))
# replica data generator
trainGen = dataSet.generator_bcXybcRe_xy_label(0, nTrain, nGenomePerBatch, 1, 1)
validGen = dataSet.generator_bcXybcRe_xy_label(nTrain, nUsed, nGenomePerBatch, 1, 1)

with NSModel.strategy.scope():
  nsNet = NSModel.NSModelMLP()
  nsNet.compile(optimizer=keras.optimizers.Adam(learning_rate=5.0e-4))

modelName = 'useless'
ckpntName = 'test_mlp4_backup'
nsNet.load_weights(tf.train.latest_checkpoint(ckpntName))
nsCB = [keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.8, min_delta=0.01,\
                                          patience = 400, min_lr=1.0e-8),
        keras.callbacks.CSVLogger(modelName+'.log')]

nsNet.fit(trainGen, validation_data=validGen,\
          epochs=1,\
          steps_per_epoch=nTrain//nGenomePerBatch,\
          validation_steps=nValid//nGenomePerBatch,\
          verbose=2, callbacks=nsCB)

with NSModel.strategy.scope():
  nsNet1 = NSModel.NSModelPinn(beta=[0.0, 0.0, 0.0])
  nsNet1.compile(optimizer=keras.optimizers.Adam(learning_rate=1.0e-7))

dataSet.init_collocation_points(500)
trainGen = dataSet.generator_bcXybcRe_xy_w_label(0, nTrain, nGenomePerBatch, separateRe=True)
validGen = dataSet.generator_bcXybcRe_xy_w_label(nTrain, nUsed, nGenomePerBatch, separateRe=True)

modelName = 'ns'
nsCB = [keras.callbacks.ModelCheckpoint(filepath='./'+modelName+'/checkpoint', \
                                        monitor='val_loss', save_best_only=True,\
                                        verbose=1),
        keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.8, min_delta=0.01,\
                                          patience = 400, min_lr=1.0e-8),
        keras.callbacks.CSVLogger(modelName+'.log')]

nsNet1.fit(trainGen, validation_data=validGen,\
          epochs=1,\
          steps_per_epoch=nTrain//nGenomePerBatch,\
          validation_steps=nValid//nGenomePerBatch,\
          verbose=2, callbacks=nsCB)

print(len(nsNet.mlp.layers))
print(len(nsNet1.mlp.layers))
for i, l in enumerate(nsNet.mlp.layers):
  nsNet1.mlp.layers[i].set_weights(l.get_weights())

nsNet1.fit(trainGen, validation_data=validGen,\
          epochs=2,\
          steps_per_epoch=nTrain//nGenomePerBatch,\
          validation_steps=nValid//nGenomePerBatch,\
          verbose=2, callbacks=nsCB)