#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 09:25:02 2021

@author: robertplanas
"""

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', default='ns_nop_a1-1-0.5_r-10-256x3-128x3-96x3-64x3-32x2-3',
                    help='Select a model')
parser.add_argument('-d', '--data', default="../data/step_cavity.vtk",
                    help='Select a data file')
parser.add_argument('-l', '--arch', type=int, nargs='*', help='architecture',
                    default=[256, 256, 256, 128, 128, 128, 96, 96, 96, 64, 64, 64, 32, 32, 3])
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')

parser.add_argument('-g', '--mosaic', type=int, default=0,
                    help='Use denser mosaic: 0 - Regular, 1- Double Density')
parser.add_argument('-print', '--print', default=True,
                    help='Print to a file if true')

parser.add_argument('-plot', '--plot', default=True, action='store_true',
                    help='Plot the solution if true')

parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',
                    help='reproduce the comparison in paper')

args = parser.parse_args()


if(args.reproduce):
    args.model = 'ns_nop_a1-1-0.5_r-10-256x3-128x3-96x3-64x3-32x2-3'
    args.data = "../data/step_cavity.vtk"
    args.arch = [256, 256, 256, 128, 128, 128,
                 96, 96, 96, 64, 64, 64, 32, 32, 3]
    args.mosaic = 0
    args.plot = True
    args.print = True


GPU = None
if(GPU is not None):
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = GPU

import os
import sys
import numpy as np
import tensorflow as tf
sys.path.append('../NS_Genomes_src/')
import Genomes_NS as G_NS
sys.path.append('../src/')
#import Empty_Model_NS as EM
import NS_model as NSModel
import time
import vtk as vtk
from vtk.util.numpy_support import vtk_to_numpy
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable

# assert that the data is a .vtk file
assert args.data[-4:] == ".vtk", "Invalid file "
vtkFile = args.data
assert args.mosaic == 1 or args.mosaic == 0, "Invalid Mosaic"
Grid = []
if (args.mosaic == 1):
    Grid = [4]


# =============================================================================
# Print File
# =============================================================================

def Print_vector_File(File_Name, Vector, Vector_Name):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt, "a")
    file1.write(Vector_Name + " = [")
    for index, element in enumerate(Vector):
        if(index == len(Vector) - 1):
            file1.write(str(element) + "]" + "\n")
        else:
            file1.write(str(element) + ',')
    file1.close()


def Print_String_File(File_Name, String):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt, "a")
    file1.write(String + " \n")
    file1.close()


#%% =============================================================================
# Load Model
# =============================================================================

modelPath = os.path.join(os.path.dirname(__file__), '../models/')
modelName = args.model
if "nsbc" in args.model:
  nsNet = NSModel.NSModelHardBC(width=args.arch, lastLinear=True, alpha=[1.0, 1.0, 0.5], omitP=True)
elif "deepO" in args.model:
  nsNet = NSModel.NSModelDeepONet(branch=args.arch)
  # this toler leads to the most accurate of DeepONet, better than 1e-10
elif "ns_nop" in args.model:
  nsNet = NSModel.NSModelMLP(width=args.arch, lastLinear=True)

nsNet.load_weights(tf.train.latest_checkpoint(modelPath + modelName)).expect_partial()

# =============================================================================
# Load the data into a matrix
# =============================================================================
reader = vtk.vtkUnstructuredGridReader()
reader.SetFileName(vtkFile)
reader.Update()
vtkData = reader.GetOutput()
# check # points
nPnt = int(vtkData.GetNumberOfPoints() / 2)

p = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
U = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
pntxyz = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPoints().GetData())


Coords_Block = np.zeros((257, 257, 2)) * np.nan
Block = np.zeros((257, 257, 3)) * np.nan

for i in range(np.shape(pntxyz)[0]):
    x = pntxyz[i, 0]
    y = pntxyz[i, 1]
    I = y * 128 + 128
    J = x * 128
    Coords_Block[int(I), int(J), :] = [x, y + 1]
    Block[int(I), int(J), :2] = U[i, :2]
    Block[int(I), int(J), 2] = p[i]


# coarse the grid
x0 = 0
y0 = 0
Width = 2
Height = 2
U = Block[x0:x0 + int(Width * 2 * 65):2, y0:y0 + int(Height * 2 * 65):2, :]
Coords = Coords_Block[x0:x0 +
                      int(Width * 2 * 65):2, y0:y0 + int(Height * 2 * 65):2, :]

# =============================================================================
# Extract the domain shape
# =============================================================================

Domain_Shape = np.ones_like(U)[:, :, 0]
for i in range(np.shape(U)[0]):
    for j in range(np.shape(U)[1]):
        if(np.isnan(U[i, j, 0])):
            Domain_Shape[i, j] = np.nan

U_Int_Nan = G_NS.Extract_Border_Domain(U)

# =============================================================================
# Place the genomes
# =============================================================================
# First you have to make sure that the prediction for one genome works
Genomes, Shared_Borders = G_NS.Generate_Genomes_Automatically(Width=Width, Height=Height, Genome_width=0.5,
                                                              Points_Borders=33, Middle_Overlapping=True,
                                                              Domain_Interior_Ones=Domain_Shape,
                                                              Domain_Int_Nan=U_Int_Nan, Coords=Coords, Finner_Grid=Grid)

# =============================================================================
# Solve the genomes
# =============================================================================

Start_Time = time.time()
MAE_U_x, MAE_U_y, MAE_P, Convergences, Solution = \
    G_NS.Iterative_Solution_Optimized(Shared_Borders, Genomes, nsNet, U, Iterations=800)

Elapsed_Time = time.time() - Start_Time

# =============================================================================
# Prints
# =============================================================================


print("Final resutls -----------")
print("MAE_U_x = ", MAE_U_x)
print("MAE_U_y = ", MAE_U_y)
print("Elapsed Time = ", Elapsed_Time)

Name = vtkFile[31:-4] + "_Grid_" + str(Grid)
if(args.print):
    Print_String_File('Test_Iterative_NS_step_cavity', Name)
    Print_vector_File('Test_Iterative_NS_step_cavity', [MAE_U_x], "MAE_U_x")
    Print_vector_File('Test_Iterative_NS_step_cavity', [MAE_U_y], "MAE_U_y")
    Print_vector_File('Test_Iterative_NS_step_cavity',
                      [Elapsed_Time], "Elapsed_Time")

if(args.plot):
    U_Magnitude = np.sqrt(
        np.square(U[:, :, 0]) + np.square(U[:, :, 1]))
    Solution_Magnitude = np.sqrt(
        np.square(Solution[:, :, 0]) + np.square(Solution[:, :, 1]))
    '''
    plt.rcParams.update({
        "text.usetex": False,
        "font.family": "sans-serif",
        "font.sans-serif": ["Helvetica"],
        'font.size': 14})
    '''

    # Plot the ground truth
    plt.figure()
    ax = plt.gca()
    im2 = plt.imshow(np.flip(np.flip(U_Magnitude, axis=1)),
                     cmap='rainbow', extent=[0, 2, 0, 2], aspect=1)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cbar = plt.colorbar(im2, cax=cax, ticks=[0, 0.5, 1])
    plt.clim(0, 1)
    plt.savefig('Ground_Truth_Velocity_Magnitude_step_cavity.pdf', format='pdf', bbox_inches='tight')

    # Plot the solved solution
    plt.figure()
    ax = plt.gca()
    im2 = plt.imshow(np.flip(np.flip(Solution_Magnitude, axis=1)),
                     cmap='rainbow', extent=[0, 2, 0, 2], aspect=1)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cbar = plt.colorbar(im2, cax=cax, ticks=[0, 0.5, 1])
    plt.clim(0, 1)
    plt.savefig('Velocity_Magnitude_step_cavity.pdf', format='pdf', bbox_inches='tight')

    nx = 129
    ny = 129
    x = np.linspace(0.0, 2.0, nx)
    y = np.linspace(0.0, 2.0, ny)
    X, Y = np.meshgrid(x, y)
    # Plot the streamlines for the ground truth
    plt.figure()
    ax = plt.gca()
    plt.streamplot(x[:], y[:], U[:, :, 0], U[:, :, 1],
                   color=U_Magnitude, cmap='rainbow', density=1.5)
    ax.set_aspect(aspect=1)
    plt.xlim(((0, 2)))
    plt.ylim(((0, 2)))
    plt.clim(0, 1)
    plt.savefig('Streamline_Ground_Truth_step_cavity.pdf', format='pdf', bbox_inches='tight')

    # Plot the streamlines for the MFP Solution
    plt.figure()
    ax = plt.gca()
    plt.streamplot(x[:], y[:], Solution[:, :, 0], Solution[:, :, 1],
                   color=Solution_Magnitude, cmap='rainbow', density=1.5)
    ax.set_aspect(aspect=1)
    plt.xlim(((0, 2)))
    plt.ylim(((0, 2)))
    plt.clim(0, 1)
    plt.savefig('Streamline_step_cavity.pdf', format='pdf', bbox_inches='tight')
