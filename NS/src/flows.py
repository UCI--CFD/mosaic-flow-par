import sys
import numpy as np
import copy as cp
import vtk
from vtk.util.numpy_support import vtk_to_numpy
#
from block import *
from genome import *
from halo_point import *

class SingleBlockFlow:
  def __init__(self, Re=200):
    self.Re = Re
    self.U  = 1.0
    self.vtkFile = ''

  def apply_bc(self, blocks):
    return

  def init(self, blocks):
    return

  def read_coordinate_from_vtk(self, blocks, vtkFile, verbose=False, var=False):
    assert len(blocks) == 1
    # create vtk reader
    self.vtkFile = vtkFile
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(vtkFile)
    reader.Update()
    vtkData = reader.GetOutput()
    # check # points
    nPnt = int(vtkData.GetNumberOfPoints() / 2)
    if verbose:
      print('Find {} points in {}'.format(nPnt, vtkFile))
      print('Block 0 has {} points.'.format(blocks[0].nPoint))
      assert(nPnt == blocks[0].nPoint)
    # first get vtkPoints ojbect, then get points data array
    # this ib a large array of (x, y, z), including dummay in Z
    pntxyz = vtk_to_numpy(vtkData.GetPoints().GetData())
    # block 0 coordinates
    s, e = 0, blocks[0].nPoint
    coordsShape = (blocks[0].shape[0], blocks[0].shape[1], 2)
    blocks[0].coords = np.reshape(pntxyz[s:e,:2], coordsShape)
    # read variables if specified
    if var:
      p = vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
      U = vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
      nPnt     =  blocks[0].nPoint
      Uref     =  self.Re * 1.0e-3
      varShape = (blocks[0].shape[0], blocks[0].shape[1])
      blocks[0].vars[:, :,  2] = p[:nPnt].reshape(varShape)/Uref/Uref
      varShape = (blocks[0].shape[0], blocks[0].shape[1], 2)
      blocks[0].vars[:, :, :2] = U[:nPnt, :2].reshape(varShape)/Uref
    return

  def create_genome(self, blocks, gShape, nShift=(1, 1), verbose=True):
    assert len(blocks)  == 1
    assert len(blocks[0].shape) == len(gShape)
    # the size of block has to be divisible over genomes
    # because I only trained network for specific sizes
    assert (blocks[0].shape[0]-1) % (gShape[0]-1) == 0
    assert (blocks[0].shape[1]-1) % (gShape[1]-1) == 0
    # create genomes in batches
    genomes = []
    if verbose:
      print('------------------------------------------------')
      print('creating {} sets of genomes'.format(1+nShift[0]+nShift[1]))
    # set up shifts in i and j, or say x and y
    iShifts = [(i+1) * (gShape[0]//(nShift[0]+1)) for i in range(nShift[0])]
    jShifts = [(j+1) * (gShape[1]//(nShift[1]+1)) for j in range(nShift[1])]
    # create genomes
    self.create_genome_set(blocks, gShape, genomes)
    if verbose:
      print('create set 0, {} genomes'.format(len(genomes[-1])))
    for i, iShift in enumerate(iShifts):
      self.create_genome_set(blocks, gShape, genomes, iShift=iShift)
      if verbose:
        print('create set {}, {} genomes'.format(i+1, len(genomes[-1])))
    for j, jShift in enumerate(jShifts):
      self.create_genome_set(blocks, gShape, genomes, jShift=jShift)
      if verbose:
        print('create set {}, {} genomes'.format(j+1+len(iShifts), len(genomes[-1])))
    if verbose:
      print('------------------------------------------------')
    return genomes

  def create_genome_set(self, blocks, gShape, genomes, \
                        iShift=0, jShift=0, verbose=True):
    genomes.append([])
    b = blocks[0]
    nG = [b.nGenome[0], b.nGenome[1]]
    if iShift > 0: nG[0] -= 1
    if jShift > 0: nG[1] -= 1
    iRange = np.linspace(iShift, b.shape[0]-gShape[0]-iShift, nG[0], dtype=int)
    jRange = np.linspace(jShift, b.shape[1]-gShape[1]-jShift, nG[1], dtype=int)
    for ib in iRange:
      for jb in jRange:
        g = Genome(shape=gShape, Re=self.Re)
        gRng = (0, 0, gShape[0], gShape[1])
        bRng = (ib, jb, ib+gShape[0], jb+gShape[1])
        g.add_map(0, bRng, gRng)
        if verbose:
          print('genome map {}-{} {}-{} from block {}-{} {}-{}'\
                .format(gRng[0], gRng[2], gRng[1], gRng[3], \
                        bRng[0], bRng[2], bRng[1], bRng[3]))
        # set inference range
        if iShift > 0:
          bRng = (ib+gShape[0]//2, jb+1, ib+gShape[0]//2+1, jb+gShape[1]-1)
          gRng = (gShape[0]//2, 1, gShape[0]//2+1, gShape[1]-1)
          g.add_infer_map(0, bRng, gRng)
        elif jShift > 0:
          bRng = (ib+1, jb+gShape[1]//2, ib+gShape[0]-1, jb+gShape[1]//2+1)
          gRng = (1, gShape[1]//2, gShape[0]-1, gShape[1]//2+1)
          g.add_infer_map(0, bRng, gRng)
        elif iShift == 0 and jShift == 0:
          bRng = (bRng[0]+1, bRng[1]+1, bRng[2]-1, bRng[3]-1)
          gRng = (gRng[0]+1, gRng[1]+1, gRng[2]-1, gRng[3]-1)
          g.add_infer_map(0, bRng, gRng)
        genomes[-1].append(g)
        if verbose:
          print('genome infer {}-{} {}-{} from block {}-{} {}-{}'\
                .format(gRng[0], gRng[2], gRng[1], gRng[3], \
                        bRng[0], bRng[2], bRng[1], bRng[3]))

  def advance(self, blocks0, genomes, nn, blocks, \
              printResidual=False, writeVTK=False, \
              vtkFile=None, separateRe=False):
    # blocks0  -  input blocks
    # genomes  -  genomes
    # nn       -  neural networks for prediction
    # blocks   -  output blocks
    for g in genomes:
      g.extract_from_block(blocks0)
    for g in genomes:
      inputs = g.create_nn_input(separateRe)
      assert len(g.inferMaps) == 1
      m = g.inferMaps[0]
      shape  = (m.gRng[2]-m.gRng[0], m.gRng[3]-m.gRng[1], 3)
      g.vars[m.gRng[0]:m.gRng[2],m.gRng[1]:m.gRng[3],:] = \
        np.reshape(nn.predict(inputs), shape)
      # g.predict(nn)
      g.copy_to_block(blocks)
    # self.apply_bc(blocks)
    return compute_residual(blocks0, blocks, verbose=printResidual)


  def output_vtk(self, blocks, vtkFile):
    assert len(blocks) == 1
    # read an unstructured grid including all cells and variables
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(self.vtkFile)
    reader.Update()
    uGrid = reader.GetOutput()
    # change the values of cells
    p = vtk_to_numpy(uGrid.GetPointData().GetArray('p'))
    U = vtk_to_numpy(uGrid.GetPointData().GetArray('U'))
    nPnt     =  blocks[0].nPoint
    p[:nPnt] = blocks[0].vars[:, :, 2].reshape((nPnt))
    p[nPnt:] = blocks[0].vars[:, :, 2].reshape((nPnt))
    U[:nPnt, :2] = blocks[0].vars[:, :, :2].reshape((nPnt,2))
    U[:nPnt,  2] = 0.0
    U[nPnt:, :2] = blocks[0].vars[:, :, :2].reshape((nPnt,2))
    U[nPnt:,  2] = 0.0
    # write the grid in vtk format
    writer = vtk.vtkUnstructuredGridWriter()
    writer.SetFileName(vtkFile)
    writer.SetInputData(uGrid)
    writer.Write()

  def output_openfoam(self, blocks):
    assert len(blocks) == 1
    return


#----------------------------------------------------------------
#Channel Flow
#----------------------------------------------------------------

class ChannelLaminar(SingleBlockFlow):
  def __init__(self, Re=200, **kwargs):
    super(ChannelLaminar, self).__init__(**kwargs)
    self.Re = Re
    self.U  = 1.0
    self.vtkFile = ''

  def apply_bc(self, blocks):
    assert len(blocks) == 1
    #### velocity
    set_halo_fixed_value(blocks[0].vars, [0,1], 'x-', [self.U,0])
    set_halo_zero_gradient(blocks[0].vars, [0,1], 'x+')
    set_halo_fixed_value(blocks[0].vars, [0,1], 'y-', [0,0])
    set_halo_fixed_value(blocks[0].vars, [0,1], 'y+', [0,0])
    #### pressure
    set_halo_zero_gradient(blocks[0].vars, [2], 'x-')
    set_halo_fixed_value(blocks[0].vars, [2], 'x+', [0])
    set_halo_zero_gradient(blocks[0].vars, [2], 'y-')
    set_halo_zero_gradient(blocks[0].vars, [2], 'y+')

  def init(self, blocks):
    assert len(blocks) == 1
    assert blocks[0].nVar == 3
    blocks[0].vars[:,:,0] = self.U
    blocks[0].vars[:,:,1] = 0
    blocks[0].vars[:,:,2] = 0

  def output_openfoam(self, blocks):
    URef = self.Re / 1000
    # velocity
    rfile = open('channel_U_header', 'r')
    wfile = open('U', 'w')
    for i, line in enumerate(rfile):
      if i == 23: line = line.replace('0.1', '{:2f}'.format(self.U*URef))
      wfile.write(line)
    rfile.close()
    # total cell
    totCell = 0
    for b in blocks: totCell += b.nCell
    # internal field title line
    wfile.write('\ninternalField nonuniform List<vector>\n')
    wfile.write(repr(totCell) + '\n')
    wfile.write('(\n')
    for b in blocks:
      for i in range(b.shape[0]-1):
        for j in range(b.shape[1]-1):
          wfile.write('({:.8f} {:.8f} 0.0)\n'.format(
            0.25*(b.vars[i,j,0]+b.vars[i+1,j,0]+b.vars[i,j+1,0]+b.vars[i+1,j+1,0])*URef,
            0.25*(b.vars[i,j,1]+b.vars[i+1,j,1]+b.vars[i,j+1,1]+b.vars[i+1,j+1,1])*URef))
    wfile.write(');\n')
    wfile.close()
    # pressure
    rfile = open('channel_p_header', 'r')
    wfile = open('p', 'w')
    for i, line in enumerate(rfile): wfile.write(line)
    rfile.close()
    # internal field title line
    wfile.write('\ninternalField nonuniform List<scalar>\n')
    wfile.write(repr(totCell) + '\n')
    wfile.write('(\n')
    for b in blocks:
      for i in range(b.shape[0]-1):
        for j in range(b.shape[1]-1):
          wfile.write('{:2f}\n'.format(
            0.25*(b.vars[i,j,2]+b.vars[i+1,j,2]+b.vars[i,j+1,2]+b.vars[i+1,j+1,2])*URef*URef))
    wfile.write(');\n')
    wfile.close()

#----------------------------------------------------------------
#Cavity Flow
#----------------------------------------------------------------

class LidDrivenCavity(SingleBlockFlow):
  def __init__(self, Re=200, **kwargs):
    super(LidDrivenCavity, self).__init__(**kwargs)
    self.Re = Re
    self.U  = 1.0
    self.vtkFile = ''

  def apply_bc(self, blocks):
    assert len(blocks) == 1
    #### velocity
    #set_halo_fixed_value(blocks[0].vars, [0,1], 'x-', [0,0])
    #set_halo_fixed_value(blocks[0].vars, [0,1], 'x+', [0,0])
    #set_halo_fixed_value(blocks[0].vars, [0,1], 'y-', [0,0])
    #set_halo_fixed_value(blocks[0].vars, [0,1], 'y+', [self.U,0])
    #### pressure
    set_halo_zero_gradient(blocks[0].vars, [2], 'x-')
    set_halo_zero_gradient(blocks[0].vars, [2], 'x+')
    set_halo_zero_gradient(blocks[0].vars, [2], 'y-')
    set_halo_zero_gradient(blocks[0].vars, [2], 'y+')

  def init(self, blocks):
    assert len(blocks) == 1
    assert blocks[0].nVar == 3
    blocks[0].vars[:,:,0] = 0
    blocks[0].vars[:,:,1] = 0
    blocks[0].vars[:,:,2] = 0


#----------------------------------------------------------------
# Test for cavity with unseen geometry
#----------------------------------------------------------------


class LidDrivenCavityUnseen():
  def __init__(self, Re=500):
    self.Re = Re
    self.U  = lambda x: (1.0 - (2.0*x-1)**8)**2


  def read_vtk(self, blocks, vtkFile, verbose=False):
    assert len(blocks) == 3

    # create vtk reader
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(vtkFile)
    reader.Update()
    vtkData = reader.GetOutput()
    # check # points
    nPnt = int(vtkData.GetNumberOfPoints() / 2)
    nPntTmp = blocks[0].nPoint + blocks[1].nPoint \
            - blocks[0].shape[0] + blocks[2].nPoint \
            - blocks[1].shape[1]
    assert(nPnt == nPntTmp)
    if verbose:
      print('Find {} points in {}'.format(nPnt, vtkFile))
      print('Block 0 has {} points.'.format(blocks[0].nPoint))
      print('Block 1 has {} points, sharing {} points with block 0.'\
           .format(blocks[1].nPoint, blocks[0].shape[0]))
      print('Block 2 has {} points, sharing {} points with block 1.'\
           .format(blocks[2].nPoint, blocks[1].shape[1]))

    # first get vtkPoints ojbect, then get points data array
    # this is a large array of (x, y, z), including dummay in Z
    pntxyz = vtk_to_numpy(vtkData.GetPoints().GetData())
    # Get points' data including (u,v) and p
    p = vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
    U = vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
    # block 0 coordinates and variables
    s,  e  = 0, blocks[0].nPoint
    ni, nj = blocks[0].shape[0], blocks[0].shape[1]
    blocks[0].coords[...]  = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
    blocks[0].vars[:,:,:2] = np.reshape(U[s:e,:2], (ni,nj,2))
    blocks[0].vars[:,:,2]  = np.reshape(p[s:e], (ni,nj))
    # block 1, copy the sharing points from block 0
    blocks[1].coords[:,0,:] = blocks[0].coords[:,-1,:]
    blocks[1].vars[:,0,:]   = blocks[0].vars[:,-1,:]
    # block 1 coordinates and variables, excluding shared border
    s = 2*blocks[0].nPoint
    e = s + blocks[1].nPoint - blocks[0].shape[0]
    ni, nj = blocks[1].shape[1], blocks[1].shape[1]-1
    blocks[1].coords[:,1:,:] = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
    blocks[1].vars[:,1:,:2]  = np.reshape(U[s:e,:2], (ni,nj,2))
    blocks[1].vars[:,1:,2]   = np.reshape(p[s:e], (ni,nj))
    # block 2, copy the sharing points from block 1
    blocks[2].coords[-1,:,:] = blocks[1].coords[0,:,:]
    blocks[2].vars[-1,:,:]   = blocks[1].vars[0,:,:]
    blocks[2].vars[-1,:,:]   = blocks[1].vars[0,:,:]
    # block 2 coordinates and variables, excluding shared border
    s = 2*(blocks[0].nPoint + blocks[1].nPoint - blocks[0].shape[0])
    e = s + blocks[2].nPoint - blocks[1].shape[1]
    ni, nj = blocks[2].shape[0]-1, blocks[2].shape[1]
    blocks[2].coords[:-1,:,:] = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
    blocks[2].vars[:-1,:,:2]   = np.reshape(U[s:e,:2], (ni,nj,2))
    blocks[2].vars[:-1,:, 2]   = np.reshape(p[s:e], (ni,nj))


  def apply_bc(self, blocks):
    # velocity
    blocks[0].vars[0,:,:2] = 0.0
    blocks[0].vars[-1,:,0] = np.fromiter(\
                             map(self.U, blocks[0].coords[-1,:,0]),
                             dtype=float)
    blocks[0].vars[-1,:,1] = 0.0
    blocks[0].vars[:,0,:]  = 0.0
    blocks[1].vars[-1,:,0] = np.fromiter(\
                             map(self.U, blocks[1].coords[-1,:,0]),
                             dtype=float)
    blocks[1].vars[-1,:,1] = 0.0
    blocks[1].vars[:,-1,:2]= 0.0
    blocks[2].vars[0,:,:2] = 0.0
    blocks[2].vars[:,0,:2] = 0.0
    blocks[2].vars[:,-1,:2]= 0.0
    # pressure
    blocks[0].vars[0,:,2]   = blocks[0].vars[1,:,2]
    blocks[0].vars[-1,:,2]  = blocks[0].vars[-2,:,2]
    blocks[0].vars[:,0,2]   = blocks[0].vars[:,1,2]
    blocks[1].vars[-1,:,2]  = blocks[1].vars[-2,:,2]
    blocks[1].vars[:,-1,2]  = blocks[1].vars[:,-2,2]
    blocks[2].vars[0,:,2]   = blocks[2].vars[1,:,2]
    blocks[2].vars[:,0,2]   = blocks[2].vars[:,1,2]
    blocks[2].vars[:,-1,2]  = blocks[2].vars[:,-2,2]
    # copy shared borders
    blocks[1].vars[:,0,:] = blocks[0].vars[:,-1,:]
    blocks[1].vars[0,:,:] = blocks[2].vars[-1,:,:]


  def advance(self, blocks0, gSet, nn, blocks, \
              printResidual=False):
    # blocks0  -  input blocks
    # genomes  -  genomes
    # nn       -  neural networks for prediction
    # blocks   -  output blocks
    for g in gSet:
      g.extract_from_block(blocks0)
    for g in gSet:
      inputs = g.create_nn_input()
      shape  = (g.shape[0]-2, g.shape[1]-2, 3)
      g.vars[1:-1,1:-1,:] = np.reshape(nn.predict(inputs), shape)
      g.copy_to_block(blocks)
    # self.apply_bc(blocks)
    return compute_residual(blocks0, blocks, verbose=printResidual)
