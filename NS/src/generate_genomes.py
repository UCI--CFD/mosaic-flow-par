import numpy as np
import scipy.sparse as sparse
import matplotlib.pyplot as plt
import os
import h5py as h5
import argparse
from block import *
from genome import *

parser = argparse.ArgumentParser()
# vtk files to extract
parser.add_argument('-f', '--filePath', help='vtk file path', \
                    default='../data/channel_profile_vtk')
parser.add_argument('-s', '--fileStart', type=int, default=0, help='start file')
parser.add_argument('-e', '--fileEnd', type=int, default=10, help='end file')
# size of blocks and genomes
parser.add_argument('-g', '--genomeSize', nargs=2, type=int, \
                    default=[33,33], help='genome size')
parser.add_argument('-b', '--blockSize', nargs=2, type=int, \
                    default=[129,129], help='block size')
parser.add_argument('-r', '--rotate', type=int, default=0, help='# rotations')
# extraction positions
parser.add_argument('-start', '--start', nargs=2, type=int, \
                    default=[0,0], help='extraction start')
parser.add_argument('-ng', '--nGenome', nargs=2, type=int, \
                    default=[5,5], help='# genomes')
parser.add_argument('-end', '--end', nargs=2, type=int, \
                    default=[64,64], help='extraction end')
# tolerance for sample difference
parser.add_argument('-toler', '--tolerance', type=float, \
                    default=2.0, help='tolerance for dist')
# whether evaluate difference between final samples
parser.add_argument('-eval', '--evaluate', default=False, \
                    action='store_true', help='evaluate min dist')
# output file names
parser.add_argument('-o', '--output', default='genomes.h5', help='output file')
args = parser.parse_args()

# start, end indices and steps to extract genomes
start = args.start
end   = args.end
nG    = args.nGenome
stride= 2
toler = np.zeros((nG[0], nG[1]))
nGMat = np.zeros((nG[0], nG[1]), dtype=int)
# for cavity
# toler[...] = args.tolerance
# toler[:-1,:] = 0.2
# for cavityw2
# toler[...] = 0.4
# toler[:4,:4] = 0.3
# toler[:3,:2] = 0.2
# toler[0,0] = 0.1
# for cavityh2
toler[-4:,:] = 0.45
toler[2:-4,:] = 0.1
toler[0:2,:] = 0.02

gSize = args.genomeSize
blocks = [Block(args.blockSize)]
gSet   = []

# generate a list of all vtk files
vtkFiles = os.listdir(args.filePath)
vtkFiles.sort()

# extract genomes
for m, vtkFile in enumerate(vtkFiles[args.fileStart:args.fileEnd]):
  # load data into blocks
  read_vtk(blocks, args.filePath+'/'+vtkFile)
  assert abs(blocks[0].vars[0,0,2]) < 1e-8
  blocks[0].vars[:,:,2] -= blocks[0].vars[0,0,2]
  if args.rotate > 0:
    b = rotate_by_90(blocks[0], args.rotate)
    blocks.clear()
    blocks.append(b)
  # extract genome satisfying the tolerance
  nGenome = len(gSet)
  iStarts = np.linspace(start[0], end[0], nG[0], dtype=int)
  jStarts = np.linspace(start[1], end[1], nG[1], dtype=int)
  for ii, i in enumerate(iStarts):
    for jj, j in enumerate(jStarts):
      g = Genome(gSize)
      bRng = [i, j, i+gSize[0]+(stride-1)*(gSize[0]-1), \
                    j+gSize[1]+(stride-1)*(gSize[1]-1)]
      gRng = [0, 0, gSize[0], gSize[1]]
      g.add_map(0, bRng, gRng)
      g.extract_from_block(blocks, s=stride)
      # check the distance to the included genomes
      satisfied = True
      for gg in gSet:
        d = np.sqrt(np.sum((gg.bc-g.bc)*(gg.bc-g.bc)))
        satisfied = d > toler[ii, jj]
        if not satisfied: break
      # discard and move on if this genome is too
      # similar to included genomes
      if not satisfied: #or g.compute_mass_flow()*h > 1e-4:
        continue # move on to next genome
      # include genome if passed previous check
      gSet.append(g)
      nGMat[ii,jj] += 1
      # print(bRng, g.coords[-1,-1,:])
  # print('extract {} genomes form {}'\
  #       .format(len(gSet)-nGenome, vtkFile))

# write current genomes to file
datafile  = h5.File(args.output, 'w')
datafile.attrs['genomeSize']  = np.array(gSize)
datafile.attrs['nGenome']     = len(gSet)
datafile.attrs['nFile']       = len(vtkFiles)
# set variables for coordinate and u v p
gCoordsData = datafile.create_dataset('Coordinates', \
                (len(gSet), gSize[0], gSize[1], 2), \
                compression='gzip', compression_opts=9, \
                dtype='float64')
gVarsData   = datafile.create_dataset('Variables', \
                (len(gSet), gSize[0], gSize[1], 3), \
                compression='gzip', compression_opts=9, \
                dtype='float64')
for i, g in enumerate(gSet):
  gCoordsData[i,...] = g.coords[...]
  gVarsData[i,...]   = g.vars[...]

# evaluate
if args.evaluate:
  # compute the max-min for each boundary point
  nBcPoint = 2 * (gSize[0] + gSize[1] - 2)
  bcMax    = np.zeros(nBcPoint*3) - 1000
  bcMin    = np.zeros(nBcPoint*3) + 1000
  for g in gSet:
    bcMax = np.maximum(bcMax, g.bc)
    bcMin = np.minimum(bcMin, g.bc)
  heights = bcMax - bcMin

  # compute the distance of the bcs
  # bcDistMat = sparse.dok_matrix((len(gSet), len(gSet)), dtype=float)
  # bcDistMat = np.zeros((len(gSet), len(gSet)))
  # for i in range(len(gSet)):
  #   for j in range(i+1, len(gSet)):
  #     d = np.sum( ((gSet[i].bc - gSet[j].bc) / (heights[:]+1e-10))
  #               * ((gSet[i].bc - gSet[j].bc) / (heights[:]+1e-10)))
  #     bcDistMat[i,j] = np.sqrt(d)
  #     bcDistMat[j,i] = np.sqrt(d)

  # # min dist
  # distMin = np.zeros(len(gSet))
  # for i in range(len(gSet)):
  #   bcDistMat[i,i] = 1000
  #   distMin[i] = np.amin(bcDistMat[i,:])
  # distMin[0] = np.amin(bcDistMat[0,1:].toarray())
  # for i in range(1, len(gSet)-1):
  #   distMin[i] = min(np.amin(bcDistMat[i,i+1:].toarray()), \
  #                    np.amin(bcDistMat[:i,i].toarray()))
  # distMin[-1] = np.amin(bcDistMat[:-1,-1].toarray())

  print('extract {} genomes'.format(len(gSet)))
  # print('sample distance {:.4e} {:.4e}'.format(\
  #       np.mean(distMin), np.std(distMin)))
  for i in range(nG[0]-1,-1,-1):
    for j in range(nG[1]):
      print('{:4d}'.format(nGMat[i,j]), end='')
    print()

  # plot value distribution
  plt.figure(figsize=(24,6))
  for g in gSet:
    plt.plot(g.bc[:], 'b-', lw=0.5)
  plt.plot(bcMax[:], 'r-')
  plt.plot(bcMin[:], 'g-')
  plt.xticks(np.arange(0, 385, 32), np.arange(0,385,32), \
             fontsize=16)
  plt.savefig('genome_range.png')