#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 19:27:12 2021

@author: robertplanas
"""

import numpy as np
import tensorflow as tf


class DenseLayers(tf.keras.layers.Layer):
  def __init__(self, width=[64,64,128], act='tanh', prefix='bc',\
               reg=None,last_linear = True, **kwargs):
    super(DenseLayers, self).__init__(**kwargs)
    assert len(width) > 0
    if reg != None:
      if len(reg) == 1:
        tmp = reg[0]
        reg = np.zeros(len(width))
        reg[:] = tmp
      else:
        assert len(reg) == len(width)
    else:
      reg = np.zeros(len(width))

    self.layers = []
    # rest layers
    for i, w in enumerate(width[:-1]):
      self.layers.append(tf.keras.layers.Dense(width[i], activation=act, \
                         kernel_regularizer=tf.keras.regularizers.l2(reg[i]),\
                         name=prefix+repr(i)))
    if last_linear:
      self.layers.append(tf.keras.layers.Dense(width[-1], \
                         kernel_regularizer=tf.keras.regularizers.l2(reg[-1]),\
                         name=prefix+repr(len(width)-1)))
    else:
      self.layers.append(tf.keras.layers.Dense(width[-1], activation=act, \
                         kernel_regularizer=tf.keras.regularizers.l2(reg[-1]),\
                         name=prefix+repr(len(width)-1)))


  def call(self, inputs):
    bc = inputs
    for layer in self.layers:
      bc = layer(bc)
    return bc


class NS_Model(tf.keras.Model):
    def __init__(self, width=[256,256,256,128,128,128,96,96,96,64,64,64,32,32,3],
                 lastLinear = True, reg = None, **kwargs):
        super(NS_Model, self).__init__(**kwargs)
        if reg == None:
          self.mlp = DenseLayers(width=width, prefix='bc', \
                                 last_linear=lastLinear)
        else:
          self.mlp = DenseLayers(width=width, reg=reg, prefix='bc', \
                                 last_linear=lastLinear)
            
        
        
    def call(self, inputs):
        '''
        inputs: [bcXybcRe, xyColloc]
        '''
        bcXybcReXy = tf.concat(inputs, axis=-1)
        return self.mlp(bcXybcReXy)
        
