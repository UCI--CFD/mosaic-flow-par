The directory `./NS/models` includes pre-trained model used in MF predictor for solving the NS equations

* ns_nop_a1-1-0.5_r-10-256x3-128x3-96x3-64x3-32x2-3
* deepO_a1-1-0.5-256x3-128x3-96x3
* nsbc_nop_a1-1-0.5-400x6-3



The directory `./NS/data/` includes the training data for GFNet and the ground truth used in evaluating MF predictor.



Next, we will demonstrate how to train a GFNet and run MF predictor with it for unseen domains under unseen boundary conditions (BCs). If not specified otherwise, we assume the scripts are run in the `./NS/src/` directory.



To train a GFNet from scratch, change to the `./NS/src/` diretory and run the script as follow:

```python
python gfnet.py -name ns_nop_a1-1-0.5_r-10 -m 0 \
  -l 256 256 256 128 128 128 96 96 96 64 64 64 32 32 3 \
  -alpha 1.0 1.0 0.5 -r 1e-10 \
  -d 961 -s 20 -omitP \
  -p 200 -lrmin 1e-7 -e 20000
```

The command-line arguments:

* `-name ns_nop_a1-1-0.5_r-10` specifies name of the model

* `-m 1` specifies type of the model, 0 is for data-only, 1 is for physics-informed, 2 is for DeepONet, and 3 is for BC enforcement.

* `-l 256 256 256 128 128 128 96 96 96 64 64 64 32 32 3` specifies the architecture for the fully connected layers

* `-alpha 1.0 1.0 0.5` specifies the coefficients timed to data loss of velocities and pressure,

* `-r 1e-10` specifies the coefficient used for regularization,

* `-d 961` specifies the number of data points,

* `-s 20` specifies the batch size (number of genomes), 

* `-p 200` specifies the patience for learning rate decay,

* `-lrmin 1e-7` specifies the minimum learning rate,

* `-e 20000` specifies the epochs for training,

  

The example command above will train the GFNet used in this paper. The training save the model as TensorFlow checkpoint and output the log of loss and MAE. 

To see all the command-line arguments, run `python gfnet.py --help`.



To run MF predictor with a pre-trained model for a lid-driven cavity with unseen BC, run the following script:

```python
python NS_Test_Single_Cavity.py 
	-m "ns_nop_a1-1-0.5_r-10-256x3-128x3-96x3-64x3-32x2-3" 
    -d "../data/square_cavity.vtk" 
    -l 256 256 256 128 128 128 96 96 96 64 64 64 32 32 3 
    -DataType "float32" 
    -g 1 
    -plot
```

The command-line arguments:

* `-m --model` selects the pre-trained deep learning model that solves the flow for a square domain. By default, this model must be stored under the path "../models/".
* `-d --data`  selects the file containing the ground truth for the square domain wished to solve. The format of the file must be ".vtk", contain the fields 'p' and 'U' and the points where these fields are recorded. Note that the python script automatically coarses the grid by a factor of two, so the original data must have (129,129) samples uniformly distributed on [0,1]x[0,1].
* `-l --arch` defines the architecture of the model. They must correspond to the specified model from the first input.
* `-DateType` selects the data type "float32" or "float64". It must match the data type of the model. 
* `-g  --mosaic` 0 or 1, selects the density of the mosaic, either using 9 genomes or 19 genomes.
* `-print` prints the results to a file if true.
* `-plot`  plots the solution magnitude and the streamline plot if true.

The outputs will be the following:

```
Final resutls -----------
MAE_U_x =  0.005687216791878595
MAE_U_y =  0.004962653329301839
Elapsed Time =  91.205970287323
```

Furthermore, as print == True and plot == True it will output the following plots

```
Streamlines_single_cavity.pdf
Velocity_Magnitude_single_cavity.pdf
Test_Iterative_NS_single_cavity.txt
```



To run MF predictor with the pre-trained model for step shape cavity with unseen BC, run the following script:

```python
python NS_Test_Step_Shape_Cavity.py 
  	-m "ns_nop_a1-1-0.5_r-10-256x3-128x3-96x3-64x3-32x2-3" 
    -l 256 256 256 128 128 128 96 96 96 64 64 64 32 32 3 
```

The command-line arguments are defined in the same way as `NS_Test_Single_Cavity.py`. 

You can also change the model to `deepO_a1-1-0.5-256x3-128x3-96x3` and `nsbc_nop_a1-1-0.5-400x6-3`.

This will print out the following:

```
Final resutls -----------
MAE_U_x =  0.013473389102050603
MAE_U_y =  0.012405793604771056
Elapsed Time =  302.7730519771576
```

Furthermore, it outputs the following figures

```
Streamline_step_cavity.pdf
Velocity_Magnitude_step_cavity.pdf
```

