import tensorflow as tf
from tensorflow import keras
import tensorflow.keras.layers as KL
import tensorflow.keras.regularizers as KR
import numpy as np
from pinn_utils import *

strategy = tf.distribute.MirroredStrategy()
    
#-------------------------------------------------------------------------------
# model subclasses, fully connected
# Input:  bc + xy, since geometry is fixed
# Output: p @ xy
#-------------------------------------------------------------------------------

class PsnFC(keras.Model):
  def __init__(self, nCell=32, width=[128,128,128,1], reg = None, \
               act='tanh', alpha=0.01, last_linear=False, \
               last_act='tanh', save_grad_stat=False, **kwargs):
    super(PsnFC, self).__init__(**kwargs)  
    # check inputs
    assert len(width) > 0
    if reg != None:
      assert len(reg) == len(width)
    # save inputs
    self.nCell   = nCell
    self.nBcCell = 4 * nCell
    self.alpha   = alpha
    self.act     = act
    self.lastAct = last_act
    self.width   = width
    self.reg     = np.zeros(len(width)) if reg==None else np.array(reg)
    self.save_grad_stat = save_grad_stat
    self.last_linear    = last_linear

    # ---- setup mlp ---- # 
    self.mlp = []
    # layers before last
    for i, w in enumerate(self.width[:-1]):
      self.mlp.append(KL.Dense(w, name='mlp'+repr(i), activation=act, \
                               kernel_regularizer=KR.l2(self.reg[i])))
    # last layer
    if last_linear:
      self.mlp.append(KL.Dense(width[-1], name='mlp'+repr(len(self.width)-1),\
                               kernel_regularizer=KR.l2(self.reg[-1])))
    else:
      self.mlp.append(KL.Dense(width[-1], name='mlp'+repr(len(self.width)-1),\
                               kernel_regularizer=KR.l2(self.reg[i]),\
                               activation=last_act))

    # ---- dicts for metrics and statistics ---- #
    self.trainMetrics = {}
    self.validMetrics = {}
    ## add metrics for layers' weights, if save_grad_stat is required
    ## i even for weights, odd for bias
    if self.save_grad_stat:
      for i in range(len(width)):
        names = ['dat_'+repr(i)+'w_avg', 'dat_'+repr(i)+'w_std',\
                 'dat_'+repr(i)+'b_avg', 'dat_'+repr(i)+'b_std',\
                 'pde_'+repr(i)+'w_avg', 'pde_'+repr(i)+'w_std',\
                 'pde_'+repr(i)+'b_avg', 'pde_'+repr(i)+'b_std']
        for name in names:
          self.trainMetrics[name] = keras.metrics.Mean(name='train '+name)
    ## loss, mae
    names = ['loss', 'dat', 'pde', 'mae']
    for key in names:
      self.trainMetrics[key] = keras.metrics.Mean(name='train '+key)
      self.validMetrics[key] = keras.metrics.Mean(name='valid '+key)
#    self.trainMetrics['mae'] = keras.metrics.Mean(name='train mae')
#    self.validMetrics['mae'] = keras.metrics.Mean(name='valid mae')
    # dict to save training and validation statuse 
    self.trainStat = {}
    self.validStat = {}
    

  def call(self, inputs, training=None):
    '''
    inputs: [p[batch,nBcCell], xy[batch,2]]
    '''
    bc = inputs[0]
    xy = inputs[1]
    bcxy = tf.concat([bc, xy], axis=-1)
    for layer in self.mlp:
      bcxy = layer(bcxy)
    return bcxy


  def record_layer_gradient(self, grads, baseName):
    '''
    record the average and standard deviation of each layer's 
    weights and biases
    '''
    for i, g in enumerate(grads):
      if g != None:
        l = i // 2
        parameter = 'w' if i%2==0 else 'b'
        prefix = baseName + '_{:d}{}_'.format(l,parameter) 
        gAbs = tf.abs(g)
        gAvg = tf.reduce_mean(gAbs)
        gStd = tf.reduce_mean(tf.square(gAbs - gAvg))
        self.trainMetrics[prefix+'avg'].update_state(gAvg)
        self.trainMetrics[prefix+'std'].update_state(gStd)


  def train_step(self, data):
    bcxyw, p = data
    bc, xy, w = bcxyw

    # ---- compute the gradients ---- #
    with tf.GradientTape(persistent=True) as tape0:
      # tape2: 2nd order derivative
      with tf.GradientTape(watch_accessed_variables=False,persistent=True) as tape2:
        tape2.watch(xy)
        # tape1: 1st order derivatives
        with tf.GradientTape(watch_accessed_variables=False) as tape1:
          tape1.watch(xy)
          pPred = self([bc, xy])
          pPred = tf.squeeze(pPred)
        g = tape1.gradient(pPred, xy)
        p_x = g[:,0]
        p_y = g[:,1]
      p_xx = tape2.gradient(p_x, xy)[:,0]
      p_yy = tape2.gradient(p_y, xy)[:,1]
      del tape2
      # bc mse + pde mse
      datMse = tf.reduce_sum(tf.multiply(w[:,0], tf.square(p - pPred)))\
             / (tf.reduce_sum(w[:,0]) + 1.0e-10)
      pdeMse = tf.reduce_sum(tf.multiply(w[:,1], tf.square(p_xx + p_yy)))\
             / (tf.reduce_sum(w[:,1]) + 1.0e-10)
      # replica's loss, divided by global batch size
      loss   = datMse + self.alpha * pdeMse
      loss  += tf.add_n(self.losses)
      loss   = loss / strategy.num_replicas_in_sync
    # update gradients and trainable variables
    if self.save_grad_stat:
      datGrad  = tape0.gradient(datMse, self.trainable_variables)
      pdeGrad  = tape0.gradient(pdeMse, self.trainable_variables)
    lossGrad = tape0.gradient(loss, self.trainable_variables)
    del tape0

    # ---- update parameters ---- #
    self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))

    # ---- update metrics and statistics ---- #
    # update loss and mae, tf internally handles distribution
    self.trainMetrics['loss'].update_state(loss * strategy.num_replicas_in_sync)
    self.trainMetrics['dat'].update_state(datMse)
    self.trainMetrics['pde'].update_state(pdeMse)
    mae = tf.reduce_sum(tf.multiply(w[:,0], tf.abs(p - pPred))) \
        / (tf.reduce_sum(w[:,0]) + 1.0e-10)
    self.trainMetrics['mae'].update_state(mae)
    # track gradients coefficients
    if self.save_grad_stat:
      self.record_layer_gradient(datGrad, 'dat')
      self.record_layer_gradient(pdeGrad, 'pde')
    # save the metrics results
    for key in self.trainMetrics:
      self.trainStat[key] = self.trainMetrics[key].result()

    return self.trainStat


  def reset_metrics(self):
    '''
    Overwrite base class's function, this will be called at 
    every epoch's end
    '''
    for key in self.trainMetrics:
      self.trainMetrics[key].reset_states()
    for key in self.validMetrics:
      self.validMetrics[key].reset_states()


  def test_step(self, data):
    bcxyw, p = data
    bc, xy, w = bcxyw

    # ---- compute data, pde mse ---- #
    with tf.GradientTape(watch_accessed_variables=False,persistent=True) as tape2:
      tape2.watch(xy)
      with tf.GradientTape(watch_accessed_variables=False) as tape1:
        tape1.watch(xy)
        pPred = self([bc, xy])
        pPred = tf.squeeze(pPred)
      g = tape1.gradient(pPred, xy)
      p_x = g[:,0]
      p_y = g[:,1]
    p_xx = tape2.gradient(p_x, xy)[:,0]
    p_yy = tape2.gradient(p_y, xy)[:,1]
    del tape2
    # bc mse + pde mse
    datMse = tf.reduce_sum(tf.multiply(w[:,0], tf.square(p - pPred)))\
           / (tf.reduce_sum(w[:,0]) + 1.0e-10)
    pdeMse = tf.reduce_sum(tf.multiply(w[:,1], tf.square(p_xx + p_yy)))\
           / (tf.reduce_sum(w[:,1]) + 1.0e-10)

    # ---- update metrics and statistics ---- #
    self.validMetrics['loss'].update_state(datMse + self.alpha * pdeMse)
    self.validMetrics['pde'].update_state(pdeMse)
    self.validMetrics['dat'].update_state(datMse)
    mae = tf.reduce_sum(tf.multiply(w[:,0], tf.abs(p - pPred))) \
        / (tf.reduce_sum(w[:,0]) + 1.0e-10)
    self.validMetrics['mae'].update_state(mae)
    for key in self.validMetrics:
      self.validStat[key] = self.validMetrics[key].result()

    return self.validStat


  def summary(self):
    nVar = 0
    for t in self.trainable_variables:
      print(t.name, t.shape)
      nVar += tf.reduce_prod(t.shape)
    print('{} trainalbe variables'.format(nVar))


  def preview(self):
    print('--------------------------------')
    print('model preview')
    print('--------------------------------')
    print('fully connected network:')
    print(self.width)
    print('last layer linear: {}'.format(self.last_linear))
    print('activation: ' + self.act)
    print('last layer activation: ' + self.lastAct)
    print('layer regularization')
    print(self.reg)
    print('regularize pde: {}'.format(self.alpha))
    print('collect gradient statistics: {}'.format(self.save_grad_stat))
    print('--------------------------------')


  def grad_xy_norm(self, inputs, nCell, nBc):
    '''
    inputs [bc[batch, nBcCell], xy[batch, 2]], label
    '''
    bc   = tf.convert_to_tensor(inputs[0][0])
    xy   = tf.convert_to_tensor(inputs[0][1])
    # 1st ordre derivatives
    with tf.GradientTape(watch_accessed_variables=False) as tape1:
      tape1.watch(xy)
      pPred = self([bc, xy])
      pPred = tf.squeeze(pPred)
      g = tape1.gradient(pPred, xy)
    # sum the derivatives from boundary points
    gNorm = tf.sqrt(tf.square(g[:,0]) + tf.square(g[:,1]))

    # convert to numpy array of shape nBc*nCell*nCell
    return np.reshape(gNorm.numpy(), (nBc, nCell, nCell))

  def infer_error_and_residual(self, inputs, nCell, nBc, residualOnly=False):
    '''
    inputs [bc[batch, nBcCell], xy[batch, 2], w[batch, 2]]
    '''
    if residualOnly:
      bc   = tf.convert_to_tensor(inputs[0])
      xy   = tf.convert_to_tensor(inputs[1])

    else:
      bc   = tf.convert_to_tensor(inputs[0][0])
      xy   = tf.convert_to_tensor(inputs[0][1])
      p    = tf.convert_to_tensor(inputs[1], dtype=keras.backend.floatx())
      p    = tf.squeeze(p)
    # 2nd ordre derivatives
    with tf.GradientTape(watch_accessed_variables=False, persistent=True) as tape2:
      tape2.watch(xy)
      # 1st ordre derivatives
      with tf.GradientTape(watch_accessed_variables=False) as tape1:
        tape1.watch(xy)
        pPred = self([bc, xy])
        pPred = tf.squeeze(pPred)
      g = tape1.gradient(pPred, xy)
      p_x = g[:,0]
      p_y = g[:,1]
    p_xx = tape2.gradient(p_x, xy)[:,0]
    p_yy = tape2.gradient(p_y, xy)[:,1]
    del tape2
    # convert to numpy array of shape nBc*nCell*nCell
    if not residualOnly:
      residual = tf.squeeze(p_xx + p_yy)
      resArr   = np.reshape(residual.numpy(), (nBc, nCell+2, nCell+2))
      err    = p - pPred
      errArr = np.reshape(err.numpy(), (nBc, nCell+2, nCell+2))
      return [errArr, resArr]
    else:
      residual = tf.squeeze(p_xx + p_yy)
      resArr   = np.reshape(residual.numpy(), (nBc, nCell, nCell))
      return resArr



#-------------------------------------------------------------------------------
# model subclasses, fully connected
# Input:  bc - values at boundary points 
#         xy - (x, y)
#         w  - marker to distinguish data and collocation
# Output: p @ xy
# Note that I assume bc's effect is linear, so it is not passed to the MLP
# Only xy is the input to the MLP, output are the coefficients timed with bc
# elementwisely.
#-------------------------------------------------------------------------------

class PsnXyFC(PsnFC):
  def __init__(self, **kwargs):
    super(PsnXyFC, self).__init__(**kwargs)  
    

  def call(self, inputs, training=None):
    bc = inputs[0]
    xy = inputs[1]
    for layer in self.mlp:
      xy = layer(xy)
    return tf.reduce_sum(tf.multiply(bc, xy), axis=-1)



#-------------------------------------------------------------------------------
# model subclasses, fully connected
# Input:  bc   - values at boundary points 
#         xyBc - (x, y) of boundary points
#         xy   - (x, y) of the input points, including data and collocation
#         w    - marker to distinguish data and collocation
# Output: p @ xy
# Note that I assume bc's effect is linear, so it is not passed to the MLP
# Only xy is the input to the MLP, output are the coefficients timed with bc
# elementwisely.
#-------------------------------------------------------------------------------


class PsnWeight(PsnFC):
  def __init__(self, **kwargs):
    super(PsnWeight, self).__init__(**kwargs)  
    # define the layers with input bc+xy, output p(x, y)
    #self.wBlock = BcWeight(self.nCell, self.width, self.act)
    

  def call(self, inputs, training=None):
    '''
    inputs [bc[batch, nBcCell], xyBc[batch, nBcCell, 2],
            xy[batch, nBcCell, 2], w[batch, 2]]
    '''
    coef = tf.concat([inputs[1], inputs[2]], axis=-1)
    for layer in self.mlp:
      coef = layer(coef)
    coef = tf.squeeze(coef)

    return tf.reduce_sum(tf.multiply(inputs[0], coef), axis=-1)


  def train_step(self, data):
    bc   = data[0][0]
    xyBc = data[0][1]
    xy   = data[0][2]
    w    = data[0][3]
    p    = data[1]
    # track gradients 
    with tf.GradientTape(persistent=True) as tape0:
      # compute pde mse
      with tf.GradientTape(watch_accessed_variables=False,persistent=True) as tape2:
        tape2.watch(xy)
        with tf.GradientTape(watch_accessed_variables=False) as tape1:
          tape1.watch(xy)
          pPred = self([bc, xyBc, xy])
          pPred = tf.squeeze(pPred)
        g = tape1.gradient(pPred, xy)
        p_x = g[:,:,0]
        p_y = g[:,:,1]
      p_xx = tf.reduce_sum(tape2.gradient(p_x, xy)[:,:,0], axis=1)
      p_yy = tf.reduce_sum(tape2.gradient(p_y, xy)[:,:,1], axis=1)
      # bc mse + pde mse
      datMse = tf.reduce_sum(tf.multiply(w[:,0], tf.square(p - pPred)))\
             / (tf.reduce_sum(w[:,0]) + 1.0e-10)
      pdeMse = tf.reduce_sum(tf.multiply(w[:,1], tf.square(p_xx + p_yy)))\
             / (tf.reduce_sum(w[:,1]) + 1.0e-10)
      # replica's loss, divided by global batch size
      # the loss also includes the regularization
      loss   = (datMse + self.alpha * pdeMse)
      loss  += tf.add_n(self.losses)
      loss   = loss / strategy.num_replicas_in_sync
      del tape2
    # update gradients and trainable variables
    if self.save_grad_stat:
      datGrad  = tape0.gradient(datMse, self.trainable_variables)
      pdeGrad  = tape0.gradient(pdeMse, self.trainable_variables)
    lossGrad = tape0.gradient(loss,   self.trainable_variables)
    del tape0

    # ---- apply gradients ---- #
    self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))

    # ---- update metrics and statistics ---- #
    # update loss and mae, tf internally handles distribution
    self.trainMetrics['loss'].update_state(loss * strategy.num_replicas_in_sync)
    self.trainMetrics['dat'].update_state(datMse)
    self.trainMetrics['pde'].update_state(pdeMse)
    self.trainMetrics['mae'].update_state(p, pPred)
    # track gradients coefficients
    if self.save_grad_stat:
      self.record_layer_gradient(datGrad, 'dat')
      self.record_layer_gradient(pdeGrad, 'pde')
    for key in self.trainMetrics:
      self.trainStat[key] = self.trainMetrics[key].result()

    return self.trainStat

  def test_step(self, data):
    bc   = data[0][0]
    xyBc = data[0][1]
    xy   = data[0][2]
    w    = data[0][3]
    p    = data[1]
    # track gradients over xy to compute pde error
    with tf.GradientTape(watch_accessed_variables=False,persistent=True) as tape2:
      tape2.watch(xy)
      with tf.GradientTape(watch_accessed_variables=False) as tape1:
        tape1.watch(xy)
        pPred = self([bc, xyBc, xy])
        pPred = tf.squeeze(pPred)
      g = tape1.gradient(pPred, xy)
      p_x = g[:, :, 0]
      p_y = g[:, :, 1]
    p_xx = tf.reduce_sum(tf.squeeze(tape2.gradient(p_x, xy)[:,:,0]), axis=1)
    p_yy = tf.reduce_sum(tf.squeeze(tape2.gradient(p_y, xy)[:,:,1]), axis=1)
    del tape2
    # bc mse + pde mse
    datMse = tf.reduce_sum(tf.multiply(w[:,0], tf.square(p - pPred)))\
           / (tf.reduce_sum(w[:,0]) + 1.0e-10)
    pdeMse = tf.reduce_sum(tf.multiply(w[:,1], tf.square(p_xx + p_yy)))\
           / (tf.reduce_sum(w[:,1]) + 1.0e-10)
    # update loss and mae, tf internally handles distribution
    loss = datMse + self.alpha * pdeMse
    self.validMetrics['loss'].update_state(loss)
    self.validMetrics['pde'].update_state(pdeMse)
    self.validMetrics['dat'].update_state(datMse)
    self.validMetrics['mae'].update_state(p, pPred)
    for key in self.validMetrics:
      self.validStat[key] = self.validMetrics[key].result()
    #
    return self.validStat


  def grad_xy_norm(self, inputs, nCell, nBc):
    '''
    inputs: [bc[batch, nBcCell], xyBc[batch, nBcCell, 2],
             xy[batch, nBcCell, 2]]
    Overwrite function in base class because the inputs has different
    number of elements
    '''
    bc   = tf.convert_to_tensor(inputs[0][0])
    xyBc = tf.convert_to_tensor(inputs[0][1])
    xy   = tf.convert_to_tensor(inputs[0][2])
    # 1st ordre derivatives
    with tf.GradientTape(watch_accessed_variables=False) as tape1:
      tape1.watch(xy)
      pPred = self([bc, xyBc, xy])
      pPred = tf.squeeze(pPred)
      g = tape1.gradient(pPred, xy)
    # sum the derivatives from boundary points
    g = tf.reduce_sum(g, axis=1)
    gNorm = tf.sqrt(tf.square(g[:,0]) + tf.square(g[:,1]))
    # convert to numpy array of shape nBc*nCell*nCell
    return np.reshape(gNorm.numpy(), (nBc, nCell, nCell))
