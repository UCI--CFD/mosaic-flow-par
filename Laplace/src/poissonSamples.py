import numpy as np
from scipy import interpolate
import random
import os
import pyamg
from pyamg.gallery import poisson
import h5py as h5
import argparse
import matplotlib.pyplot as plt

PI = np.pi
random.seed(2)

# command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-n', '--num_sample', default=10000, type=int, help='# samples')
parser.add_argument('-l', '--edge_len', default=32, type=int, help='edge length')
parser.add_argument('-path', '--path', default='../data/bcs_10000.h5', help='h5 file for bc')
args = parser.parse_args()

# num samples
nSample = args.num_sample

# num grid cells along an edge
nCell = args.edge_len

# file name for the bc data
bcFileName = args.path

# space step, domain length is 1
h = 1.0 / nCell

# matrix
A = poisson((nCell, nCell), format='csr')

# create datasets to contain solutons
solFile = h5.File('Laplace_{}_{}.h5'.format(nCell, nSample), 'w')
solFile.attrs['nSample']    = nSample
solFile.attrs['domainSize'] = np.array([nCell, nCell])
solData = solFile.create_dataset('PoissonSolution', (nSample, nCell+2, nCell+2), \
                                  compression='gzip', compression_opts=9, \
                                  dtype='float64', chunks=True)

# load boundary condition
bcFile = h5.File(bcFileName, 'r')
#assert bcFile['bcs'].shape[0] == nSample
print('load ' + repr(nSample) + ' boundary conditions from ' + bcFileName)
bcs = np.zeros(bcFile['bcs'].shape)
bcs[...] = bcFile['bcs'][...]
bcFile.close()

# write out 4 bc samples
# fout = open('samples.txt', 'w')
# fout.write('x s1 s2 s3 s4\n')
# for i in range(len(bcs[0, :])):
#   x = (i+0.5)*1.0/33
#   fout.write('{:.4e} {:.4e} {:.4e} {:.4e} {:.4e}\n'\
#               .format(x, bcs[0,i], bcs[1,i], bcs[2,i], bcs[3,i]))
# fout.close()

# variable with dirichlet bc
p = np.zeros((nCell+2, nCell+2))

# generate samples
for iSample in range(nSample):
#   # random dirichlet bc
#   # cells used to interpolate
#   x = np.linspace(0, (4*nCell+3)*h, int(nCell/2))
#   y = np.zeros(len(x))
#   for i in range(len(x)-1):
#     y[i] = random.uniform(-1,1)
#   y[-1] = y[0]
#   splineFunc = interpolate.splrep(x, y, s=0)
#   xBC = np.linspace(0, (4*nCell+3)*h, 4*nCell+4)
#   bc  = interpolate.splev(xBC, splineFunc)
#   p[0,  :]      = bc[         :  nCell+2]
#   p[1:,-1]      = bc[  nCell+2:2*nCell+3]
#   p[-1,-2::-1]  = bc[2*nCell+3:3*nCell+4]
#   p[-2:0:-1, 0] = bc[3*nCell+4:4*nCell+4]

  # copy boundary condition to solution variable
  p[0,  :]      = bcs[iSample,          :  nCell+2]
  p[1:,-1]      = bcs[iSample,   nCell+2:2*nCell+3]
  p[-1,-2::-1]  = bcs[iSample, 2*nCell+3:3*nCell+4]
  p[-2:0:-1, 0] = bcs[iSample, 3*nCell+4:4*nCell+4]

  # rhs, using rhs function values
  rhs = np.zeros((nCell, nCell))
  for i in range(0, nCell):
    for j in range(0, nCell):
      x = (i + 0.5) * h
      y = (j + 0.5) * h
      #rhs[i,j] = -20*PI*PI*np.sin(2*PI*x)*np.sin(4*PI*y) * (-h*h)
  # add bc values to rhs
  rhs[ 0,:] += p[ 0,1:-1]
  rhs[-1,:] += p[-1,1:-1]
  rhs[:, 0] += p[1:-1, 0]
  rhs[:,-1] += p[1:-1,-1]
  # transform rhs to a vector
  rhs = np.reshape(rhs, ((nCell)*(nCell)), order='C')
  # solve
  x = pyamg.solve(A, rhs, verb=False, tol=1e-14)
  # copy solution
  p[1:-1,1:-1] = np.reshape(x, (nCell, nCell), order='C')
  # # compute residuals
  # residualL0 = 0.0
  # residualL1 = 0.0
  # for i in range(1, nCell+1):
  #   for j in range(1, nCell+1):
  #     x = (i - 0.5) * h
  #     y = (j - 0.5) * h
  #     # b = -20*PI*PI * np.sin(2*PI*x) * np.sin(4*PI*y)
  #     b = 0.0
  #     residual    = abs(b - (p[i-1,j] + p[i+1,j] + p[i,j+1] + p[i,j-1] - 4*p[i,j])/h/h)
  #     residualL1 += residual
  #     residualL0  = max(residualL0, residual)
  # residualL1 = residualL1 / nCell / nCell
  # print("residual L0: {:.4e}".format(residualL0))
  # print("residual L1: {:.4e}".format(residualL1))
  # write the solution to hdf5 dataset
  solData[iSample,:,:] = p
  # show progress
  if iSample > 0 and iSample%200 == 0:
    print("generate {} samples".format(iSample))

solFile.close()
