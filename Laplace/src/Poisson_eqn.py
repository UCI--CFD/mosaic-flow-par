import numpy as np
from scipy import interpolate
import random
import pyamg
from pyamg.gallery import poisson

random.seed(2)

def set_random_halo(p, random_min=-1.0, random_max=1.0):
  assert type(p) == np.ndarray
  nx = p.shape[0] - 2
  ny = p.shape[1] - 2
  h  = 1.0/nx if nx > ny else 1.0/ny

  x = np.linspace(0, (2*nx+2*ny+3)*h, min(nx, ny)//2)
  y = np.zeros(len(x))
  for i in range(len(x)-1):
    y[i] = random.uniform(random_min, random_max)
  y[-1] = y[0]
  splineFunc = interpolate.splrep(x, y, s=0)
  xBC = np.linspace(0, (2*nx+2*ny+3)*h, 2*(nx+ny)+4)
  bc  = interpolate.splev(xBC, splineFunc)
  p[0,  :]      = bc[         :  ny+2]
  p[1:,-1]      = bc[  ny   +2:  ny+nx+3]
  p[-1,-2::-1]  = bc[  ny+nx+3:2*ny+nx+4]
  p[-2:0:-1, 0] = bc[2*ny+nx+4:2*ny+2*nx+4]


def set_sin_halo(p, h=1.0/32):
  assert type(p) == np.ndarray
  nx = p.shape[0] - 2
  ny = p.shape[1] - 2
  x = np.linspace(0, (2*nx+2*ny+4)*h, (2*nx+2*ny+4))
  y = 0.5*np.sin(0.5*np.pi*x)
  p[0,  :]      = y[         :  ny+2]
  p[1:,-1]      = y[  ny   +2:  ny+nx+3]
  p[-1,-2::-1]  = y[  ny+nx+3:2*ny+nx+4]
  p[-2:0:-1, 0] = y[2*ny+nx+4:2*ny+2*nx+4]


def apply_bc_to_rhs(p, b):
  assert type(p) is np.ndarray
  assert type(b) is np.ndarray
  assert p.shape[0] == b.shape[0]+2 and p.shape[1] == b.shape[1]+2

  b[ 0,:] += p[ 0,1:-1]
  b[-1,:] += p[-1,1:-1]
  b[:, 0] += p[1:-1, 0]
  b[:,-1] += p[1:-1,-1]


def solve(p, rhs, A=None, pOut=None, verbose=False):
  assert type(p)   == np.ndarray
  assert type(rhs) == np.ndarray
  assert p.shape[0] == rhs.shape[0]+2 and p.shape[1] == rhs.shape[1]+2

  nx = p.shape[0] - 2
  ny = p.shape[1] - 2

  # prepare rhs
  b = np.copy(rhs)
  apply_bc_to_rhs(p, b)
  b = np.reshape(b, (nx*ny))

  if not A: A = poisson((nx, ny), format='csr')
  if pOut:
    pOut[1:-1,1:-1] = np.reshape(pyamg.solve(A, b, verb=verbose, tol=1e-14), (nx, ny))
  else:
    p[1:-1,1:-1] = np.reshape(pyamg.solve(A, b, verb=verbose, tol=1e-14), (nx, ny))


def solve_genome(p0, Ag, rhs, gi, gj, gx, gy, p1):
  # rhs of genome
  bg = np.zeros((gx, gy))
  bg[:,:]   = rhs[gi-1:gi-1+gx, gj-1:gj-1+gy]
  bg[ 0,:] += p0[gi-1,  gj:gj+gy]
  bg[-1,:] += p0[gi+gx, gj:gj+gy]
  bg[:, 0] += p0[gi:gi+gx, gj-1]
  bg[:,-1] += p0[gi:gi+gx, gj+gy]
  bg = np.reshape(bg, (gx*gy), order='C')
  # genome solution
  p1[gi:gi+gx, gj:gj+gy] = np.reshape(pyamg.solve(Ag, bg, verb=False, tol=1e-14),\
                                      (gx, gy), order='C')
