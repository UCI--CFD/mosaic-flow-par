import numpy as np
from scipy import interpolate
import random
import pyamg
from pyamg.gallery import poisson
import argparse
import tensorflow as tf
import Poisson_eqn as PsnEqn
import Poisson_model as PsnModel
import h5py as h5
from block import*
from genome import * 

PI = np.pi
random.seed(4)

def setup_block_corner(blocks):
  blocks[0].vars[ 0, 0,0] = blocks[1].vars[ 0,-2,0]
  blocks[0].vars[-1, 0,0] = blocks[1].vars[-1,-2,0]
  blocks[0].vars[ 0,-1,0] = blocks[3].vars[ 0, 1,0]
  blocks[0].vars[-1,-1,0] = blocks[3].vars[-1, 1,0]
  blocks[1].vars[ 0,-1,0] = blocks[2].vars[-2, 1,0]
  blocks[1].vars[-1,-1,0] = blocks[4].vars[ 1, 1,0]
  blocks[2].vars[-1, 0,0] = blocks[1].vars[ 1,-2,0]
  blocks[2].vars[-1,-1,0] = blocks[3].vars[ 1, 1,0]
  blocks[3].vars[ 0, 0,0] = blocks[2].vars[-2,-2,0]
  blocks[3].vars[-1, 0,0] = blocks[4].vars[ 1,-2,0]
  blocks[4].vars[ 1, 0,0] = blocks[1].vars[-2,-2,0]
  blocks[4].vars[ 1,-1,0] = blocks[3].vars[-2, 1,0]


# domain length and grid space
gx, gy = 32, 32
h = 1.0/gx

# 5 blocks make up the cross shape
blocks = []
for i in range(5):
  blocks.append(Block((gx, gy), nVar=1))
# setup connections
blocks[0].add_connection(0, 2, 2)
blocks[0].add_connection(1, 3, 3)
blocks[0].add_connection(2, 4, 0)
blocks[0].add_connection(3, 1, 1)
blocks[1].add_connection(1, 0, 3)
blocks[2].add_connection(2, 0, 0)
blocks[3].add_connection(3, 0, 1)
blocks[4].add_connection(0, 0, 2)

# setup physical bc, -4+8 is for corners
nBCcell = gx*3*4 - 4 + 8
x = np.linspace(0, nBCcell, nBCcell//16)
y = np.zeros(len(x))
for i in range(len(x)-1):
  y[i] = random.uniform(-0.5, 0.5)
y[-1] = y[0]
splineFunc = interpolate.splrep(x, y, s=0)
xBC = np.linspace(0, nBCcell*h, nBCcell)
bc  = interpolate.splev(xBC, splineFunc)
# copy bc to blocks
s, e = 0, gx+1
blocks[1].vars[0, 0:-1,0]  = bc[s:e]
s, e = e-1, e-1 + gx + 1
blocks[2].vars[-2::-1,0,0] = bc[s:e]
s, e = e, e + gx + 1
blocks[2].vars[0,1:,0] = bc[s:e]
s, e = e, e + gx
blocks[2].vars[-1,1:-1,0] = bc[s:e]
s, e = e-1, e-1 + gx + 1
blocks[3].vars[0,1:,0] = bc[s:e]
s, e = e, e + gx + 1
blocks[3].vars[1:,-1,0] = bc[s:e]
s, e = e, e + gx
blocks[3].vars[-1,-2:0:-1,0] = bc[s:e]
s, e = e-1, e-1 + gx + 1
blocks[4].vars[-1,1:,0] = bc[s:e]
s, e = e, e + gx + 1
blocks[4].vars[-1,-2::-1,0] = bc[s:e]
s, e = e, e + gx
blocks[4].vars[-2:0:-1,-1,0] = bc[s:e]
s, e = e-1, e-1 + gx + 1
blocks[1].vars[-1,-2::-1,0] = bc[s:e]
s, e = e, e + gx
blocks[1].vars[-2:0:-1,0,0] = bc[s:e]
assert e == len(bc)

# create a copy of blocks
blocks0 = []
for b in blocks:
  blocks0.append(cp.deepcopy(b))

# setup genomes for the blocks
genomes = [[], [], []]
# basic genomes
for b in range(len(blocks)):
  g = Genome(nVar=1)
  gRng = (1, 1, 1+gx, 1+gy)
  bRng = (1, 1, 1+gx, 1+gy)
  g.add_map(b, bRng, gRng)
  genomes[0].append(g)
# i-shift genome between block 0, 2
g = Genome(nVar=1)
g.add_map(0, (1, 1, 1+gx//2, 1+gy), (1+gx//2, 1, 1+gx, 1+gy))
g.add_map(2, (1+gx//2, 1, 1+gx, 1+gy), (1, 1, 1+gx//2, 1+gy))
genomes[1].append(g)
# i-shift genome between block 0, 4
g = Genome(nVar=1)
g.add_map(0, (1+gx//2, 1, 1+gx, 1+gy), (1, 1, 1+gx//2, 1+gy))
g.add_map(4, (1, 1, 1+gx//2, 1+gy), (1+gx//2, 1, 1+gx, 1+gy))
genomes[1].append(g)
# j-shift genome between block 1, 0
g = Genome(nVar=1)
g.add_map(1, (1, 1+gy//2, 1+gx, 1+gy), (1, 1, 1+gx, 1+gy//2))
g.add_map(0, (1, 1, 1+gx, 1+gy//2), (1, 1+gy//2, 1+gx, 1+gy))
genomes[2].append(g)
# j-shift genome between block 0, 3
g = Genome(nVar=1)
g.add_map(0, (1, 1+gy//2, 1+gx, 1+gy), (1, 1, 1+gx, 1+gy//2))
g.add_map(2, (1, 1, 1+gx, 1+gy//2), (1, 1+gy//2, 1+gx, 1+gy))
genomes[2].append(g)

# iterate with linear solver
maxIter = 3
nIterConverge = -1
Ag  = poisson((gx, gy), format='csr')
for m in range(maxIter):
  for iType in range(3): 
    for g in genomes[iType]:
      # copy from block, include bc
      g.extract_from_block(blocks0)
      # setup rhs
      rhs = np.zeros((gx, gy))
      rhs[ 0,:] += g.vars[ 0,1:-1,0]
      rhs[-1,:] += g.vars[-1,1:-1,0]
      rhs[:, 0] += g.vars[1:-1, 0,0]
      rhs[:,-1] += g.vars[1:-1,-1,0]
      # solve genome with AMG
      g.vars[1:-1,1:-1,0] = np.reshape(pyamg.solve(Ag, rhs.reshape((gx*gy)), 
                                                   verb=False, tol=1e-14), \
                                       (gx, gy))
      # copy genome to block
      g.copy_to_block(blocks)
    # exchange blocks' boundary
    update_block_connection(blocks)
    setup_block_corner(blocks)
    # check the residuals
    residuals = compute_residual(blocks0, blocks, verbose=True)
    if residuals[0][0] < 1.0e-10: 
      nIterConverge = m
      break
    copy_block(blocks, blocks0)
    if nIterConverge > 0: break;

# clear data
nullify_block(blocks0)
nullify_block(blocks)

# load model
pMax =  1.8346210104700842
pMin = -1.7867983987223126
psnNet = PsnModel.create_ff0(gx)
modelPath = '../run/'
modelName = 'psnModel_cat-bc-coord_128-128-128-64-32-1swish_rs-2_b20'
psnNet.load_weights(modelPath + modelName + '.h5')

# iterate with networks
maxIter = 1000
genomeTypes  = ['basic', 'i-shift', 'j-shift']
for m in range(maxIter):
  for iType in range(3): 
    # copy data from block to genome
    for g in genomes[iType]:
      g.extract_from_block(blocks)
    # predict
    pg = psnNet.predict(PsnModel.create_genome_input(pMax, pMin, genomes, 
                                                     genome=genomeTypes[iType]))
    pg = pg * (pMax - pMin) + pMin
    # copy prediction to genome and block
    s, e = 0, gx*gy
    for g in genomes[iType]:
      g.vars[1:-1,1:-1,0] = pg[s:e,0].reshape((gx, gy))
      g.copy_to_block(blocks)
      s, e = e, e + gx*gy
    # exchange block boundaries
    update_block_connection(blocks)
    setup_block_corner(blocks)
  # check the residuals
  residuals = compute_residual(blocks0, blocks, verbose=True)
  if residuals[0][0] < 1.0e-10: break
  #
  copy_block(blocks, blocks0)
