#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 15:54:55 2021

@author: robertplanas
"""

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../res/"))
import argparse
import h5py as h5
import numpy as np

parser = argparse.ArgumentParser()

# Model Parameters Branch

parser.add_argument('-w', '--width', type=int, nargs='*', help='width', 
                    default = [32, 32, 64, 64, 96, 96, 96, 96, 96, 124,124,124,124,124])
parser.add_argument('-act', '--activation', default = 'tanh', 
                    help = 'Activation function for Branch Model')
parser.add_argument('-last_linear', '--last_linear', default = True, 
                    help = 'Use linear activation on the last layer')
# Alpha for residual
parser.add_argument('-alpha', '--alpha', default = 0, type = float, 
                 help = 'Alpha parameter for the residual. Set to 0 if do not want to use')

# Regularization
parser.add_argument('-reg', '--reg', default = 0, type = float, 
                 help = 'L2 regularization')

#Data
parser.add_argument('-f', '--file', default='../../CNN/data/Laplace_BC_AMG_ED_31.h5',
                    help='data file')

#Traing Parameters
parser.add_argument('-e', '--nEpoch', type=int, default=10000,
                    help='initial train epochs')
parser.add_argument('-patience', '--patience', type=int, default=200,
                    help='patience')

parser.add_argument('-B', '--Batch', type=int, default=20,
                    help='Batch Size')

# learning rate adjustment
parser.add_argument('-f_lr', '--factor_lr', type=float, default = 0.9,
                    help = 'Reducing the learning rate')
parser.add_argument('-lr0', '--lr0', type=float,
                    default=1e-3, help='init leanring rate')


#Restart 
parser.add_argument('-restart', '--restart', default=False, action='store_true',
                    help='restart from checkpoint')
parser.add_argument('-ckpnt', '--checkpoint', default=None,
                    help='Checkpoint name to restart the training from')



# Choose the data type
parser.add_argument('-dt', '--DataType', default='float32',
                    help='DataType')
parser.add_argument('-nVal', '--nVal', type=int, default=100,
                    help='number of validation solutions ')
parser.add_argument('-nTest', '--nTest', type=int, default=100,
                    help='number of test solutions ')
parser.add_argument('-nTrain', '--nTrain', type=int, default=1000,
                    help='number of test solutions ')

# Choose nCol and nDat
parser.add_argument('-nCol', '--nCol', default=500, type = int,
                    help='Number of collocation points')




args = parser.parse_args()

import os
import tensorflow as tf
from tensorflow import keras
import xy_models as models
import utils_og as utils 

#%% =============================================================================
#  Define the name of the model 
# =============================================================================


act = "_act-" + str(args.activation)

Reg = "_reg-" + f"{args.reg:.0e}"
Res = "_alpha-" + f"{args.alpha:.0e}"

Fac = "_flr-" + str(args.factor_lr)
Patience = "_Pat-" + str(args.patience)
Iterations = "_Iter-" + str(args.nEpoch)
n_Train_Samples = '_nTrain-' + str(args.nTrain)


modelName = "LPFC_Cat-I_" + act +  Reg  + Res + Fac + Patience + Iterations + n_Train_Samples

print(f"Model Name : {modelName}")

#%% =============================================================================
# Load the data  
# =============================================================================

# AutoEncoder File
Data_File = os.path.join(os.path.dirname(__file__), args.file)

# load hdf5 data and test residual of Poisson (psn)
psnFile = h5.File(Data_File, 'r')
nSample = psnFile.attrs['nSample']
nCell = psnFile.attrs['domainSize'][0] 
psnData = psnFile['LaplaceSolution']
print('# Samples: {}'.format(nSample))
print('# Cells: {}*{}'.format(nCell, nCell))
print('data shape: ', end='')
print(psnData.shape)

# read the data into numpy array
p = np.zeros(psnData.shape)
p = psnData[:, :, :]
pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))

# split traning and validation
nValid = int(args.nVal)
nTest = int(args.nTest)
nTrain = int(args.nTrain)
print('{} solutions in training, {} in validation, {} in testing'.format(
    nTrain, nValid, nTest))

p = (p -pMin)/(pMax - pMin)*2 -1

#%% =============================================================================
# Create the inputs
# =============================================================================
Y_Train = p[:nTrain ,:,:]
Y_Val = p[nTrain:nTrain + nValid,:,:]
Y_Test = p[nTrain + nValid:,:,:] 

X = np.zeros_like(p)
X[:,0,:] = p[:,0,:]
X[:,-1,:] = p[:,-1,:]
X[:,:,0] = p[:,:,0]
X[:,:,-1] = p[:,:,-1]

X_Train = X[:nTrain, :, :]
X_Val = X[nTrain:nTrain + nValid,:,:]
X_Test = X[nTrain + nValid:, : ,:]

X_points = np.linspace(0,1, num = nCell)[None, :]
Y_points = np.linspace(0,1, num = nCell)[None, :]
xy_dat = np.reshape(np.asarray(np.meshgrid(X_points, Y_points)).T, (nCell*nCell, 2))

TrainGen = utils.Generator(X_Train, Y_Train, Batch = args.Batch, nCol = args.nCol)
ValidGen = utils.Generator(X_Val, Y_Val, Batch = args.Batch, nCol = args.nCol)
TestGen = utils.Generator(X_Test, Y_Test, Batch = args.Batch, nCol = args.nCol)


 

#%% ===========================================================================
# Load the model
# =============================================================================
LPFC_Model = models.LPFC_CatI(width=args.width, activation = args.activation, alpha=args.alpha, 
                 last_linear=args.last_linear, last_act='linear', reg = args.reg, nCell = nCell, Batch = args.Batch)
LPFC_Model.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))

#%% ===========================================================================
# Predict Fake Input
# =============================================================================
print("Prediction Data points -----")
Out_dat = LPFC_Model.call_print((X[0:args.Batch,:,:], xy_dat) )

#%% =============================================================================
# Set Callbacks
# =============================================================================
Path_res = os.path.join(os.path.dirname(__file__), "../res/")
if not os.path.exists(Path_res + modelName):
    os.mkdir(Path_res + modelName)
Callbacks = [tf.keras.callbacks.ModelCheckpoint(filepath=Path_res + modelName +"/" +  modelName + '.h5',
                                                monitor='loss', save_best_only=True,
                                                save_weights_only=True, verbose=1),
                tf.keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=args.factor_lr,
                                                  patience=args.patience, min_delta=0.01,
                                                  min_lr=1e-7),
                tf.keras.callbacks.CSVLogger(Path_res + modelName + "/" + modelName + '.log', append=True)]


#%% =============================================================================
# Train the model in modules to avoid losing the optimum
# =============================================================================

LPFC_Model.fit(TrainGen, validation_data=ValidGen, epochs=args.nEpoch, 
              steps_per_epoch = nTrain//args.Batch,
              validation_steps=nValid//args.Batch,
              callbacks=Callbacks, verbose = 2)

  
LPFC_Model.load_weights((Path_res + modelName + "/" + modelName + ".h5" ))
LPFC_Model.optimizer.lr.assign(LPFC_Model.optimizer.lr*args.factor_lr)
#%% =============================================================================
# Evaluate model
# =============================================================================
LPFC_Model_1 = models.LPFC_CatI(width=args.width, activation = args.activation, alpha=1, 
                 last_linear=args.last_linear, last_act='linear', reg = args.reg, nCell = nCell, Batch = args.Batch)
LPFC_Model_1.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))
Out_dat = LPFC_Model_1((X[0:args.Batch,:,:], xy_dat) )
LPFC_Model_1.load_weights((Path_res + modelName + "/" + modelName + ".h5" ))
Answer = LPFC_Model_1.evaluate(TestGen, steps = nTest//args.Batch)
Loss = Answer[0]
MSE = Answer[1]
MAE = Answer[2]
Res = Answer[3]


Answer = LPFC_Model_1.evaluate(TrainGen, steps = nTrain//args.Batch)
Loss_Train = Answer[0]
MSE_Train = Answer[1]
MAE_Train = Answer[2]
Res_Train = Answer[3]

resName = Path_res + "Training_LPFC"
with open(resName + ".txt", "a") as f:
    f.write(f"Model = {modelName} \n \t Test -- Loss = {Loss:.2e}, Res = {Res:.2e}, MSE = {MSE:.2e}, MAE = {MAE:.2e}  \n")
    f.write(f"\t Train -- Loss = {Loss_Train:.2e}, Res = {Res_Train:.2e}, MSE = {MSE_Train:.2e}, MAE = {MAE_Train:.2e}  \n")
    f.close()
    
    f.close()



#%%

