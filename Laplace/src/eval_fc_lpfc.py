import sys
sys.path.append('../src')
import h5py as h5
import numpy as np
import tensorflow as tf
# %matplotlib inline
import matplotlib.pyplot as plt
from tensorflow import keras
import pinn_model as PsnModel
from pinn_utils import *
import pandas as pd
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-m', '--modelType', type=int, default=1, \
                    help='0 - psnfc, 1 - psnxyfc')
parser.add_argument('-l', '--arch', type=int, nargs='*',
                    default=[32,32,64,64,96,96,96,96,96,128,128,128,128,128],
                    help='architecture')
parser.add_argument('-f', '--model',
                    default='../run_rebuttal_hpc3/lplc82-32x2-64x2-96x5-128x5t8800_a0.001_d400_c400_s20',
                    help='model path')
parser.add_argument('-p', '--precision', default='float32', help='model precision')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')
args = parser.parse_args()

#%%
#------------------------------------------------------------
# Load solutions and visualize a few samples
#------------------------------------------------------------

# control knobs
visualize = False
fName     = '../data/Laplace32_21000.h5'

# load data into a numpy array
psnFile = h5.File(fName, 'r')
nSample = psnFile.attrs['nSample']
nCell   = psnFile.attrs['domainSize'][0]
psnData = psnFile['PoissonSolution']
h       = 1.0 / (nCell)
p       = np.zeros(psnData.shape)
p[...]  = psnData[:,:,:]
pMin, pMax = np.amin(p[:,:,:]), np.amax(p[:,:,:])
#
print('# Samples: {}'.format(nSample))
print('# Cells: {}*{}'.format(nCell, nCell))
print('data shape: ', end='')
print(psnData.shape)
print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))

#%%
#------------------------------------------------------------
# Define the generator for poisson FC and XyFc
#------------------------------------------------------------
def PredictGen(p, sampleBegin, sampleEnd, pMax, pMin, nCell, nSamplePerBatch, iStride=1, jStride=1):
  nCasePerSample = (nCell//iStride) * (nCell//jStride) + 4*(nCell+1)
  iSample = sampleBegin
  batchSize = nSamplePerBatch * nCasePerSample
  h = 1.0 / nCell
  bcs    = np.zeros((batchSize, 4*nCell))
  bc     = np.zeros((4*nCell))
  coords = np.zeros((batchSize, 2))
  labels = np.zeros((batchSize, 1))
  w      = np.array([0.0, 0.0])
  while 1:
    if iSample + nSamplePerBatch > sampleEnd: iSample = sampleBegin
    iCase = 0
    for m in range(nSamplePerBatch):
      # bc of current sample
      bc[       :  nCell] = (p[iSample, 0, 1:-1] + p[iSample, 1, 1:-1]) * 0.5
      bc[  nCell:2*nCell] = (p[iSample, 1:-1, -1] + p[iSample, 1:-1, -2]) * 0.5
      bc[2*nCell:3*nCell] = (p[iSample, -1, -2:0:-1] + p[iSample, -2, -2:0:-1]) * 0.5
      bc[3*nCell:4*nCell] = (p[iSample, -2:0:-1, 0] + p[iSample, -2:0:-1, 1]) * 0.5
      # normalize bc
      bc[:] = (bc[:] - pMin) / (pMax - pMin)
      # set all cases use the same bc
      for i in range(iCase, iCase+nCasePerSample): bcs[i,:] = bc[:]
      # add cells
      for i in range(0, nCell+2, iStride):
        for j in range(0, nCell+2, jStride):
  	      coords[iCase,:] = [(i-0.5)*h, (j-0.5)*h]
  	      labels[iCase,0] = (p[iSample, i, j] - pMin) / (pMax - pMin)
  	      iCase += 1
      iSample += 1
    yield [bcs, coords, w], labels


#%%
#------------------------------------------------------------
# Evaluate new model using network to compute residual
#------------------------------------------------------------

# set model pathes and precisions
models = []
precisions = []
architectures = []
modelTypes = []
modelInfos = []
if args.reproduce:
  models = [
    '../models/lplc-128x2-96x2-64x2-32x2-1t2000_a0.001_d100_c400_s20',
    '../models/lplc-32x2-64x2-96x2-128x2t2000_a0.001_d100_c400_s20',
    '../models/lplc-128x5-96x5-64x2-32x2-1t4000_a0.001_d400_c400_s20',
    '../models/lplc-32x2-64x2-96x5-128x5t4000_a0.001_d400_c400_s20',
    '../models/lplc-128x5-96x5-64x2-32x2-1t8000_a0.001_d400_c400_s20',
    '../models/lplc-32x2-64x2-96x5-128x5t8000_a0.001_d400_c400_s20',
    '../models/lplc_fp64-128x5-96x5-64x2-32x2-1t8000_a0.001_d400_c400_s20',
    '../models/lplc_fp64-32x2-64x2-96x5-128x5t8000_a0.001_d400_c400_s20',
    '../models/lplc-32x2-64x2-96x5-128x5t18000_a0.001_d400_c400_s20',
    '../models/lplc_fp64-32x2-64x2-96x5-128x5t18000_a0.001_d400_c400_s20']
  precisions = ['float32', 'float32', 'float32', 'float32', 'float32',
                'float32', 'float64', 'float64', 'float32', 'float64']
  architectures = [
      [128, 128, 96, 96, 64, 64, 32, 32, 1],
      [32, 32, 64, 64, 96, 96, 128, 128],
      [128, 128, 128, 128, 128, 96, 96, 96, 96, 96, 64, 64, 32, 32, 1],
      [32, 32, 64, 64, 96, 96, 96, 96, 96, 128, 128, 128, 128, 128],
      [128, 128, 128, 128, 128, 96, 96, 96, 96, 96, 64, 64, 32, 32, 1],
      [32, 32, 64, 64, 96, 96, 96, 96, 96, 128, 128, 128, 128, 128],
      [128, 128, 128, 128, 128, 96, 96, 96, 96, 96, 64, 64, 32, 32, 1],
      [32, 32, 64, 64, 96, 96, 96, 96, 96, 128, 128, 128, 128, 128],
      [32, 32, 64, 64, 96, 96, 96, 96, 96, 128, 128, 128, 128, 128],
      [32, 32, 64, 64, 96, 96, 96, 96, 96, 128, 128, 128, 128, 128] ]
  modelInfos = [
      'FC 8 layers 2000 samples',  'LPFC 8 layers 2000 samples',
      'FC 14 layers 4000 samples', 'LPFC 14 layers 4000 samples',
      'FC 14 layers 8000 samples', 'LPFC 14 layers 8000 samples',
      'FC 14 layers 8000 samples double precision',
      'LPFC 14 layers 8000 samples double precision',
      'LPFC 14 layers 18000 samples',
      'LPFC 14 layers 18000 samples double precision' ]
  modelTypes = [0, 1, 0, 1, 0, 1, 0, 1, 1, 1]
else:
  models.append(args.model)
  precisions.append(args.precision)
  architectures.append(args.arch)
  modelTypes.append(args.modelType)

maes = []
mars = []
for m, model in enumerate(models):
  # setup keras percision
  keras.backend.set_floatx(precisions[m])
  # create model
  if modelTypes[m] == 0:
    psnNet = PsnModel.PsnFC(width=architectures[m])
  elif modelTypes[m] == 1:
    psnNet = PsnModel.PsnXyFC(width=architectures[m])
  # load checkpoint
  psnNet.load_weights(tf.train.latest_checkpoint(model)).expect_partial()
  # create the predictor data generator
  nSampleUsed = 20600
  nSampleTest = 400
  s = nSampleUsed
  e = s + nSampleTest
  nSolPerBatch = 1
  predGen = PredictGen(p, nSampleUsed, nSampleUsed+nSampleTest, \
                       pMax, pMin, nCell, nSolPerBatch)
  # evaluate error and residual
  rhs = np.zeros((nCell, nCell))
  mae = np.zeros(nSampleTest)
  mar = np.zeros(nSampleTest)
  resL0L1  = np.zeros((nSampleTest, 2))
  errL0L1  = np.zeros((nSampleTest, 2))
  rerrL0L1 = np.zeros((nSampleTest, 2))
  for i in range(0, nSampleTest, nSolPerBatch):
    inputs = next(predGen)
    error, residual = psnNet.infer_error_and_residual(inputs, nCell, nSolPerBatch)
    error    = np.squeeze(np.absolute(error * (pMax - pMin)))
    residual = np.squeeze(np.absolute(residual * (pMax - pMin)))
    mask     = np.absolute(p[s+i, :, :]) > 1e-3
    mask     = mask.astype(np.int)
    rerror   = np.absolute(error[:,:]/(p[s+i,:,:]+1e-10)) * mask
    mae[i]   = np.mean(error[1:-1,1:-1])
    mar[i]   = np.mean(residual[1:-1,1:-1])
    resL0L1[i,:]  = np.amax(residual[1:-1,1:-1]), np.mean(residual[1:-1,1:-1])
    errL0L1[i,:]  = np.amax(error[1:-1,1:-1]), np.mean(error[1:-1,1:-1])
    rerrL0L1[i,:] = np.amax(rerror[1:-1,1:-1]), np.mean(rerror[1:-1,1:-1])
  maes.append(np.mean(mae))
  mars.append(np.mean(mar))

  # print('MAE {:e} MAR {:e}'.format(np.mean(mae), np.mean(mar)))
  # print('Evaluate on {} samples'.format(nSampleTest))
  # print('Residual {:e} {:e}'.format(np.amax(resL0L1[:,0]), np.mean(resL0L1[:,1])))
  # print('Error    {:e} {:e}'.format(np.amax(errL0L1[:,0]), np.mean(errL0L1[:,1])))
  # print('Rerror   {:e} {:e}'.format(np.amax(rerrL0L1[:,0]), np.mean(rerrL0L1[:,1])))

for m in range(len(maes)):
  if args.reproduce:  print(modelInfos[m])
  print('MAE {:e} MAR {:e}'.format(maes[m], mars[m]))
