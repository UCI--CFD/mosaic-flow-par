import numpy as np
import tensorflow as tf
from tensorflow import keras

keras.backend.set_floatx('float64')

def create_ff0(nCell):
  bcInput = keras.Input(shape=(4*nCell,), name='bc')
  coordInput = keras.Input(shape=(2,), name='coordinate')
  bcCoord = keras.layers.concatenate([bcInput, coordInput], axis=-1, name='cat-bc-coord')
  bcCoordLayer1Out = keras.layers.Dense(4*nCell, activation='swish', name='bcCoordDense1')(bcCoord)
  bcCoordLayer2Out = keras.layers.Dense(4*nCell, activation='swish', name='bcCoordDense2')(bcCoordLayer1Out)
  bcCoordLayer3Out = keras.layers.Dense(4*nCell, activation='swish', name='bcCoordDense3')(bcCoordLayer2Out)
  bcCoordLayer4Out = keras.layers.Dense(2*nCell, activation='swish', name='bcCoordDense4')(bcCoordLayer3Out)
  bcCoordLayer5Out = keras.layers.Dense(1*nCell, activation='swish', name='bcCoordDense5')(bcCoordLayer4Out)
  solution = keras.layers.Dense(1, activation='swish', name='bcCoordDense6')(bcCoordLayer5Out)
  psnNet = keras.Model([bcInput, coordInput], solution, name='poisson_model')
  return psnNet


def create_genome_input(p, pMax, pMin, nx, ny, gx, gy, genome=None, method='boundary'):
  assert method in ['boundary', 'halo']
  assert genome in ['basic', 'i-shift', 'j-shift']
  assert nx % gx == 0
  assert ny % gy == 0
  h  = 1.0/gx if gx < gy else 1.0/gy

  xs = 1+gx//2    if genome == 'i-shift' else 1
  xe = nx+1-gx//2 if genome == 'i-shift' else nx+1 
  ys = 1+gy//2    if genome == 'j-shift' else 1
  ye = ny+1-gy//2 if genome == 'j-shift' else ny+1

  nGenome = max(((xe-xs)//gx) * ((ye-ys)//gy), 1)
  bcs     = np.zeros((nGenome*gx*gy, 2*(gx+gy)))
  coords  = np.zeros((nGenome*gx*gy, 2))
  #print(nGenome, nx, ny, 1)

  m = 0
  for gi in range(xs, xe, gx):
    for gj in range(ys, ye, gy):
      bc = np.zeros(2*(gx+gy))
      bc[       :     gy]  = 0.5 * (p[gi, gj:gj+gy] + p[gi-1, gj:gj+gy])
      bc[     gy:gx+  gy]  = 0.5 * (p[gi:gi+gx, gj+gy-1] + p[gi:gi+gx, gj+gy])
      bc[gx+  gy:gx+2*gy]  = 0.5 * (p[gi+gx-1, gj+gy-1:gj-1:-1] + p[gi+gx, gj+gy-1:gj-1:-1])
      bc[gx+2*gy:       ]  = 0.5 * (p[gi+gx-1:gi-1:-1, gj] + p[gi+gx-1:gi-1:-1, gj-1])
      bc[:] = (bc[:] - pMin) / (pMax - pMin)
      for i in range(gi, gi+gx):
        for j in range(gj, gj+gy):
          bcs[m, :]    = bc[:]
          coords[m, 0] = (i - gi + 0.5) * h
          coords[m, 1] = (j - gj + 0.5) * h
          m += 1

  return [bcs, coords]


# def create_genome_input(pMax, pMin, genomes, genome='basic', method='boundary'):
#   assert method in ['boundary', 'halo']
#   assert genome in ['basic', 'i-shift', 'j-shift']

#   gx = genomes[0][0].shape[0]
#   gy = genomes[0][0].shape[1]
#   h  = 1.0 / gx

#   gType = 0
#   if genome == 'i-shift':
#     gType = 1
#   elif genome == 'j-shift':
#     gType = 2

#   nGenome = len(genomes[gType])
#   bcs     = np.zeros((nGenome*gx*gy, 2*(gx+gy)))
#   coords  = np.zeros((nGenome*gx*gy, 2))

#   m = 0
#   for g in genomes[gType]:
#     bc = np.zeros(2*(gx+gy))
#     bc[       :     gy]  = 0.5 * (g.vars[0,1:-1,0] + g.vars[1,1:-1,0])
#     bc[     gy:gx+  gy]  = 0.5 * (g.vars[1:-1,-1,0] + g.vars[1:-1,-2,0])
#     bc[gx+  gy:gx+2*gy]  = 0.5 * (g.vars[-1,-2:0:-1,0] + g.vars[-2,-2:0:-1,0])
#     bc[gx+2*gy:       ]  = 0.5 * (g.vars[-2:0:-1,0,0] + g.vars[-2:0:-1,1,0])
#     bc[:] = (bc[:] - pMin) / (pMax - pMin)
#     for i in range(1, 1+gx):
#       for j in range(1, 1+gy):
#         bcs[m, :]    = bc[:]
#         coords[m, 0] = (i - 0.5) * h
#         coords[m, 1] = (j - 0.5) * h
#         m += 1

#   return [bcs, coords]
