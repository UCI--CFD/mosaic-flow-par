import sys
sys.path.append('../src')
import numpy as np
from scipy import interpolate
import random
import pyamg
from pyamg.gallery import poisson
import argparse
import tensorflow as tf
import Poisson_eqn as PsnEqn
import Poisson_model as PsnModel
import h5py as h5

PI = np.pi
random.seed(2)

# command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-dsize', '--domain_size', nargs=2, help='domain size', type=int)
parser.add_argument('-gsize', '--genome_size', nargs=2, help='genome size', type=int)
args = parser.parse_args()

nx, ny = args.domain_size
gx, gy = args.genome_size
assert ny % gy == 0
assert nx % gx == 0

# domain length and grid space
h  = 1.0/gx if gx < gy else 1.0/gy
lx = nx * h
ly = ny * h
print('domain length {:f} {:f}, grid step {:f}'.format(lx, ly, h))

# output data file
solFile = h5.File('poisson_infer_history.h5', 'w')

# variable with random dirichlet bc
p = np.zeros((nx+2, ny+2))
PsnEqn.set_random_halo(p, random_min=-0.5, random_max=0.5)

# rhs, using rhs function values
rhs = np.zeros((nx, ny))

# solver for whole domain and write to file
PsnEqn.solve(p, rhs)
solData = solFile.create_dataset('solution', (1, nx+2, ny+2), \
                                 compression='gzip', compression_opts=9, \
                                 dtype='float64', chunks=True)
solData[0,:,:] = p[:,:]

# data buffer for genomes
pIter0 = np.copy(p)
pIter1 = np.copy(p)
pIter0[1:-1,1:-1] = 0.0
pIter1[1:-1,1:-1] = 0.0
Ag = poisson((gx, gy), format='csr')

# iteration space
genomeTypes  = ['basic', 'i-shift', 'j-shift']
xs, xe = [1, 1+gx//2, 1], [nx+1, nx+1-gx//2, nx+1]
ys, ye = [1, 1, 1+gy//2], [ny+1, ny+1, ny+1-gy//2]

# iterate with AMG solver
maxIter       = 1000
nIterConverge = 0
pAMGHist = np.zeros((maxIter, 3, nx+2, ny+2))
for i in range(maxIter):
  for iType in range(3): 
    if xs[iType] >= xe[iType] or ys[iType] >= ye[iType]:
      continue
    for gi in range(xs[iType], xe[iType], gx):
      for gj in range(ys[iType], ye[iType], gy):
        PsnEqn.solve_genome(pIter0, Ag, rhs, gi, gj, gx, gy, pIter1)
    err = np.absolute(p - pIter1)
    res = np.absolute(pIter0 - pIter1)
    print('{} {:7s} error {:e} {:e} residual {:e} {:e}' .format(i, genomeTypes[iType], \
          np.amax(err), np.sum(err)/nx/ny, np.amax(res), np.sum(res)/nx/ny))
    pIter0[:,:] = pIter1[:,:]
    pAMGHist[i,iType,:,:] = pIter0[:,:]
    if np.amax(res) < 1.0e-12: nIterConverge = i+1
  if nIterConverge > 0: break
# write history
AMGData = solFile.create_dataset('AMGHistory', (nIterConverge, 3, nx+2, ny+2), \
                                 compression='gzip', compression_opts=9, \
                                 dtype='float64', chunks=True)
AMGData[...] = pAMGHist[:nIterConverge,...]


# load model
# hardcoded gx=gy and pMax, pMin values
pMax =  1.8346210104700842
pMin = -1.7867983987223126
psnNet = PsnModel.create_ff0(gx)
modelPath = '../model/swish_rs-2_b20/'
modelName = 'psnModel_cat-bc-coord_128-128-128-64-32-1swish_rs-2_b20_ep4012'
# modelName = 'psnModel_cat-bc-coord_128-128-128-64-32-1swish_rs-2_b20'
psnNet.load_weights(modelPath + modelName + '.h5')

# iterate with genomes
maxIter = 1000
pIter0[1:-1,1:-1] = 0.0
pIter1[1:-1,1:-1] = 0.0
pPredHist = np.zeros((maxIter, 3, nx+2, ny+2))
for i in range(maxIter):
  for iType in range(3): 
    if xs[iType] >= xe[iType] or ys[iType] >= ye[iType]:
      continue
    print(iType, genomeTypes[iType])
    inputs = PsnModel.create_genome_input(pIter0, pMax, pMin, nx, ny, gx, gy, genome=genomeTypes[iType])
    pg = psnNet.predict(inputs)
    # pg = psnNet.predict(PsnModel.create_genome_input(pIter0, pMax, pMin, nx, ny,\
                                                     # gx, gy, genome=genomeTypes[iType]))
    pg = pg * (pMax - pMin) + pMin
    s  = 0
    for gi in range(xs[iType], xe[iType], gx):
      for gj in range(ys[iType], ye[iType], gy):
        pIter0[gi:gi+gx, gj:gj+gy] = pg[s:s+gx*gy,0].reshape((gx, gy))
        s += gx * gy
    err = np.absolute(pIter0 - p)
    print('{} {:7s} error {:e} {:e}' .format(i, genomeTypes[iType], \
          np.amax(err), np.sum(err)/nx/ny))
    pPredHist[i,iType,:,:] = pIter0[:,:]
  res = np.absolute(pIter0 - pIter1)
  print('{} residual {:e} {:e}'.format(i, np.amax(res), np.sum(res)/nx/ny))
  if np.amax(res) < 1.0e-12:
    nIterConverge = i+1
    break
  pIter1[...] = pIter0[...]
# write history
predData = solFile.create_dataset('PredHistory', (nIterConverge, 3, nx+2, ny+2), \
                                  compression='gzip', compression_opts=9, \
                                  dtype='float64', chunks=True)
predData[...] = pPredHist[:nIterConverge,...]
