#%%
import numpy as np
import random
import pyamg
from pyamg.gallery import poisson
import h5py as h5
import argparse
import matplotlib.pyplot as plt

#%%
plt.rc('text', usetex=True)

PI = np.pi
random.seed(2)

ni = 32
nj = 64
njHalf = int(nj/2)

h = 1.0 / ni

#%%
#--------------------------------------------------------
# set up matrix, rhs, exact solution
#--------------------------------------------------------
# matrix
AFull = poisson((ni, nj), format='csr')
AHalf = poisson((ni, njHalf), format='csr')
#Atest = poisson((5,5))
#print(Atest.todense())

# variable with dirichlet bc
pFull0 = np.zeros((ni+2, nj+2))
for i in range(ni+2):
  for j in range(nj+2):
    x = (j - 0.5) * h
    y = (i - 0.5) * h
    pFull0[i, j] = 0.25*(-x*x + y*y + x*y)
pFull0[1:-1, 1:-1] = 0.0
## add bc values to rhs
#for j in range(1,nj+1):
  #pFull0[ 0,j] = random.uniform(-1,1)
  #pFull0[-1,j] = random.uniform(-1,1)
#for i in range(1, ni+1):
  #pFull0[i, 0] = random.uniform(-1,1)
  #pFull0[i,-1] = random.uniform(-1,1)

# rhs
bFull = np.zeros((ni, nj))
# for i in range(0, ni):
#   for j in range(0, nj):
#     x = (i + 0.5) * h
#     y = (j + 0.5) * h
#     bFull[i,j] = -8*PI*PI*np.cos(2*PI*x)*np.cos(2*PI*y) * (-h*h)
# add bc to rhs
bFull[:, 0] += pFull0[1:-1, 0]
bFull[:,-1] += pFull0[1:-1,-1]
bFull[ 0,:] += pFull0[ 0,1:-1]
bFull[-1,:] += pFull0[-1,1:-1]

# solve for global
rhsFull = np.reshape(bFull, (ni*nj), order='C')
x = pyamg.solve(AFull, rhsFull, verb=False, tol=1e-14)
pFull0[1:-1,1:-1] = np.reshape(x, (ni, nj), order='C')

# # write a file
# with open('sol.dat', 'w') as fout:
#   for i in range(1, ni+1):
#     for j in range(1, nj+1):
#       fout.write('{:.4e} {:.4e} {:.4e}\n'.format((j-0.5)*h, (i-0.5)*h, pFull0[i,j]))
#     fout.write('\n')

#%%
# # print(np.amin(pFull0), np.amax(pFull0))
# fig = plt.figure(figsize=(4.3,2), frameon=False)
# ax = plt.Axes(fig, [0., 0., 1., 1.])
# ax.set_axis_off()
# fig.add_axes(ax)
# tmp = ax.imshow(pFull0[1:-1,1:-1], interpolation='gaussian', aspect='auto', \
#                 cmap='rainbow', vmax=0.4, vmin=-1)
# cbar=plt.colorbar(tmp, ax=ax, shrink=0.8, pad=0.04, orientation='vertical',
#                   ticks=[-1, 0.4], aspect=12)
# cbar.ax.tick_params(labelsize=18)
# cbar.ax.set_yticklabels(['-1', '0.4'])
# # plt.axis('off')
# plt.savefig('laplace_example.pdf')

#%%
#--------------------------------------------------------
# create/reset intermeidate variables
#--------------------------------------------------------
pFull1 = np.zeros((ni+2, nj+2))
pFull1[:, 0] = pFull0[:, 0]
pFull1[:,-1] = pFull0[:,-1]
pFull1[ 0,:] = pFull0[ 0,:]
pFull1[-1,:] = pFull0[-1,:]
pFull2 = np.zeros((ni+2, nj+2))
pFull2[:,:] = pFull1[:,:]

bHalf0 = np.zeros((ni, njHalf))
bHalf1 = np.zeros((ni, njHalf))
rhsHalf = np.zeros(ni*njHalf)
x = np.zeros(ni*njHalf)

#%%
#--------------------------------------------------------
# overlapped genomes
#--------------------------------------------------------
fout = open('overlap.log', 'w')
maxIter = 30
resL0s = np.zeros(maxIter)
resL1s = np.zeros(maxIter)
for i in range(maxIter):
  if i % 2 == 0:
    bHalf0[:,:] = bFull[:,:njHalf]
    bHalf1[:,:] = bFull[:,njHalf:nj]
    bHalf0[:,-1] = bFull[:,njHalf-1] + pFull1[1:-1,njHalf+1]
    bHalf1[:, 0] = bFull[:,njHalf] + pFull1[1:-1,njHalf]
    #
    rhsHalf[:] = np.reshape(bHalf0, (ni*njHalf), order='C')
    x[:] = pyamg.solve(AHalf, rhsHalf, verb=False, tol=1e-14)
    pFull1[1:-1,1:njHalf+1] = np.reshape(x, (ni, njHalf), order='C')
    #
    rhsHalf[:] = np.reshape(bHalf1, (ni*njHalf), order='C')
    x[:] = pyamg.solve(AHalf, rhsHalf, verb=False, tol=1e-14)
    pFull1[1:-1,njHalf+1:-1] = np.reshape(x, (ni, njHalf), order='C')
  else:
    s = int(nj/4)
    e = int(nj*3/4)
    bHalf0[:,:] = bFull[:,s:e]
    bHalf0[:,0] = bFull[:,s] + pFull1[1:-1,s]
    bHalf0[:,-1] = bFull[:,e-1] + pFull1[1:-1,e+1]
    rhsHalf[:] = np.reshape(bHalf0, (ni*njHalf), order='C')
    x[:] = pyamg.solve(AHalf, rhsHalf, verb=False, tol=1e-14)
    pFull1[1:-1,s+1:e+1] = np.reshape(x, (ni, njHalf), order='C')
  residualL0 = np.amax(abs(pFull1 - pFull0))
  residualL1 = np.sum(abs(pFull1 - pFull0)) / ni / nj
  resL0s[i] = residualL0
  resL1s[i] = residualL1
  print('{}, {:.4e}, {:.4e}'.format(i, residualL0, residualL1))
  fout.write('{} {:.4e}\n'.format(i, residualL1))
fout.close()

#fig = plt.figure(figsize=(4,4))
#x = np.arange(maxIter)
#plt.plot(x, resL0s, '-b')
#plt.yscale('log')
##plt.xscale('log')
#plt.title('Convergence History')
#plt.ylabel('residual')
#plt.xlabel('iterations')
#plt.savefig('gnomeResL0.pdf')

#%%
#--------------------------------------------------------
# non-overlapped genomes
#--------------------------------------------------------
fout = open('non_overlap.log', 'w')
maxIter = 400
for i in range(maxIter):
  bHalf0[:,:] = bFull[:,:njHalf]
  bHalf1[:,:] = bFull[:,njHalf:nj]
  bHalf0[:,-1] = bFull[:,njHalf-1] + pFull1[1:-1,njHalf+1]
  bHalf1[:, 0] = bFull[:,njHalf] + pFull1[1:-1,njHalf]
  #
  rhsHalf[:] = np.reshape(bHalf0, (ni*njHalf), order='C')
  x[:] = pyamg.solve(AHalf, rhsHalf, verb=False, tol=1e-14)
  pFull1[1:-1,1:njHalf+1] = np.reshape(x, (ni, njHalf), order='C')
  #
  rhsHalf[:] = np.reshape(bHalf1, (ni*njHalf), order='C')
  x[:] = pyamg.solve(AHalf, rhsHalf, verb=False, tol=1e-14)
  pFull1[1:-1,njHalf+1:-1] = np.reshape(x, (ni, njHalf), order='C')
  #
  residualL0 = np.amax(abs(pFull1 - pFull0))
  residualL1 = np.sum(abs(pFull1 - pFull0)) / ni / nj
  print('{} {:.4e} {:.4e}'.format(i, residualL0, residualL1))
  fout.write('{} {:4e}\n'.format(i+1, residualL1))
fout.close()

# # compute residuals
# residualL0 = 0.0
# residualL1 = 0.0
# for i in range(1, nPoint-1):
#   for j in range(1, 2*nPoint-2):
#     x = i * h
#     y = j * h
#     b = -20*PI*PI * np.sin(2*PI*x) * np.sin(4*PI*y)
#     residual    = abs(b - (pFull0[i-1,j] + pFull0[i+1,j] + pFull0[i,j+1] + pFull0[i,j-1] - 4*pFull0[i,j])/h/h)
#     residualL1 += residual
#     residualL0  = max(residualL0, residual)
# print("residual L0: {:.4e}".format(residualL0))
# print("residual L1: {:.4e}".format(residualL1))
