#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 14:11:37 2021

@author: robertplanas
"""

# =============================================================================
# Inputs
# =============================================================================

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', default="lplc-32x2-64x2-96x5-128x5t18000_a0.001_d400_c400_s20",
                    help='Select a model')
parser.add_argument('-d', '--data', default="../data/Sin_BC_AMG_64.h5",
                    help='Select a data file')
parser.add_argument('-l', '--arch', type=int, nargs='*', help='architecture',
                    default=[32, 32, 64, 64, 96, 96, 96, 96, 96, 128, 128, 128, 128, 128])
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')
parser.add_argument('-n', '--name', default="BC = sin(x/Width)",
                    help='Name of the output file ')
parser.add_argument('-print', '--print', default=False,
                    help='Print to a file if true')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')

args = parser.parse_args()

GPU = None
if(GPU is not None):
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = GPU

import sys
import os
import h5py as h5
import numpy as np
import tensorflow as tf
sys.path.append(os.path.dirname(__file__))
#sys.path.append(os.path.join(os.path.dirname(__file__), "../Laplace_Genomes_src/"))
#sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
#sys.path.append('./../src/')
import pinn_model as psnModel
#sys.path.append('./../Laplace_Genomes_src/')

import Genomes_FF as G_FF
import time

#%%
# =============================================================================
# Print File
# =============================================================================


def Print_vector_File(File_Name, Vector, Vector_Name):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt, "a")
    file1.write(Vector_Name + " = [")
    for index, element in enumerate(Vector):
        if(index == len(Vector) - 1):
            file1.write(str(element) + "]" + "\n")
        else:
            file1.write(str(element) + ',')
    file1.close()


def Print_String_File(File_Name, String):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt, "a")
    file1.write(String + " \n")
    file1.close()



# =============================================================================
# Load Model
# =============================================================================
Data_Type = args.DataType
tf.keras.backend.set_floatx(Data_Type)
modelPath = os.path.join(os.path.dirname(__file__), "../models/")
modelName = args.model
Model_FF_BC = psnModel.PsnXyFC(width=args.arch)
Model_FF_BC.load_weights(tf.train.latest_checkpoint(modelPath + modelName))
Model_FF_BC.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3))
bc_zeros = np.zeros((2, 128))
xy_zeros = np.zeros((2, 2))
output_zeros = np.zeros((2, 1))
w_zeros = np.zeros((2, 2))
bc_xy_zeros = [bc_zeros, xy_zeros, w_zeros]
Model_FF_BC.evaluate(bc_xy_zeros, output_zeros, verbose=2)
Model_FF_BC.trainable = False
# Freeze the weights
for layer in Model_FF_BC.layers:
    layer.trainable = False

MAEs = []
TIMES = []
Strings = []

if args.reproduce:

    Data = ["../data/Sin_BC_AMG_32.h5",
            "../data/Sin_BC_AMG_64.h5",
            "../data/Sin_BC_AMG_96.h5",
            "../data/Sin_BC_AMG_128.h5",
            "../data/Sin_BC_AMG_160.h5",
            "../data/Sin_BC_AMG_192.h5",
            "../data/Sin_BC_AMG_256.h5",
            "../data/Sin_BC_Period_1_AMG_32.h5",
            "../data/Sin_BC_Period_1_AMG_64.h5",
            "../data/Sin_BC_Period_1_AMG_96.h5",
            "../data/Sin_BC_Period_1_AMG_128.h5",
            "../data/Sin_BC_Period_1_AMG_192.h5",
            "../data/Sin_BC_Period_1_AMG_256.h5"]
    
    Names = ["BC = sin(x/1)",
             "BC = sin(x/2)",
             "BC = sin(x/3)",
             "BC = sin(x/4)",
             "BC = sin(x/5)",
             "BC = sin(x/6)",
             "BC = sin(x/8)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)"]

else:
    Data = [args.data]
    Names = [args.name]
    
for index, data in enumerate(Data):    
    # =============================================================================
    # Load data
    # =============================================================================
    File_AMG = os.path.join(os.path.dirname(__file__), data)
    psnFile = h5.File(File_AMG, 'r')
    nSample = psnFile.attrs['nSample']
    nCell = psnFile.attrs['domainSize'][0]
    psnData = psnFile['LaplaceSolution']
    print('# Samples: {}'.format(nSample))
    print('# Cells: {}*{}'.format(nCell, nCell))
    print('data shape: ', end='')
    print(psnData.shape)
    p = np.zeros(psnData.shape)
    p = psnData[:, :, :]
    pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
    print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))
    psnFile.close()
    #%
    # Define the width of the domain
    Width = (len(p[0, :, :]) - 2) / 32
    Pixels = Width * 32
    
    # Default max and min values used in training to normalize the inputs
    MIN = -1.785187
    MAX = 2.011001
    # Normalize
    U_N = G_FF.Normalize_Domain(np.squeeze(
        p), Min=-1.785187, Max=2.011001).astype(Data_Type)
    # Define the domain with ones, excluding the halo borders
    Domain_Interior_Ones = np.zeros_like(U_N)
    Domain_Interior_Ones[1:-1, 1:-1] = 1
    # Define the domain with nan values for the points where no information is available, defined here for square domains
    Domain_Int_Nan = np.ones_like(U_N) * U_N
    Domain_Int_Nan[2:-2, 2:-2] = np.nan
    
    Gnomes, Shared_Borders = G_FF.Generate_Genomes_Automatically(Width=Width, Height=Width, Genome_width=1,
                                                                 Points_Borders=32, Domain_Interior_Ones=Domain_Interior_Ones,
                                                                 Domain_Int_Nan=Domain_Int_Nan)
    start_time = time.time()
    MAE, MSE, Convergences, Solution = G_FF.Iterative_Solution_Optimized(Shared_Borders, Gnomes, Model_FF_BC,
                                                                         U_N, Max=MAX, Min=MIN, Iterations=200, Un_normalize=True)
    
    Time = time.time() - start_time
    
    Current_Area = "------- " + Names[index] +  " Area " + str(Width**2) + " Data_Type = " + Data_Type + "  -------"
    MAEs.append(MAE)
    TIMES.append(Time)
    Strings.append(Current_Area)
    
    if(args.print):
        
        Name_File = os.path.join(os.path.dirname(__file__), "Test_Iterative_Square")
        Print_String_File(Name_File, Current_Area)
        Print_vector_File(Name_File, [MAE], "MAE")
        Print_vector_File(Name_File, [MSE], "MSE")
        Print_vector_File(Name_File, [Time], "Times")

print("Final resutls -----------")
for index, MAE in enumerate(MAEs):
    Time = TIMES[index]
    print(Strings[index])
    print("       MAE = ", MAE)
    print("       Elapsed Time = ", Time)
