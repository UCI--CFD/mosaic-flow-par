import sys
sys.path.append('../src')
import os
import h5py as h5
import numpy as np
import tensorflow as tf
from tensorflow import keras
import argparse
import pinn_model as psnModel
from pinn_utils import *
import time

keras.backend.set_floatx('float64')

parser = argparse.ArgumentParser()

# architecture
parser.add_argument('-m', '--model', type=int, default=1, \
                    help='0 - psnfc, 1 - psnxyfc')
parser.add_argument('-l', '--arch', type=int, nargs='*', help='architecture')
parser.add_argument('-last', '--lastLayerAct', default='tanh',\
                    help='activation function for last layer')

# coefficient for pde error
parser.add_argument('-alpha', '--alpha', type=float, default=0.01, \
                    help='regularization of pde')
parser.add_argument('-r', '--reg', type=float, nargs='*', default=None,\
                    help='l2 regularization')

# control training
parser.add_argument('-f', '--file', default='../data/Laplace32_3000.h5', \
                    help='data file')
parser.add_argument('-split', '--split', type=float, default=0.1, help='split ratio')
parser.add_argument('-name', '--name', default='nsPinn', help='model name prefix')
parser.add_argument('-ie', '--initEpoch', type=int, default=0, \
                    help='the start epoch')
parser.add_argument('-e', '--nEpoch', type=int, default=500, \
                    help='initial train epochs')
parser.add_argument('-a', '--adapt', type=int, default=20, \
                    help='number of adapting collocation')
parser.add_argument('-ae', '--nEpochAdapt', type=int, default=500, \
                    help='epochs for each adapt')
parser.add_argument('-restart', '--restart', default=False, action='store_true',\
                    help='restart from checkpoint')
parser.add_argument('-ckpnt', '--checkpoint', default=None, help='checkpoint name')

# learning rate adjustment
parser.add_argument('-lr0', '--lr0', type=float, default=5e-4, help='init leanring rate')
parser.add_argument('-lrmin', '--lrmin', type=float, default=1e-7, help='min leanring rate')
parser.add_argument('-p', '--patience', type=int, default=200, \
                    help='patience for reducing learning rate')
parser.add_argument('-lr',  '--restartLr', type=float, default=None, \
                     help='learning rate to restart training')

# data and collocation points
parser.add_argument('-t', '--nTrain', type=int, default=2000, \
                    help='number of training solutions ')
parser.add_argument('-d', '--nDataPoint', type=int, default=0,\
                    help='number of data points in training')
parser.add_argument('-c', '--nCollocPoint', type=int, default=400,\
                    help='number of collocation points in training')
parser.add_argument('-s', '--nSolPerBatch', type=int, default=20, \
                    help='number of solution per batch')

# save more info
parser.add_argument('-g', '--saveGradStat', default=False, action='store_true',\
                    help='save gradient statistics')
args = parser.parse_args()

archStr   = ''
l0        = args.arch[0]
nSameSize = 1
for l in args.arch[1:]:
  if l == l0:
    nSameSize += 1
  else:
    if nSameSize == 1:
      archStr = archStr + '-' + repr(l0) 
    else:
      archStr = archStr + '-{}x{}'.format(l0, nSameSize)
    l0        = l
    nSameSize = 1
if nSameSize == 1:
  archStr = archStr + '-' + repr(l0)
else:
  archStr = archStr + '-{}x{}'.format(l0, nSameSize)
print('architecture ' + archStr)


# load hdf5 data and test residual of Poisson (psn)
psnFile = h5.File(args.file, 'r')
nSample = psnFile.attrs['nSample']
nCell   = psnFile.attrs['domainSize'][0]
psnData = psnFile['PoissonSolution']
print('# Samples: {}'.format(nSample))
print('# Cells: {}*{}'.format(nCell, nCell))
print('data shape: ', end='')
print(psnData.shape)

# read the data into numpy array
p = np.zeros(psnData.shape)
p = psnData[:,:,:]
pMin, pMax = np.amin(p[:,:,:]), np.amax(p[:,:,:])
print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))

#split traning and validation
nUsed = args.nTrain
nValid = int(args.split * nUsed)
nTrain  = nUsed - nValid
nSolPerBatch = args.nSolPerBatch
print('{} solutions in training, {} in validation, {} per batch'.format(\
        nTrain, nValid, nSolPerBatch))

# initial collocation points
nDatPnt1D = np.int(np.sqrt(args.nDataPoint))
points    = np.zeros((nUsed, args.nCollocPoint, 2))
gNorm     = np.zeros((nSolPerBatch, nCell, nCell))
# use an actficial density to put more points to the boudnary
for s in range(0, nUsed, nSolPerBatch):
  for ss in range(nSolPerBatch):
    for i in range(nCell):
      for j in range(nCell):
        gNorm[ss, i, j] = max(abs(i-nCell/2+0.5), abs(j-nCell/2+0.5));
  adapt_collocation(gNorm, points, s, stride=4, nearBoundary=2)

# define generator for training and validation
trainGen = BcXybcXyWMixGen(p, points, 0, nTrain, nSolPerBatch, \
                           nDataPoint1D=nDatPnt1D)
validGen = BcXybcXyWMixGen(p, points, nTrain, nUsed, nSolPerBatch, \
                           nDataPoint1D=nDatPnt1D)
#validGen = BcXybcXyWBandGen(p, nTrain, nUsed, nSolPerBatch);

# define model
modelName = args.name + archStr + 't{}_a{}_d{}_c{}_s{}'.format(args.nTrain,\
               str(args.alpha), args.nDataPoint, args.nCollocPoint, \
               nSolPerBatch)
if args.model == 0:
  with psnModel.strategy.scope():
    psnNet = psnModel.PsnFC(width=args.arch, alpha=args.alpha, reg=args.reg, \
                            save_grad_stat=args.saveGradStat, \
                            last_act=args.lastLayerAct)
    psnNet.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))
  auxNet = psnModel.PsnFC(width=args.arch, alpha=args.alpha, reg=args.reg, \
                          last_act=args.lastLayerAct)
else:
  with psnModel.strategy.scope():
    psnNet = psnModel.PsnXyFC(width=args.arch, alpha=args.alpha, reg=args.reg, \
                              save_grad_stat=args.saveGradStat, \
                              last_act=args.lastLayerAct)
    psnNet.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))
  auxNet = psnModel.PsnXyFC(width=args.arch, alpha=args.alpha, reg=args.reg, \
                          last_act=args.lastLayerAct)

# callbacks
psnCallbacks = [keras.callbacks.ModelCheckpoint(filepath='./'+modelName+'/checkpoint', \
                                                monitor='mae', save_best_only=True,\
                                                save_weights_only=True, verbose=1),\
                keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.8, \
                                                  patience=args.patience, min_delta=0.01,\
                                                  min_lr=args.lrmin),\
                keras.callbacks.CSVLogger(modelName+'.log', append=True)]

# load checkpoints if restart
if args.restart:
  ckpntName = modelName if args.checkpoint == None else args.checkpoint
  psnNet.load_weights(tf.train.latest_checkpoint(ckpntName))
  if args.restartLr != None:
    keras.backend.set_value(psnNet.optimizer.learning_rate, args.restartLr)

# initial training, using grid cells as collocation points
psnNet.fit(trainGen, validation_data=validGen, \
           initial_epoch=args.initEpoch, epochs=args.nEpoch,\
           steps_per_epoch=nTrain//nSolPerBatch,\
           validation_steps=nValid//nSolPerBatch,\
           verbose=2, callbacks=psnCallbacks)
psnNet.summary()

# training with adaptive collocation points
gridGen = BcXybcXydupGen(p, 0, nTrain, nSolPerBatch)
epochPrev = args.nEpoch
for m in range(args.adapt):
  # adapt points
  auxNet.load_weights(tf.train.latest_checkpoint(modelName))
  for s in range(0, nUsed, nSolPerBatch):
    inputs = next(gridGen)
    gNorm  = auxNet.grad_xy_norm(inputs, nCell, nSolPerBatch)
    adapt_collocation(gNorm, points, s, stride=4, nearBoundary=2)
  # new training generator with adapted points
  trainGen = BcXybcXyWMixGen(p, points, 0, nTrain, nSolPerBatch, \
                             nDataPoint1D=nDatPnt1D)
  validGen = BcXybcXyWMixGen(p, points, nTrain, nUsed, nSolPerBatch, \
                             nDataPoint1D=nDatPnt1D)
  # train
  print('--------------------------------')
  print('Adapt collocation points')
  print('--------------------------------')
  psnNet.fit(trainGen, validation_data=validGen, \
             initial_epoch=epochPrev, epochs=epochPrev+args.nEpochAdapt, \
             steps_per_epoch=nTrain//nSolPerBatch,\
             validation_steps=nValid//nSolPerBatch,\
             verbose=2, callbacks=psnCallbacks)
  epochPrev += args.nEpochAdapt
