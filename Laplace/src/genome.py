import numpy as np
from block import *
import copy as cp

class GenomeMap:
  def __init__(self, block, blockRange, genomeRange):
    self.block = block
    self.bRng  = blockRange
    self.gRng  = genomeRange


class Genome:
  def __init__(self, gSizej=32, gSizei=32, nVar=1, nCoord=2):
    self.nCoord = nCoord
    self.nVar   = nVar
    # map between blocks' data range to genome's.
    self.maps   = []
    # data extracted from blocks
    self.shape     = (gSizei, gSizej)
    self.coords    = np.zeros((gSizei+2, gSizej+2, nCoord))
    self.vars      = np.zeros((gSizei+2, gSizej+2, nVar))
    self.varsBound = np.zeros((nVar, 2))
    self.coordsMin = np.zeros(nCoord)
    # input for networks
    self.bc         = np.zeros(2*(gSizej+gSizei)*nVar)


  def add_map(self, map):
    maps.append(cp.deepcopy(map))


  def add_map(self, block, blockRange, genomeRange):
    self.maps.append(GenomeMap(block, blockRange, genomeRange))

  
  def setup_bc(self):
    # i-
    s, e = 0, self.nVar * self.shape[1]
    self.bc[s:e] = np.reshape(0.5*(self.vars[0,1:-1,:] + self.vars[1,1:-1,:]), (e-s))
    # j+
    s, e = e, e + self.nVar * self.shape[0]
    self.bc[s:e] = np.reshape(0.5*(self.vars[1:-1,-2,:] + self.vars[1:-1,-1,:]), (e-s))
    # i+
    s, e = e, e + self.nVar * self.shape[1]
    self.bc[s:e] = np.reshape(0.5*(self.vars[-1,-2:0:-1,:] + self.vars[-2,-2:0:-1,:]), (e-s))    
    # j-
    s, e = e, e + self.nVar * self.shape[0]
    self.bc[s:e] = np.reshape(0.5*(self.vars[-2:0:-1,0,:] + self.vars[-2:0:-1,1,:]), (e-s))


  def extract_from_block(self, blocks):
    # flowVars and coordinates shall be list of 2D numpy arrays
    for m in self.maps:
      # copy variables
      self.vars[m.gRng[0]:m.gRng[2], m.gRng[1]-1:m.gRng[3]+1, :] = \
        blocks[m.block].vars[m.bRng[0]:m.bRng[2], m.bRng[1]-1:m.bRng[3]+1,:]
      self.vars[m.gRng[0]-1, m.gRng[1]:m.gRng[3], :] = \
        blocks[m.block].vars[m.bRng[0]-1, m.bRng[1]:m.bRng[3],:]
      self.vars[m.gRng[2], m.gRng[1]:m.gRng[3], :] = \
        blocks[m.block].vars[m.bRng[2], m.bRng[1]:m.bRng[3],:]


  def normalize(self):
    for i in range(self.nVar):
      # self.varsBound[i, 0] = np.amin(self.vars[:,:,i])
      # self.varsBound[i, 1] = np.amax(self.vars[:,:,i])
      self.varsBound[i, 0] = -1.785187
      self.varsBound[i, 1] =  2.011001
      self.vars[:,:,i] = (self.vars[:,:,i] - self.varsBound[i,0]) \
                       / (self.varsBound[i,1] - self.varsBound[i,0])


  def denormalize(self):
    for i in range(self.nVar):
      self.vars[:,:,i] = self.vars[:,:,i] * (self.varsBound[i,1] - self.varsBound[i,0]) \
                       + self.varsBound[i,0]


  def copy_to_block(self, blocks):
    for m in self.maps:
      blocks[m.block].vars[m.bRng[0]:m.bRng[2], m.bRng[1]:m.bRng[3],:] = \
        self.vars[m.gRng[0]:m.gRng[2], m.gRng[1]:m.gRng[3], :]


#----------------------------------------------------------------
# fucntions work on genome objects
#----------------------------------------------------------------


def create_nn_input(genomes, gStart, gEnd, isOldModel=False):
  assert len(genomes) > 0
  # number boundary points
  g0        = genomes[0]
  nBcCell   = 2 * (g0.shape[0] + g0.shape[1])
  nInCell   = g0.shape[0] * g0.shape[1]
  h         = 1.0 / g0.shape[0]
  # allocate inputs for nn
  batchSize = (gEnd - gStart) * g0.shape[0] * g0.shape[1]
  bc        = np.zeros((batchSize, nBcCell))
  xy        = np.zeros((batchSize, 2))
  w         = np.zeros((batchSize, 2))
  # loop over the genomes
  for iG, g in enumerate(genomes[gStart:gEnd]):
    # broadcast this genome's bc to all predition points
    for i in range(iG*nInCell, (iG+1)*nInCell):
      bc[i, :] = g.bc[:]
    # prediction points' coordinates
    iPnt = iG * nInCell
    for i in range(g.shape[0]):
      for j in range(g.shape[1]):
        xy[iPnt, :] = [(i+0.5)*h, (j+0.5)*h]
        iPnt += 1
  #      
  if isOldModel:
    return [bc, xy]
  else:
    return [bc, xy, w]


def infer_genomes_nn(genomes, nn, nGenomePerBatch=20, isOldModel=False, \
                     saveResidual=False):
  assert len(genomes) > 0
  g0 = genomes[0]
  # normalize and setup bc vector
  for g in genomes:
    g.normalize()
    g.setup_bc()
  # infer by batch
  for gStart in range(0, len(genomes), nGenomePerBatch):
    gEnd   = min(gStart + nGenomePerBatch, len(genomes))
    # inputs for nn
    inputs = create_nn_input(genomes, gStart, gEnd, isOldModel=isOldModel)
    # infer
    shape  = (gEnd-gStart, g0.shape[0], g0.shape[1], g0.nVar)
    if not saveResidual:
      p = np.reshape(nn.predict(inputs), shape)
    else:
      p = np.reshape( \
          nn.infer_error_and_residual(inputs, g0.shape[0], gEnd-gStart, \
                                      residualOnly=True), \
          shape)
    # copy back to genomes
    for iG in range(gStart, gEnd):
      genomes[iG].vars[1:-1, 1:-1, :] = p[iG-gStart, :, :, :]
  # denormalize
  if not saveResidual:
    for g in genomes:
      g.denormalize()
  else:
    g.vars = g.vars * (g.varsBound[0,1] - g.varsBound[0,0])


def infer_genomes_ls(genomes, model):
  assert len(genomes) > 0
  for g in genomes:
    g.normalize()
    g.setup_bc()
    model.predict(g)
    g.denormalize()


def create_genome_set(blocks, gShape, genomes, iShift=0, jShift=0):
  # don't support ishifts in both dimension yet
  assert (iShift == 0 or jShift == 0)

  genomes.append([])
  # create genomes inside the block
  for iBlk, b in enumerate(blocks):
    # number of genomes for this block
    nGi = b.shape[0] // gShape[0]
    nGj = b.shape[1] // gShape[1]
    if iShift > 0: nGi -= 1
    if jShift > 0: nGj -= 1
    iRange = np.linspace(1+iShift, 1+b.shape[0]-gShape[0]-iShift, \
                         nGi, dtype=int)
    jRange = np.linspace(1+jShift, 1+b.shape[1]-gShape[1]-jShift, \
                         nGj, dtype=int)
    for iStart in iRange:
      for jStart in jRange:
        g = Genome(gSizei=gShape[0], gSizej=gShape[1])
        gRng = (1, 1, gShape[0]+1, gShape[1]+1)
        bRng = (iStart, jStart, iStart+gShape[0], jStart+gShape[1])
        g.add_map(iBlk, bRng, gRng)
        genomes[-1].append(g)

  # append genomes with shift on i or j border
  if iShift > 0 and jShift == 0:
    create_genome_ishift(blocks, gShape, genomes, iShift)
  if jShift > 0 and iShift == 0:
    create_genome_jshift(blocks, gShape, genomes, jShift)


def create_genome_ishift(blocks, gShape, genomes, iShift):
  for iBlk, b in enumerate(blocks):
    nGj = b.shape[1] // gShape[1]
    jRange = np.linspace(1, 1+b.shape[1]-gShape[1], nGj, dtype=int)

    # ---- i- boundary ---- #
    iToBlk = b.bcs['i-'].toBlock
    toBlk  = blocks[iToBlk]
    if iToBlk > iBlk:
      for jStart in jRange:
        g = Genome(gSizei=gShape[0], gSizej=gShape[1])
        # genome part on this block
        gRng = (1+gShape[0]-iShift, 1, 1+gShape[0], 1+gShape[1])
        bRng = (1, jStart, 1+iShift, jStart+gShape[1])
        g.add_map(iBlk, bRng, gRng)
        # genome part on connected block
        gRng = (1, 1, 1+gShape[0]-iShift, 1+gShape[1])
        bRng = (1+toBlk.shape[0]-(gShape[0]-iShift), jStart, \
                1+toBlk.shape[0], jStart+gShape[1])
        g.add_map(iToBlk, bRng, gRng)
        # save genome to list
        genomes[-1].append(g)

    # ---- i+ boundary ---- #
    iToBlk = b.bcs['i+'].toBlock
    toBlk  = blocks[iToBlk]
    if iToBlk > iBlk:
      for jStart in jRange:
        g = Genome(gSizei=gShape[0], gSizej=gShape[1])
        # genome part on this block
        gRng = (1, 1, 1+gShape[0]-iShift, 1+gShape[1])
        bRng = (1+b.shape[0]-(gShape[0]-iShift), jStart, \
                1+b.shape[0], jStart+gShape[1])
        g.add_map(iBlk, bRng, gRng)
        # genome part on connected block
        gRng = (1+gShape[0]-iShift, 1, 1+gShape[0], 1+gShape[1])
        bRng = (1, jStart, 1+iShift, jStart+gShape[1])
        g.add_map(iToBlk, bRng, gRng)
        # save genome to list
        genomes[-1].append(g)


def create_genome_jshift(blocks, gShape, genomes, jShift):
  for iBlk, b in enumerate(blocks):
    nGi    = b.shape[0] // gShape[0]
    iRange = np.linspace(1, 1+b.shape[0]-gShape[0], nGi, dtype=int)

    # ---- j- boundary ---- #
    iToBlk = b.bcs['j-'].toBlock
    toBlk  = blocks[iToBlk]
    if iToBlk > iBlk:
      for iStart in iRange:
        g = Genome(gSizei=gShape[0], gSizej=gShape[1])
        # genome part on this block
        gRng = (1, 1+gShape[1]-jShift, 1+gShape[0], 1+gShape[1])
        bRng = (iStart, 1, iStart+gShape[0], 1+jShift)
        g.add_map(iBlk, bRng, gRng)
        # genome part on connected block
        gRng = (1, 1, 1+gShape[0], 1+gShape[1]-jShift)
        bRng = (iStart,           1+toBlk.shape[1]-(gShape[1]-jShift), \
                iStart+gShape[0], 1+toBlk.shape[1])
        g.add_map(iToBlk, bRng, gRng)
        # save genome to list
        genomes[-1].append(g)

    # ---- j+ boundary ---- #
    iToBlk = b.bcs['j+'].toBlock
    toBlk  = blocks[iToBlk]
    if iToBlk > iBlk:
      for iStart in iRange:
        g = Genome(gSizei=gShape[0], gSizej=gShape[1])
        # genome part on this block
        gRng = (1, 1, 1+gShape[0], 1+gShape[1]-jShift)
        bRng = (iStart,           1+b.shape[1]-(gShape[1]-jShift), \
                iStart+gShape[0], 1+b.shape[1])
        g.add_map(iBlk, bRng, gRng)
        # genome part on connected block
        gRng = (1, 1+gShape[1]-jShift, 1+gShape[0], 1+gShape[1])
        bRng = (iStart, 1, iStart+gShape[0], 1+jShift)
        g.add_map(iToBlk, bRng, gRng)
        # save genome to list
        genomes[-1].append(g)




