# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import pyamg as amg
import h5py as h5
import matplotlib.pyplot as plt

# Select witdth for square domains 
All_Widths = [1,2,3,4,5,6,8]

# Select BC
# "1": Simple boundary conditions
# "2": Complex boundary conditinos
Select_BC = "1"

# Check: Bool to check correctness of the solutino
Check = True

for Width in All_Widths:
    
    # num samples
    nSample = 1
    # num grid cells along an edge
    nCell = 32
    # file name for the bc data
    FileName = "Sin_BC_" + Select_BC + "_AMG_" + str(nCell*Width)
    
    # space step, domain length is 1
    h = 1.0 / nCell
    
    # matrix
    A = amg.gallery.poisson((nCell*Width, nCell*Width), format='csr')
        
    # create datasets to contain solutons
    solFile = h5.File(FileName + '.h5', 'w')
    solFile.attrs['nSample']    = nSample
    solFile.attrs['domainSize'] = np.array([nCell*Width, nCell*Width])
    solData = solFile.create_dataset('LaplaceSolution', (nSample, Width*nCell+2, Width*nCell+2), \
                                      compression='gzip', compression_opts=9, \
                                      dtype='float64', chunks=True)
    
    # Define the grid for the solution
    p = np.zeros((Width*nCell+2, Width*nCell+2))
    
    #Define the boundary conditions
    x = np.linspace(h/2, Width-h/2, Width*nCell)
    if(Select_BC == "1"):
        BC = np.sin(2*np.pi*x/Width)
    elif(Select_BC == "2"):
        BC = np.sin(2*np.pi*x)
        
    # Define the RHS
    RHS = np.zeros((nCell*Width, nCell*Width))
    RHS[0,:] += 2*BC
    RHS[:,-1] += 2*BC
    RHS[-1,:] += np.flip(2*BC)
    RHS[:,0] += np.flip(2*BC)
    
    # transform rhs to a vector
    RHS = np.reshape(RHS, ((Width*nCell)*(Width*nCell)), order='C')
    
    # Modify the A Values
    for index in range(A.shape[0]):
        count = A[index].nnz
        index_4 = -1
        for index_i in A[index].indices:
            if(A[index,index_i]  ==4 ):
                index_4 = index_i
        if(count == 4):
            A[index, index_4] = 5
        elif(count == 3 ):
            A[index, index_4]  = 6
            
    print("Solving....")
    x = amg.solve(A, RHS, verb=False, tol=1e-14)
    
    #Save the values
    p[1:-1,1:-1] = np.reshape(x, (Width*nCell, Width*nCell), order='C') 
    p[0,1:-1] = 2*BC - p[1,1:-1]
    p[1:-1,-1] = 2*BC - p[1:-1, -2]
    p[-1,1:-1] = np.flip(2*BC) - p[-2,1:-1]
    p[1:-1,0] = np.flip(2*BC) - p[1:-1,1]
    
    
    #Check the residual and the accuracy of the BC
    if(Check):
        #Check RHS with discrete difference
        RHS = np.zeros((Width*nCell,Width*nCell))
        for i in range(Width*nCell):
            for j in range(Width*nCell):
                RHS[i,j] = (4*p[i+1,j+1] - p[i+1, j] - p[i,j+1] - p[i+2, j+1] - p[i+1, j+2])/(h**2)
        
        max_RHS = np.max(np.abs(RHS))
        print('Max Absolute Error RHS ',max_RHS )
       
        #Check the BC
        BC_approx_1 = 0.5*(p[0,1:-1] + p[1,1:-1] )
        BC_approx_2 = 0.5*(p[1:-1,-1] + p[1:-1,-2])
        BC_approx_3 = np.flip(0.5*(p[-1,1:-1] +p[-2,1:-1]))
        BC_approx_4 = np.flip(0.5*(p[1:-1,0] +p[1:-1,1]))
        max_BC1 = np.max(np.abs(BC_approx_1- BC))
        max_BC2 = np.max(np.abs(BC_approx_2- BC))
        max_BC3 = np.max(np.abs(BC_approx_3- BC))
        max_BC4 = np.max(np.abs(BC_approx_4- BC))
        max_BC = np.max([max_BC1, max_BC2, max_BC3, max_BC4])
        print('Max Absloute Error BC ', max_BC )
    
    solData[0,:,:] = p
    solFile.close()
                   





