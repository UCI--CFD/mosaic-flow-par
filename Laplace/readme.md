
The directory `./Laplace/models` includes all LPFC and FC models evaluated in this paper. The pre-trained models used in MF predictor are the single- and double-precision models with 18000 samples,
* lplc-32x2-64x2-96x5-128x5t18000_a0.001_d400_c400_s20
* lplc_fp64-32x2-64x2-96x5-128x5t18000_a0.001_d400_c400_s20

The directory `./Laplace/data/` includes the training data and the boundary conditions.

Next, we will demonstrate how to train a GFNet and run MF predictor with it for unseen domains. If not specified otherwise, we assume the scripts are run in the `./Laplace/src/` directory.

To train a GFNet from scratch, change to the `./Laplace/src/` diretory and run the script as follow:
```python
  python gfnet.py -name lplc -m 0 \
    -l 128 128 96 96 64 64 32 32 1 \
    -f ../data/Laplace32_21000.h5 \
    -t 2000 -s 20 \
    -alpha 0.001 \
    -d 100 -c 400 \
    -p 200 -lrmin 1e-7 \
    -a 20 -ae 1000 -e 1000
```
The command-line arguments:
* `-name lplc` specifies name of the model
* `-m 0` specifies type of the model, 0 is for FC and 1 is for LPFC,
* `-l 128 128 96 96 64 64 32 32 1` specifies the architecture for the fully connected layers
* `-f ../data/Laplace32_21000.h5` specifies the training data to use,
* `-t 8000 -s 20` specify the number of training samples and the batch size, repectively,
* `-alpha` is the coeficient timed to the PDE residual,
* `-d 100 -c 400` specify the number of data points and collocation points, respectively,
* `-p 200` specifies the patience for learning rate decay,
* `-e 1000` specifies the epochs for initial training,
* `-a 20 -ae 1000` specify that it will adapt collocation point times and train 1000 epochs each time.

The example command above will train the GFNet used in the 1st row in Table 1. The training save the model as TensorFlow checkpoint and output the log of loss and MAE. 

To see all the command-line arguments, run `python gfnet.py --help`.



To run MF predictor with a pre-trained model for an unseen square domain, run the following script:

```python
python Laplace_Unseen_Square_Domains_rep.py 
	-m "lplc-32x2-64x2-96x5-128x5t18000_a0.001_d400_c400_s20" 
    -d "../data/Sin_BC_AMG_64.h5" 
    -l 32 32 64 64 96 96 96 96 96 128 128 128 128 128 
    -DataType "float32" 
    -n "BC = sin(x/Width)" 
    -print True
```

The command-line arguments: 

* `-m --model` selects the pre-trained deep learning model that solves the flow for a square domain. 
* `-d --data` selects the file containing the ground truth for the square domain wished to solve. The format of the file must be ".h5" and have the following attributes: 
  	'nSample' = 1
  	'domainSize' = 32*Width + 2
  	'LaplaceSolution' = "True Values" 
  Note that samples satisfying this structure can be easily generated with the file `../data/data_AMG.py`.
* `-l --arch` sets teh architecture of the model
* `-DataType` selects  the data type "float32" or "float64". It must match the data type of the model.
* `-n --name` is the name of the test
* `-print --print`  prints the results to a file if true

The above command will print out the following on the screen if `-print` is  not set. 

```
Final resutls -----------
MAE =  0.001323677748996648
Elapsed Time =  5.132557153701782
```

If `-print` is set, it will print to file as follow:

```
BC = sin(x/Width) Area 4.0 Data_Type = float32  ------- 
MAE = [0.001323677748996648]
MSE = [1.6210569856484653e-06]
Times = [5.132557153701782]
```

For complex unseen geometries, the script `SC_Logo.py` use MF predictor to solve Laplace equation on the SC 2021 logo. It can be run similarly and output three plots for the ground truth, prediction, and error as in Figure 1, see `python SC_Logo.py --help` and `reproduce.md`.
