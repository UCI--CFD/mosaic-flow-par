import sys
sys.path.append('../src')
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
sys.path.append('./../src/')
sys.path.append(os.path.join(os.path.dirname(__file__), "../Genomes/"))
import numpy as np
import tensorflow as tf
from tensorflow import keras
import argparse
import matplotlib.pyplot as plt
from block import *
keras.backend.set_floatx('float32')

import torch as torch
import utils
from utilities3 import *
import NS_dataset as NS_D 
import pickle5 as pickle5
import Model as FNO

#%%

def read_vtk(blocks, vtkFile, verbose=False):
  assert len(blocks) == 3

  # create vtk reader
  reader = vtk.vtkUnstructuredGridReader()
  reader.SetFileName(vtkFile)
  reader.Update()
  vtkData = reader.GetOutput()
  # check # points
  nPnt = int(vtkData.GetNumberOfPoints() / 2)
  nPntTmp = blocks[0].nPoint + blocks[1].nPoint \
          - blocks[0].shape[0] + blocks[2].nPoint \
          - blocks[1].shape[1]

  assert(nPnt == nPntTmp)
  if verbose:
    print('Find {} points in {}'.format(nPnt, vtkFile))
    print('Block 0 has {} points.'.format(blocks[0].nPoint))
    print('Block 1 has {} points, sharing {} points with block 0.'\
         .format(blocks[1].nPoint, blocks[0].shape[0]))
    print('Block 2 has {} points, sharing {} points with block 1.'\
         .format(blocks[2].nPoint, blocks[1].shape[1]))

  # first get vtkPoints ojbect, then get points data array
  # this is a large array of (x, y, z), including dummay in Z
  pntxyz = vtk_to_numpy(vtkData.GetPoints().GetData())
  # Get points' data including (u,v) and p
  p = vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
  U = vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
  # block 0 coordinates and variables
  s,  e  = 0, blocks[0].nPoint
  ni, nj = blocks[0].shape[0], blocks[0].shape[1]
  blocks[0].coords[...]  = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
  blocks[0].vars[:,:,:2] = np.reshape(U[s:e,:2], (ni,nj,2))
  blocks[0].vars[:,:,2]  = np.reshape(p[s:e], (ni,nj))
  # block 1, copy the sharing points from block 0
  blocks[1].coords[:,0,:] = blocks[0].coords[:,-1,:]
  blocks[1].vars[:,0,:]   = blocks[0].vars[:,-1,:]
  # block 1 coordinates and variables, excluding shared border
  s = 2*blocks[0].nPoint
  e = s + blocks[1].nPoint - blocks[0].shape[0]
  ni, nj = blocks[1].shape[1], blocks[1].shape[1]-1
  blocks[1].coords[:,1:,:] = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
  blocks[1].vars[:,1:,:2]  = np.reshape(U[s:e,:2], (ni,nj,2))
  blocks[1].vars[:,1:,2]   = np.reshape(p[s:e], (ni,nj))
  # block 2, copy the sharing points from block 1
  blocks[2].coords[-1,:,:] = blocks[1].coords[0,:,:]
  blocks[2].vars[-1,:,:]   = blocks[1].vars[0,:,:]
  blocks[2].vars[-1,:,:]   = blocks[1].vars[0,:,:]
  # block 2 coordinates and variables, excluding shared border
  s = 2*(blocks[0].nPoint + blocks[1].nPoint - blocks[0].shape[0])
  e = s + blocks[2].nPoint - blocks[1].shape[1]
  ni, nj = blocks[2].shape[0]-1, blocks[2].shape[1]
  blocks[2].coords[:-1,:,:] = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
  blocks[2].vars[:-1,:,:2]   = np.reshape(U[s:e,:2], (ni,nj,2))
  blocks[2].vars[:-1,:, 2]   = np.reshape(p[s:e], (ni,nj))


def extract_bc(arr):
  nVar = 2
  bc = np.zeros(2*(arr.shape[0] + arr.shape[1] - 2)*nVar)

  s, e = 0, nVar*(arr.shape[1]-1)
  bc[s:e]  = arr[0,:-1,:nVar].flatten()
  s, e = e, e + nVar*(arr.shape[0]-1)
  bc[s:e]  = arr[:-1, -1, :nVar].flatten()
  s, e = e, e + nVar*(arr.shape[1]-1)
  bc[s:e]  = arr[-1, -1:0:-1, :nVar].flatten()
  s, e = e, e + nVar*(arr.shape[0]-1)
  bc[s:e]  = arr[-1:0:-1, 0, :nVar].flatten()

  return bc
 

#----------------------------------------------------------------------------------
# create flow and read solutions to blocks, cavity L 12g
# There are three blocks of identical sizes
#----------------------------------------------------------------------------------

# flow and original fine grid
vtkFile = (os.path.join(os.path.dirname(__file__), '../data/step_cavity.vtk'))
sizeFine = (129, 129)
blocksOriginal = [Block((129, 129)), Block((129, 129)),Block((129, 129))]
read_vtk(blocksOriginal, vtkFile, True)
# coarsen the grid sizeCoarse = (65, 65) 
blocks = [Block((65,65), nGenome=(2,2)),\
          Block((65,65), nGenome=(2,2)),\
          Block((65,65), nGenome=(2,2))]
# set up connections
blocks[0].add_connection('j+', 1, 'j-')
blocks[1].add_connection('j-', 0, 'j-')
blocks[1].add_connection('i-', 2, 'i+')
blocks[2].add_connection('i+', 1, 'i-')
# extract values from fine grid
for i in range(3):
  blocks[i].vars[...]    = blocksOriginal[i].vars[::2, ::2, :]
  blocks[i].coords[...]  = blocksOriginal[i].coords[::2, ::2, :]


#%% =============================================================================
# Load Model
# =============================================================================
Data_Type = 'float32'
tf.keras.backend.set_floatx(Data_Type)
Path = os.path.join(os.path.dirname(__file__), "../res/")
modelName = "FNO_NS_width-64_modes-16"
modelPath = Path + modelName

#Get the structure of the model from the name: 
modes = int(modelName[int(modelName.find("_modes-") + 7): ])
width = int(modelName[int(modelName.find("_width-") + 7):int(modelName.find("_modes-")) ])
Model_FNO = FNO.FNO2d_ns(modes, modes, width)
Model_FNO.load_state_dict(torch.load(modelPath, map_location=torch.device('cpu')))

Norm_File = os.path.join(os.path.dirname(__file__), '../res/normalizers.pkl')
with open(Norm_File, 'rb') as inp:
    x_normalizer = pickle5.load(inp)
    y_normalizer = pickle5.load(inp)


#%%----------------------------------------------------------------------------------
# Set the bc of the 12 basic genomes
#----------------------------------------------------------------------------------
nBcPnt  = 128
nVar    = 2
nGenome = 12
nEdgePnt= nBcPnt // 4
bcs     = np.zeros((nGenome, nBcPnt*nVar))
gVars   = np.zeros((nGenome, nEdgePnt+1, nEdgePnt+1, 3))

# there are 4 basic genomes per block
iG = 0
for m in range(3):
  for iStart in [0, nEdgePnt]:
    for jStart in [0, nEdgePnt]:
      iEnd = iStart + nEdgePnt + 1
      jEnd = jStart + nEdgePnt + 1
      gVars[iG,...] = blocks[m].vars[iStart:iEnd, jStart:jEnd, :]
      bcs[iG, :]    = extract_bc(gVars[iG,...])
      iG += 1

# genome's inner coorindates 
xy = np.zeros((nEdgePnt-1, nEdgePnt-1, 2))
h  = 0.5 / nEdgePnt
for i in range(nEdgePnt-1):
  for j in range(nEdgePnt-1):
    xy[i,j,:] = [(j+1)*h, (i+1)*h]
xy = xy.reshape((-1, 2))
print(xy.shape)

# prepare the input to the network
mae = np.zeros(3)
Initial = torch.Tensor(gVars[:, :,:, :2])
x_inputs = x_normalizer.encode(Initial).reshape(nGenome,33,33,2)
x_inputs[:,1:-1,1:-1, :] = 0


for iG in range(nGenome):
    x_input = x_inputs[iG:iG+1,:,:,:]
    uvp = y_normalizer.decode(Model_FNO(x_input).reshape(1,33,33, 3)).detach().numpy()
    err  = abs(gVars[iG:iG+1, 1:-1, 1:-1, :] - uvp[:, 1:-1, 1:-1, :])
    mae += np.sum(err, axis=(0,1,2))
mae = mae / (nEdgePnt-1) / (nEdgePnt-1) / nGenome

print("mae {:.2e} {:.2e} {:.2e}".format(mae[0], mae[1], mae[2]))
