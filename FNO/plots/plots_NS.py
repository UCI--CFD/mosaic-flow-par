#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 08:54:48 2021

@author: robertplanas
"""

import numpy as np
import pandas as pd
import os as os
from matplotlib import pyplot as plt


Files = ["Results_NS_Equation_width-128_modes-12.csv",
         "Results_NS_Equation_width-64_modes-12.csv",
         "Results_NS_Equation_width-64_modes-16.csv",
         "Results_NS_Equation_width-64_modes-18.csv"]
Title = [f'Model with width = {128} and modes = {12}',
         f'Model with width = {64} and modes = {12}',
         f'Model with width = {64} and modes = {16}',
         f'Model with width = {64} and modes = {18}']
for index, file in enumerate(Files): 
    dire = os.path.join(os.path.dirname(__file__), '../res/')
    file = dire + file
    df = pd.read_csv(file)
    Epochs = df['Epochs'].to_numpy()
    Train_Loss = df['Train Loss'].to_numpy()
    Val_Loss = df['Val Loss'].to_numpy()
    Test_U_MSE = df['Test U MSE'].to_numpy()
    Test_U_MAE = df['Test U MAE'].to_numpy()[-1]
    Test_V_MSE = df['Test V MSE'].to_numpy()
    Test_V_MAE = df['Test V MAE'].to_numpy()[-1]
    Test_P_MSE = df['Test P MSE'].to_numpy()
    Test_P_MAE = df['Test P MAE'].to_numpy()[-1]
    plt.figure()
    plt.plot(Epochs, Train_Loss, label = 'Training Loss')
    plt.plot(Epochs, Val_Loss, label = ' Validation Loss')
    plt.plot(Epochs, Test_U_MSE, label = ' Test U MSE')
    plt.plot(Epochs, Test_V_MSE, label = ' Test V MSE')
    plt.plot(Epochs, Test_P_MSE, label = ' Test P MSE')
    plt.title(Title[index] + f' MAE U = {Test_U_MAE:.2e}, MAE V = {Test_V_MAE:.2e}, MAE P = {Test_P_MAE:.2e}')
    plt.yscale('log')
    plt.legend()
    
    
    
    