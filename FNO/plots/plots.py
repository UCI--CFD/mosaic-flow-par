#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 08:54:48 2021

@author: robertplanas
"""

import numpy as np
import pandas as pd
import os as os
from matplotlib import pyplot as plt


Files = ['Results_Laplace_Equation_widht-32_modes-16.csv',
         "Results_Laplace_Equation_width-64_modes-16.csv",
         "Results_Laplace_Equation_width-64_modes-18.csv"]
Title = [f'Model with width = {32} and modes = {16}',
         f'Model with width = {64} and modes = {16}',
         f'Model with width = {64} and modes = {18}']
for index, file in enumerate(Files): 
    dire = os.path.join(os.path.dirname(__file__), '../res/')
    file = dire + file
    df = pd.read_csv(file)
    Epochs = df['Epochs'].to_numpy()
    Train_Loss = df['Train Loss'].to_numpy()
    Val_Loss = df['Val Loss'].to_numpy()
    Test_MSE = df['Test MSE'].to_numpy()
    Test_MAE = df['Test MAE'].to_numpy()[-1]
    plt.figure()
    plt.plot(Epochs, Train_Loss, label = 'Training Loss')
    plt.plot(Epochs, Val_Loss, label = ' Validation Loss')
    plt.plot(Epochs, Test_MSE, label = ' Test MSE')
    plt.title(Title[index] + f' MAE = {Test_MAE:.2e}')
    plt.yscale('log')
    plt.legend()
    
    
    
    