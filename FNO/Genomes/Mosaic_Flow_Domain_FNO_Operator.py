#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 10:51:22 2021

@author: robertplanas
"""

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', default="FNO_Laplace_width-64_modes-18",
                    help='Select a model')
parser.add_argument('-d', '--data', default="../data_AMG/Sin_BC_1_AMG_5x5.h5",
                    help='Select a data file')
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')
parser.add_argument('-n', '--name', default="BC = sin(x/Width)",
                    help='Name of the output file ')
parser.add_argument('-print', '--print', default=False,
                    help='Print to a file if true')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')
parser.add_argument('-it', '--iterations', type = int, default = 500,
                    help = 'Number of iterations')
parser.add_argument('-tol', '--tolerance', type = float, default = 1e-8,
                    help = 'MSE tolerance for the iterative approach')


args = parser.parse_args()


import sys
import os
import h5py as h5
import numpy as np
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
sys.path.append('./../src/')
import Model as FNO
import Genomes as G
import time
import Print_to_File as P2F
import torch as torch
import tensorflow as tf
import utils
from utilities3 import *




#%% =============================================================================
# Load Model
# =============================================================================
Data_Type = args.DataType
#tf.keras.backend.set_floatx(Data_Type)
Path = os.path.join(os.path.dirname(__file__), "../res/")
modelName = args.model
modelPath = Path + modelName

#Get the structure of the model from the name: 
modes = int(modelName[int(modelName.find("_modes-") + 7): ])
width = int(modelName[int(modelName.find("_width-") + 7):int(modelName.find("_modes-")) ])
Model_FNO = FNO.FNO2d(modes, modes, width)
Model_FNO.load_state_dict(torch.load(modelPath, map_location=torch.device('cpu')))
#Optimization_Error = 1.828e-04
Optimization_Error = 6.95e-05



#%% =============================================================================
# Load Encoder and decpders
# =============================================================================
DATA_PATH = '../../../CNN/data/Laplace_BC_AMG_ED_31.h5'
Data_File = os.path.join(os.path.dirname(__file__), DATA_PATH)
p, nSample, nCell = utils.Load_Data_File(Data_File, Scale= True)

#ntrain = 1000
#nval = 100
#ntest = 100
ntrain = 8000
nval = 1000
ntest = 1000
x=  (torch.from_numpy(p[:ntrain + nval + ntest, :,:].astype(np.float32))).double()
y= (torch.from_numpy(p[:ntrain + nval + ntest, :,:].astype(np.float32))).double()
x[:,1:-1,1:-1] = 0

x_train = x[:ntrain, :, : ]
y_train = y[:ntrain, :, : ]

x_test = x[ntrain:ntrain+ntest, :, :]
y_test = y[ntrain:ntrain+ntest, :, :]

x_val = x[ntrain + ntest:ntrain+ntest + nval, :, :]
y_val = y[ntrain + ntest:ntrain+ntest + nval, :, :]


x_normalizer = UnitGaussianNormalizer(x_train)
y_normalizer = UnitGaussianNormalizer(y_train)



#%% ===========================================================================
# Set up the experiments
# =============================================================================


Data = os.path.join(os.path.dirname(__file__), 'Mosaic_Flow_Logo_Data.h5')
psnFile = h5.File(Data, 'r')
Domain_Interior_Ones_Data = psnFile['Domain_Interior_Ones']
Domain_Borders_Data = psnFile['Domain_Borders']
Domain_Values_Data = psnFile['Domain_Values']
Domain_Ones_Data = psnFile['Domain_Ones']


Domain_Interior_Ones = np.zeros(Domain_Interior_Ones_Data.shape)
Domain_Interior_Ones[:,:] = Domain_Interior_Ones_Data
Domain_Borders = np.zeros(Domain_Borders_Data.shape)
Domain_Borders[:,:] = Domain_Borders_Data
Domain_Values = np.zeros(Domain_Values_Data.shape)
Domain_Values[:,:] = Domain_Values_Data
Domain_Ones = np.zeros(Domain_Ones_Data.shape)
Domain_Ones[:,:] = Domain_Ones_Data

Width = (Domain_Interior_Ones.shape[0] -1 )/ 31
Heigth = (Domain_Interior_Ones.shape[1] -1 )/ 31

# Normalize
U = np.squeeze(Domain_Values)
U_N = U
#%%
# Default max and min values used in training to normalize the inputs
MIN = -1.199990063905716
MAX = 1.1999822501681834


# Generate the genomes
Genomes_List= G.Generate_Genomes_Automatically(Width=Width, Height=Heigth, Genome_width=1,
                                               Points_Borders=31, Domain_Ones=Domain_Interior_Ones,
                                               DataType = tf.float32)

# Solve the domain
Solution, Time, MAE = G.Solve_Domain_Iterative(Genomes_List, Domain_Borders, np.nan_to_num(Domain_Ones),
             Model_FNO,  x_normalizer, y_normalizer,  Batch_Size = None, DataType = tf.float32, max_iter = args.iterations, min_tol = args.tolerance,
             Real_Solution = np.nan_to_num(U), Min = MIN, Max = MAX,)

Solution = Solution * Domain_Interior_Ones

Number_of_Pixels = 0 
for i in range(len(Solution[:,0])):
    for j in range(len(Solution[0,:])):
        if(not np.isnan(Solution[i, j])):
            Number_of_Pixels += 1
            
MAE = np.sum(np.abs(np.nan_to_num(U) - np.nan_to_num(Solution)))/Number_of_Pixels

Current_Area = "\n #FNO 80000 ------- MF Logo -------"
MF_Predictor_MAE = MAE

#%%
mae_test = 0

Basic_Genomes = Genomes_List[0]
for Genome in Basic_Genomes:
    U_Normalized = torch.Tensor(U_N[None,Genome.i0:Genome.i0+32, Genome.j0:Genome.j0 + 32])
    U_Normalized = x_normalizer.encode(U_Normalized) 
    U_Normalized = U_Normalized.detach().numpy()
    Input = Genome.Update_Inputs(tf.convert_to_tensor(U_N, dtype = tf.float32))
    Input = Input.numpy()[None,:,:]
    Input_Normalized = x_normalizer.encode(torch.Tensor(Input)).reshape(1,32,32,1)
    U_p = Model_FNO(Input_Normalized).reshape(1,32,32)
    U_p = y_normalizer.decode(U_p).detach().numpy()
    err  = abs(U[Genome.i0 +1:Genome.i0+31, Genome.j0+1:Genome.j0 + 31] - U_p[0, 1:-1, 1:-1])
    mae_test += np.sum(err, axis=(0,1))
mae_test = mae_test / (31-1) / (31-1) / len(Basic_Genomes)
        
  
Name_File = os.path.join(os.path.dirname(__file__), "Operator_Comparison_Logo")
P2F.Print_String_File(Name_File, Current_Area)
P2F.Print_vector_File(Name_File, [mae_test - Optimization_Error], "Generalization_Error")
P2F.Print_vector_File(Name_File, [mae_test], "MAE_Test")
P2F.Print_vector_File(Name_File, [MF_Predictor_MAE - mae_test], "Assembly_Error")
P2F.Print_vector_File(Name_File, [MF_Predictor_MAE], "MF_Predictor_Error")
P2F.Print_vector_File(Name_File, [Time], "Time")
 
    