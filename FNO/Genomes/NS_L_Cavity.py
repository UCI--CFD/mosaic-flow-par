#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 10:51:22 2021

@author: robertplanas
"""

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', default="FNO_NS_width-64_modes-16",
                    help='Select a model')
parser.add_argument('-d', '--data', default="../data_NS/cavity_step_12g_vtk/U_2008_1418.vtk",
                    help='Select a data file')
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')
parser.add_argument('-n', '--name', default="L_Cavity_Flow",
                    help='Name of the output file ')
parser.add_argument('-print', '--print', default=False,
                    help='Print to a file if true')
parser.add_argument('-plot', '--plot', default=False,
                    help='Plot to a file if true')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')
parser.add_argument('-it', '--iterations', type = int, default = 500,
                    help = 'Number of iterations')
parser.add_argument('-tol', '--tolerance', type = float, default = 3e-7,
                    help = 'MSE tolerance for the iterative approach')
parser.add_argument('-mos', '--mosaic', type = int, default = 2,
                    help = '1 or 2: If 2, more genomes are used')

args = parser.parse_args()


import sys
import os
import h5py as h5
import numpy as np
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
sys.path.append('./../src/')
import Model as FNO
import Genomes_NS as G
import Print_to_File as P2F
import Load_Data as LD
import time
import torch as torch
import tensorflow as tf
import utils
from utilities3 import *
import NS_dataset as NS_D 
import pickle5 as pickle5

#%% =============================================================================
# Load Model
# =============================================================================
Data_Type = args.DataType
#tf.keras.backend.set_floatx(Data_Type)
Path = os.path.join(os.path.dirname(__file__), "../res/")
modelName = args.model
modelPath = Path + modelName

#Get the structure of the model from the name: 
modes = int(modelName[int(modelName.find("_modes-") + 7): ])
width = int(modelName[int(modelName.find("_width-") + 7):int(modelName.find("_modes-")) ])
Model_FNO = FNO.FNO2d_ns(modes, modes, width)
Model_FNO.load_state_dict(torch.load(modelPath, map_location=torch.device('cpu')))


#%% =============================================================================
# Load Normalizers
# =============================================================================
Norm_File = os.path.join(os.path.dirname(__file__), '../res/normalizers.pkl')
with open(Norm_File, 'rb') as inp:
    x_normalizer = pickle5.load(inp)
    y_normalizer = pickle5.load(inp)


#%% =========================================================================
# Load data,
# =========================================================================

File = os.path.join(os.path.dirname(__file__), args.data)
U, Coords = LD.Load_L_Cavity(File)
Width = 2
Height = 2
Genome_width = 0.5

#%% =========================================================================
# Extract Domain information
# =========================================================================

# Domain Shape
Domain_Ones = np.ones_like(U)[:,:,0]
for i in range(np.shape(U)[0]):
    for j in range(np.shape(U)[1]):
        if(np.isnan(U[i,j,0])):
            Domain_Ones[i,j] = np.nan

Domain_Ones_Extra_Dims = np.concatenate((Domain_Ones[:,:,None],Domain_Ones[:,:,None],Domain_Ones[:,:,None]),axis = -1)
Domain_Borders_Ones = np.nan_to_num(G.Extract_Border_Domain(Domain_Ones_Extra_Dims))
Domain_Interior_Ones = np.nan_to_num(Domain_Ones_Extra_Dims - Domain_Borders_Ones)
# Domain only borders 
Domain_Borders = np.nan_to_num(G.Extract_Border_Domain(U, "Zero"))

#%% =========================================================================
# Generate Genomes
# =========================================================================

# Generate the genomes
Genomes_List= G.Generate_Genomes_Automatically(Width=Width, Height=Width, Genome_width=0.5,
                                               Points_Borders=32, Domain_Ones=Domain_Ones,
                                               DataType = tf.float32,  mosaic = 1)

#%% =========================================================================
# Solve iterative
# =========================================================================

# Solve the domain
Solution, Time, MAE = G.Solve_Domain_Iterative(Genomes_List, Domain_Borders, Domain_Interior_Ones,
             Model_FNO, x_normalizer, y_normalizer,  Batch_Size = None,  DataType = tf.float32, max_iter = args.iterations, min_tol = 1e-10,
             Real_Solution = np.nan_to_num(U))


#%% =========================================================================
# Solve iterative
# =========================================================================
Solution = Solution*Domain_Ones_Extra_Dims
MAE_u, MAE_v, MAE_p = MAE
Name_File = args.data[args.data.find("12g_vtk/") + 8 :args.data.find(".vtk")  ]
NAME = "FNO_NS_L_Cavity_" + Name_File

print("Solving L Cavity", Name_File)
print(f"MAE_u = {MAE_u:.3e}, MAE_v = {MAE_v:.3e}, MAE_p = {MAE_p:.3e},  Time = {Time:.1e}")

if(args.print):
    
    Name_File_txt = os.path.join(os.path.dirname(__file__), "NS_L_Cavity")
    P2F.Print_String_File(Name_File_txt, "Solving Single Cavity " +  Name_File)
    P2F.Print_vector_File(Name_File_txt, [MAE_u], "MAE_u")
    P2F.Print_vector_File(Name_File_txt, [MAE_v], "MAE_v")
    P2F.Print_vector_File(Name_File_txt, [MAE_p], "MAE_p")
    P2F.Print_vector_File(Name_File_txt, [Time], "Times")
 
if(args.plot):
    P2F.Plot_Cavity_L(Solution, n = 129, Width = 2, Name = Name_File )