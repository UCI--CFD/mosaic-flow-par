#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 11:07:04 2021

@author: robertplanas
"""

import numpy as np
import tensorflow as tf
import torch as torch
import time as time 
from matplotlib import pyplot as plt
#import tensorflow_probability as tfp


def Check_Surroundings(i,j,Domain):
    if(np.isnan(Domain[i,j])):
        return True
    if(i == 0 or j == 0 or i == len(Domain[:,0]) - 1 or j == len(Domain[0,:]) -1 ):
        return True
    
    if(np.isnan(Domain[i-1,j]) or np.isnan(Domain[i+1,j]) or np.isnan(Domain[i,j+1]) or np.isnan(Domain[i,j-1])):
        return True
 
    return False 

def Extract_Border_Domain(Domain, Nan_or_Zero = "NaN"):
    if Nan_or_Zero == "NaN":
        Value = np.nan
    else:
        Value = 0
    Size_x = Domain.shape[0]
    Size_y = Domain.shape[1]
    Dim = Domain.shape[2]
    Domain_Return = np.empty((Size_x, Size_y, Dim))
    for i in range(Size_x):
        for j in range(Size_y):
            if(Check_Surroundings(i,j, Domain[:,:,0])):
                Domain_Return[i,j, :] = Domain[i,j, :]
            else: 
                Domain_Return[i,j, :] = Value
    return Domain_Return 



def Loss_Batch_Optimize(Genome_List, Model, Solution, Batch_Size, DataType = tf.float32, Dimensions = 3):
    
    Genome_Batch = []
    Predictions_Batch = []
    Solution_Aux = tf.sparse.from_dense(tf.zeros_like(Solution))
    Auxiliar = tf.sparse.from_dense(tf.zeros_like(Solution))
    for index, Genome in enumerate(Genome_List):
        if(len(Genome_Batch) == 0 ):
            Genome_Batch = tf.expand_dims(Genome.Update_Inputs(Solution), axis = 0) 
        else:
            Genome_Batch = tf.concat([Genome_Batch, tf.expand_dims(Genome.Update_Inputs(Solution), axis = 0) ], axis = 0)
        if(Genome_Batch.shape[0] == Batch_Size):
            if(len(Predictions_Batch) == 0):
                Predictions_Batch = Model(Genome_Batch)
            else:
                Predictions_Batch = tf.concat([Predictions_Batch, Model(Genome_List)], axis = 0 ) 
            Genome_Batch = []
    for index, Genome in enumerate(Genome_List):
        
        Output = tf.slice(Predictions_Batch, [index, 0,0, 0 ], [1,Genome.Points_Border + 1 , Genome.Points_Border + 1, Dimensions])
        Solution_Aux, Auxiliar = Genome.Update_Solution(Output, Solution_Aux, Auxiliar)
    
    Solution_Aux = tf.sparse.to_dense(Solution_Aux)
    Auxiliar = tf.sparse.to_dense(Auxiliar)
    Updated_Domain = tf.cast(tf.cast(Auxiliar, bool), DataType)
    return tf.reduce_mean(tf.math.square(tf.math.multiply(Solution, Updated_Domain)  - tf.math.multiply_no_nan((Solution_Aux/Auxiliar), Updated_Domain)), axis = (0,1))
    


def Predict_Batch(Genome_List, Model, Solution, Batch_Size, x_normalizer, y_normalizer, DataType = tf.float32, Dimensions = 3):
    
    Genome_Batch = []
    Predictions_Batch = []
    Solution_Aux = tf.sparse.from_dense(tf.zeros_like(Solution))
    Auxiliar = tf.sparse.from_dense(tf.zeros_like(Solution))
    for index, Genome in enumerate(Genome_List):
        if(len(Genome_Batch) == 0 ):
            Genome_Batch = tf.expand_dims(Genome.Update_Inputs(Solution), axis = 0) 
        else:
            Genome_Batch = tf.concat([Genome_Batch, tf.expand_dims(Genome.Update_Inputs(Solution), axis = 0) ], axis = 0)
        if(Genome_Batch.shape[0] == Batch_Size):
            if(len(Predictions_Batch) == 0):
                Genome_Batch_numpy = Genome_Batch.numpy()[:,:,:,0:2]
                Genome_Batch_numpy[:,1:-1,1:-1, :] = 0
                Batch = Genome_Batch_numpy.shape[0]
                Genome_Batch_torch = torch.from_numpy(Genome_Batch_numpy)
                Genome_Batch_torch = x_normalizer.encode(Genome_Batch_torch).reshape(Batch, 33,33,2)
                Predictions_Batch_torch = Model(Genome_Batch_torch).reshape(Batch, 33,33, 3)
                Predictions_Batch_torch = y_normalizer.decode(Predictions_Batch_torch)
                Predictions_Batch_numpy = Predictions_Batch_torch.detach().numpy()
                Predictions_Batch = tf.convert_to_tensor(Predictions_Batch_numpy, dtype = DataType)

            
            else:
                Genome_Batch_numpy = Genome_List.numpy()[:,:,:,0:2]
                Genome_Batch_numpy[:,1:-1,1:-1] = 0
                Batch = Genome_Batch_numpy.shape[0]
                Genome_Batch_torch = torch.from_numpy(Genome_Batch_numpy)
                Genome_Batch_torch = x_normalizer.encode(Genome_Batch_torch).reshape(Batch, 33,33,2)
                Predictions_Batch_torch = Model(Genome_Batch_torch).reshape(Batch, 33,33,3)
                Predictions_Batch_torch = y_normalizer.decode(Predictions_Batch_torch)
                Predictions_Batch_numpy = Predictions_Batch_torch.detach().numpy()
                Predictions_Batch_Current = tf.convert_to_tensor(Predictions_Batch_numpy, dtype = DataType)
                
                Predictions_Batch = tf.concat([Predictions_Batch, Predictions_Batch_Current], axis = 0 ) 
            Genome_Batch = []
    for index, Genome in enumerate(Genome_List):
        Output = tf.slice(Predictions_Batch, [index, 0,0, 0], [1,Genome.Points_Border + 1 , Genome.Points_Border + 1, Dimensions])
        Solution_Aux, Auxiliar = Genome.Update_Solution(Output, Solution_Aux, Auxiliar)
    
    Solution_Aux = tf.sparse.to_dense(Solution_Aux)
    Auxiliar = tf.sparse.to_dense(Auxiliar)
   
    Updated_Domain = tf.cast(tf.cast(Auxiliar, bool), DataType)
    Non_Updated_Domain = tf.cast(tf.math.logical_not(tf.cast(Auxiliar, bool)), DataType)
    Solution_Batch = Non_Updated_Domain * Solution +  tf.math.multiply_no_nan((Solution_Aux/Auxiliar), Updated_Domain)
    '''
    plt.figure()
    plt.imshow(Solution)
    plt.title("Solution that will update the domain")
   
    plt.figure()
    plt.imshow(Non_Updated_Domain * Solution)
    plt.title("Non_Updated_Domain")
    plt.figure()
    plt.imshow(tf.math.multiply_no_nan((Solution_Aux/Auxiliar), Updated_Domain))
    plt.title("Updated_Domain")
    plt.figure()
    plt.imshow(Solution_Batch)
    plt.title("Solution_Batch")
    
    '''
    
    '''
    plt.figure()
    plt.imshow(Solution_Aux)
    plt.title("Solution_Sum")
    plt.figure()
    plt.imshow(Auxiliar)
    plt.title("Auxiliar")
    plt.figure()
    plt.imshow(Solution_Batch)
    plt.title("Solution Batch ")
    '''
    return Solution_Batch
    
    

class Genome:
    def __init__(self, width = 1, Points_Border = 31, x0_global = 0, y0_global = 0,
                 Domain_Width = 1, Domain_Height = 1, DataType = tf.float32, Dimensions = 3):
        
        # Define parameters
        self.width = width 
        self.Points_Border = Points_Border
        self.x0_global = x0_global
        self.y0_global = y0_global
        self.i0 = int(x0_global/width*Points_Border)
        self.j0 = int(y0_global/width*Points_Border)
        self.Domain_Width = Domain_Width
        self.Domain_Height = Domain_Height
        self.Domain_Width_Pixels = int(Domain_Width/width*Points_Border + 1)
        self.Domain_Height_Pixels = int(Domain_Height/width*Points_Border + 1)
        self.Input = tf.zeros((Points_Border+1, Points_Border+1, Dimensions))
        Border_Values = np.ones((Points_Border+1, Points_Border+1, Dimensions))
        Border_Values[1:-1,1:-1, :] = np.zeros((Points_Border-1, Points_Border-1, Dimensions))
        self.Border_Values = tf.convert_to_tensor(Border_Values, dtype = DataType )
        self.Indices = []
        self.Dimensions = Dimensions
        for i in range(self.i0, self.i0+Points_Border +1 ):
            for j in range(self.j0, self.j0+Points_Border +1 ):
                for k in range(self.Dimensions):
                    Indices =np.asarray([int(i),int(j),int(k)])[None, :]
                    if(len(self.Indices) == 0):
                        self.Indices = Indices
                    else:
                        self.Indices = np.vstack([self.Indices, Indices])   
                        
    def Update_Inputs(self, Solution):
        self.Input = tf.multiply(self.Border_Values, tf.slice(Solution, [self.i0, self.j0, 0],
                                                              [self.Points_Border + 1, self.Points_Border +1, self.Dimensions]))
        return self.Input
                
    
    def Update_Solution(self, Output, Solution, Auxiliar):
        # Output : Dense tensor of shape (Points_Border +1, Points_Border +1)
        # Solution: Sparse tensor
        # Auxiliar: Sparse tensor
        Output = tf.sparse.SparseTensor(self.Indices, tf.reshape(Output,((self.Points_Border +1)**2*self.Dimensions)), 
                                        dense_shape = [self.Domain_Width_Pixels, self.Domain_Height_Pixels, self.Dimensions])
        Ones = tf.sparse.SparseTensor(self.Indices, tf.ones(((self.Points_Border +1)**2*self.Dimensions,)), 
                                      dense_shape = [self.Domain_Width_Pixels, self.Domain_Height_Pixels, self.Dimensions])
        Solution = tf.sparse.add(Solution, Output)
        Auxiliar = tf.sparse.add(Auxiliar, Ones)    
        return Solution, Auxiliar
    
    
def Generate_Genomes_Automatically(Width, Height, Genome_width,  Points_Borders, Domain_Ones , DataType = tf.float32,
                                   Dimensions = 3, mosaic = 1):
    
    Genomes = []
    Number_of_Genomes = 0
    Number_Gnomes_x = int(Width/Genome_width)
    Number_Gnomes_y = int(Height/Genome_width)
    
    print("Placing Genomes...")
    #Set the lists of gnomes
    if(mosaic == 1):
        H_V = [[0,0], [0, Genome_width/2], [Genome_width/2, 0], [Genome_width/2, Genome_width/2]]
    else:
        H_V = [[0,0], [0, Genome_width/2], [Genome_width/2, 0], [Genome_width/2, Genome_width/2],
               [0, Genome_width/4], [Genome_width/4, 0 ],[Genome_width/4, Genome_width/4]]
    for index, element in enumerate(H_V):
        print(f"Placing Layer of Genomes {index +1 }/4")
        Layer_Genomes = []
        for x in range(Number_Gnomes_x):
            for y in range(Number_Gnomes_y):
                x0_global = x*Genome_width + element[0]
                y0_global = y*Genome_width + element[1]
                i0_global = int(x0_global/Genome_width*Points_Borders )
                j0_global = int(y0_global/Genome_width*Points_Borders )
                if(x0_global + Genome_width <= Width and y0_global + Genome_width <= Height and 
                   Domain_Ones[i0_global,j0_global] == 1 and 
                   Domain_Ones[i0_global + Points_Borders ,j0_global] == 1 and 
                   Domain_Ones[i0_global,j0_global + Points_Borders] == 1 and
                   Domain_Ones[i0_global + Points_Borders,j0_global + Points_Borders] == 1):
                    Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, 
                                            x0_global = x0_global, y0_global = y0_global,
                                            Domain_Width =Width, Domain_Height=Height, DataType = DataType,
                                            Dimensions = Dimensions)
                    Layer_Genomes.append(Genome_element)
        Number_of_Genomes += len(Layer_Genomes)
        Genomes.append(Layer_Genomes)
    
                
    print("A total of ", (Number_of_Genomes), " genomes are being used")
    return Genomes

def Reinitialize_Borders_Solution(Solution, Domain_Borders,Domain_Interior_Ones):
    return Domain_Borders + Domain_Interior_Ones*Solution

def Solve_Domain_Iterative(Genomes_List, Domain_Borders, Domain_Interior_Ones,
                 Model, x_normalizer, y_normalizer, Batch_Size = None, DataType = tf.float32, max_iter = 500, min_tol = 5e-8,
                 Real_Solution = None, Dimensions = 3, Initial_position = None):
    
    Start_Time = time.time()
    Iterations = 0
    Tolerance = np.inf
    Solution = tf.cast(tf.ones_like(Domain_Borders) * Domain_Borders, DataType)
    if(Initial_position is not None):
        Solution = tf.cast(Initial_position, DataType)
    #Convert inputs to tensors
    Domain_Borders = tf.cast(tf.convert_to_tensor(Domain_Borders), DataType)
    Domain_Interior_Ones = tf.cast(tf.convert_to_tensor(Domain_Interior_Ones), DataType)
    while(Iterations < max_iter):
        Solution_P = Solution
        for Layer_Genomes in Genomes_List:
            # Reinitialize the borders of the solution:
            Solution = Reinitialize_Borders_Solution(Solution, Domain_Borders,Domain_Interior_Ones)
            if(Batch_Size == None ):
                Batch_Size_L = len(Layer_Genomes)
            else:
                Batch_Size_L = Batch_Size
            Solution = Predict_Batch(Layer_Genomes, Model, Solution, Batch_Size_L,x_normalizer, y_normalizer, DataType = DataType,
                                     Dimensions = Dimensions)  
            
        Tolerance = tf.reduce_mean(tf.square(Solution_P - Solution))
        #print(f"Iteration: {Iterations}, Tolerance = {Tolerance:.2e}")
        MAE_u = np.mean(np.abs(Solution[:,:,0] - Real_Solution[:,:,0] ))
        MAE_v = np.mean(np.abs(Solution[:,:,1] - Real_Solution[:,:,1] ))
        print(f"Iteration: {Iterations}, Tolerance = {Tolerance:.2e}, MAE_u = {MAE_u:.3e}, MAE_v = {MAE_v:.3e}")
        if(Tolerance < min_tol):
            break
        Iterations += 1
    
    Solution = Reinitialize_Borders_Solution(Solution, Domain_Borders,Domain_Interior_Ones)
    
    Solution = Solution.numpy()
    MAE = None
    Time = time.time() - Start_Time
    if(Real_Solution is not None):
        MAE_u = np.mean(np.abs(Solution[:,:,0] - Real_Solution[:,:,0] ))
        MAE_v = np.mean(np.abs(Solution[:,:,1] - Real_Solution[:,:,1] ))
        MAE_p = None
        if(Dimensions == 3):
            MAE_p = np.mean(np.abs(Solution[:,:,2] - Real_Solution[:,:,2] ))
        MAE = (MAE_u, MAE_v, MAE_p)
    
    return Solution, Time, MAE 
    
        
        
class Optimize:
    def __init__(self, Genomes_List, Domain_Borders, Domain_Interior_Ones,
                 Model, Batch_Size = None, DataType = tf.float32, max_iter = 500, min_tol = 1e-8,
                 Real_Solution = None, Random_Initilize = False,
                 Compute_Intermediate = False, Dimensions = 3, Initial_position = None):
        
        # Parameters
        self.Genomes_List = Genomes_List
        self.Domain_Borders = Domain_Borders
        self.Domain_Interior_Ones = Domain_Interior_Ones
        self.Model = Model
        self.Batch_Size = Batch_Size
        self.DataType = DataType
        self.max_iter = max_iter
        self.min_tol = min_tol
        self.Real_Solution = Real_Solution
        self.Compute_Intermediate = Compute_Intermediate
        self.Dimensions = Dimensions
        # Initialize Domain 
        Shape_Domain = Domain_Interior_Ones.shape 
        self.Solution_Values = tf.reshape(Domain_Borders, (Shape_Domain[0]*Shape_Domain[1]*Shape_Domain[2],))
        if(Random_Initilize):
            self.Solution_Values += tf.reshape(np.random.rand(Shape_Domain[0],Shape_Domain[1], Shape_Domain[2] )*Domain_Interior_Ones, (Shape_Domain[0]*Shape_Domain[1]*Shape_Domain[2],))
        if(Initial_position is not None):
            self.Solution_Values = tf.reshape(Initial_position, (Shape_Domain[0]*Shape_Domain[1]*Shape_Domain[2],))
            
        self.Solution_Values = tf.cast(self.Solution_Values, dtype = self.DataType)
        
        
        # Dictionary of Losses
        self.History = {}
        # History of the total loss
        self.History['Loss'] = []
        self.History['Loss_u'] = []
        self.History['Loss_v'] = []
        if(self.Dimensions == 3):
            self.History['Loss_p'] = []
        if(self.Real_Solution is not None and self.Compute_Intermediate):
            self.History['MAE_u'] = []
            self.History['MAE_v'] = []
            if(self.Dimensions == 3):
                self.History['MAE_p'] = []
        self.Iteration = 0  
        
        self.Optimize()

        
    def loss(self, x):
        
        x = tf.reshape(x, (self.Domain_Interior_Ones.shape))
        Loss = 0
        Loss_u = 0
        Loss_v = 0 
        if(self.Dimensions == 3):
            Loss_p = 0
        for Layer_Genomes in self.Genomes_List:
            # Reinitialize the borders of the solution:
            x = Reinitialize_Borders_Solution(x, self.Domain_Borders,self.Domain_Interior_Ones)
            if(self.Batch_Size == None ):
                Batch_Size_L = len(Layer_Genomes)
            else:
                Batch_Size_L = self.Batch_Size
            loss_u , loss_v, loss_p = Loss_Batch_Optimize(Layer_Genomes, self.Model, x, Batch_Size_L, DataType = self.DataType)
            Loss_u += loss_u
            Loss_v += loss_v
            Loss_p += loss_p 
            Loss = Loss_u + Loss_v + 1000*Loss_p
            
            
        self.History['Loss'].append(Loss.numpy()) 
        self.History['Loss_u'].append(Loss_u.numpy()) 
        self.History['Loss_v'].append(Loss_v.numpy()) 
        if(self.Dimensions == 3):
            self.History['Loss_p'].append(Loss_p.numpy())  
        if(self.Real_Solution is not None and self.Compute_Intermediate):  
            Solution_ii = x
            MAE_u = np.mean(np.nan_to_num(np.abs(Solution_ii[:,:,0] - self.Real_Solution[:,:,0] )))
            MAE_v = np.mean(np.nan_to_num(np.abs(Solution_ii[:,:,1] - self.Real_Solution[:,:,1] )))
            self.History['MAE_u'].append(MAE_u)
            self.History['MAE_v'].append(MAE_v)
            if(self.Dimensions == 3):
                MAE_p = np.mean(np.nan_to_num(np.abs(Solution_ii[:,:,2] - self.Real_Solution[:,:,2] )))
                self.History['MAE_p'].append(MAE_p)
            

        if(np.mod(self.Iteration,10)==0):
            print(f"Iteration ={self.Iteration},  Loss = {Loss:.3e}, Loss_u = {Loss_u:.3e}, Loss_v = {Loss_v:.3e}, Loss_p = {Loss_p:.3e}")
            if(self.Real_Solution is not None and self.Compute_Intermediate ):
                print(f"\t MAE_u = {self.History['MAE_u'][-1]:.3e}, MAE_v = {self.History['MAE_v'][-1]:.3e}", end = '')
                if(self.Dimensions == 3):
                     print(f"\t MAE_p = {self.History['MAE_p'][-1]:.3e}")
        self.Iteration += 1
        return Loss

    def Loss_and_Graidents(self, x):
        return tfp.math.value_and_gradient(
            lambda x: self.loss(x),
            x)
    
        
    def Optimize(self): 
        
        self.Time = time.time()
        start = self.Solution_Values
        optim_results = tfp.optimizer.lbfgs_minimize(
              self.Loss_and_Graidents,
              initial_position=start,
              num_correction_pairs=10,
              x_tolerance=self.min_tol,
              max_iterations = self.max_iter)
        
        self.Results = np.reshape(optim_results.position.numpy(), self.Domain_Interior_Ones.shape)
        self.MAE_u = None
        self.MAE_v = None
        self.MAE_p = None
        self.Time = time.time() - self.Time

        if(self.Real_Solution is not None):
            self.MAE_u = np.mean(np.nan_to_num(np.abs(self.Results[:,:,0] - self.Real_Solution[:,:,0])))
            self.MAE_v = np.mean(np.nan_to_num(np.abs(self.Results[:,:,1] - self.Real_Solution[:,:,1] )))
            if(self.Dimensions == 3):
                self.MAE_p = np.mean(np.nan_to_num(np.abs(self.Results[:,:,2] - self.Real_Solution[:,:,2] )))        
    

            
            
            
        
        
        
