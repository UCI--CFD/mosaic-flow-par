#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 09:39:45 2021

@author: robertplanas
"""

import numpy as np
import h5py as h5 
import os as os
from pyDOE import lhs 

def Load_Data_File(Data_File, Scale= True):
    psnFile = h5.File(Data_File, 'r')
    nSample = psnFile.attrs['nSample']
    nCell = psnFile.attrs['domainSize'][0] 
    psnData = psnFile['LaplaceSolution']
    print('# Samples: {}'.format(nSample))
    print('# Cells: {}*{}'.format(nCell, nCell))
    print('data shape: ', end='')
    print(psnData.shape)
    
    # read the data into numpy array
    p = np.zeros(psnData.shape)
    p = psnData[:, :, :]
    pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
    print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))
    if(Scale):
        print('Scaling the data to [-1,1]...')
        p = (p -pMin)/(pMax - pMin)*2 -1    
    
    psnFile.close()
    return p, nSample, nCell

def Generate_Training_Sets(p, Samples,  Size_Grid, fV = 0.1, fT = 0.1, nSample = 10000, nCell= 32):
    if(Size_Grid > nCell):
        print(f"Grid size too large! Setting Grid Size to {nCell}")
        Size_Grid = nCell
    if(Samples > nSample):
        print(f"Too many samples for the data! Setting Samples to {nSample}")
        Samples = nSample
    
    Indices = (lhs(2, samples = Samples)*(nCell - Size_Grid)).astype(int)
    
    U = np.zeros((Samples, Size_Grid, Size_Grid))
    
    for index, index_pair in enumerate(Indices):
        U[index,:,: ] = p[index, index_pair[0]:index_pair[0] + Size_Grid, index_pair[1]:index_pair[1] + Size_Grid ]
        
    
    
    # split traning and validation
    nVal = int(fV*Samples)
    nTest = int(fT*Samples)
    nTrain = Samples - nVal - nTest
    
    print('{} solutions in training, {} in validation, {} in testing'.format(
        nTrain, nVal, nTest))
    print(f'Dataset shape = {U.shape}')
    
    U_Train = U[0:nTrain, :, :]
    U_Val = U[nTrain:nTrain + nVal , :, :]
    U_Test = U[nTrain + nVal:nTrain + nVal + nTest, :, :]
    
    return U_Train, U_Val, U_Test

def Generate_Training_Sets_Noise(p, Samples,  Size_Grid, Max_Noise_Variance, fV = 0.1, fT = 0.1, nSample = 10000, nCell= 32):
    if(Size_Grid > nCell):
        print(f"Grid size too large! Setting Grid Size to {nCell}")
        Size_Grid = nCell
    if(Samples > nSample):
        print(f"Too many samples for the data! Setting Samples to {nSample}")
        Samples = nSample
    
    Indices = (lhs(2, samples = Samples)*(nCell - Size_Grid)).astype(int)
    
    U = np.zeros((Samples, Size_Grid, Size_Grid))
    
    for index, index_pair in enumerate(Indices):
        U[index,:,: ] = p[index, index_pair[0]:index_pair[0] + Size_Grid, index_pair[1]:index_pair[1] + Size_Grid ]
        
    
    Noise = np.zeros((U.shape))
    Noise_Variances = np.random.uniform(low=0.0, high=Max_Noise_Variance, size=Samples)
    for index in range(Samples):
        Noise_Variance = Noise_Variances[index]
        mu, sigma = 0, np.sqrt(Noise_Variance) # mean and standard deviation
        s = np.random.normal(mu, sigma, U.shape[1]*U.shape[2])
        Noise[index, :, : ] = np.reshape(s, (U.shape[1],U.shape[2]))
        
    
    U_Noise = U + Noise
    
    # split traning and validation
    nVal = int(fV*Samples)
    nTest = int(fT*Samples)
    nTrain = Samples - nVal - nTest
    
    print('{} solutions in training, {} in validation, {} in testing'.format(
        nTrain, nVal, nTest))
    print(f'Dataset shape = {U.shape}')
    
    U_Train = U[0:nTrain, :, :]
    U_Val = U[nTrain:nTrain + nVal , :, :]
    U_Test = U[nTrain + nVal:nTrain + nVal + nTest, :, :]
    
    U_Train_Noise = U_Noise[0:nTrain, :, :]
    U_Val_Noise = U_Noise[nTrain:nTrain + nVal , :, :]
    U_Test_Noise = U_Noise[nTrain + nVal:nTrain + nVal + nTest, :, :]
    
    return U_Train, U_Val, U_Test, U_Train_Noise, U_Val_Noise, U_Test_Noise


def Load_Data_File_RHS(Data_File, Scale= True):
    psnFile = h5.File(Data_File, 'r')
    nSample = psnFile.attrs['nSample']
    nCell = psnFile.attrs['domainSize'][0] 
    psnData = psnFile['RHS']
    print('# Samples: {}'.format(nSample))
    print('# Cells: {}*{}'.format(nCell, nCell))
    print('data shape: ', end='')
    print(psnData.shape)
    
    # read the data into numpy array
    p = np.zeros(psnData.shape)
    p = psnData[:, :, :]
    pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
    print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))
    if(Scale):
        print('Scaling the data to [-1,1]...')
        p = (p -pMin)/(pMax - pMin)*2 -1    
    psnFile.close()
    return p, nSample, nCell  
    
def Generate_RHS_Grid(RHS_o, Size_Grid):
    nCell = RHS_o.shape[-1]
    Samples =  RHS_o.shape[0]
    if(Size_Grid > nCell):
        print(f"Grid size too large! Setting Grid Size to {nCell}")
        Size_Grid = nCell
    
    Indices = (lhs(2, samples = Samples)*(nCell - Size_Grid)).astype(int)
    
    RHS = np.zeros((Samples, Size_Grid, Size_Grid))
    
    for index, index_pair in enumerate(Indices):
        RHS[index,:,: ] = RHS_o[index, index_pair[0]:index_pair[0] + Size_Grid, index_pair[1]:index_pair[1] + Size_Grid ]
    
    return RHS

def Load_Data_File_Poisson(Data_File, Scale= True):
    psnFile = h5.File(Data_File, 'r')
    nSample = psnFile.attrs['nSample']
    nCell = psnFile.attrs['domainSize'][0] 
    RHSData = psnFile['RHS']
    UData = psnFile['U']
    print('# Samples: {}'.format(nSample))
    print('# Cells: {}*{}'.format(nCell, nCell))
    print('data shape: ', end='')
    print(UData.shape)
    
    # read the data into numpy array
    U = np.zeros(UData.shape)
    RHS = np.zeros(RHSData.shape)
    U = UData[:, :, :]
    RHS = RHSData[:,:,:]
    
    psnFile.close()
    
    return U, RHS, nSample, nCell  
    

def Generate_Training_Sets_Poisson(U, RHS, Samples, fV = 0.1, fT = 0.1, nSample = 10000):

    if(Samples > nSample):
        print(f"Too many samples for the data! Setting Samples to {nSample}")
        Samples = nSample
    
    nVal = int(fV*Samples)
    nTest = int(fT*Samples)
    nTrain = Samples - nVal - nTest
    
    print('{} solutions in training, {} in validation, {} in testing'.format(
        nTrain, nVal, nTest))
    print(f'Dataset shape = {U.shape}')
    
    U_Train = U[0:nTrain, :, :]
    U_Val = U[nTrain:nTrain + nVal , :, :]
    U_Test = U[nTrain + nVal:nTrain + nVal + nTest, :, :]
    
    RHS_Train = RHS[0:nTrain, :, :]
    RHS_Val = RHS[nTrain:nTrain + nVal , :, :]
    RHS_Test = RHS[nTrain + nVal:nTrain + nVal + nTest, :, :]
    
    return U_Train, U_Val, U_Test, RHS_Train, RHS_Val, RHS_Test