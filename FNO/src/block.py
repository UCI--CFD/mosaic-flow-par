import numpy as np
import matplotlib.pyplot as plt
import vtk
from vtk.util.numpy_support import vtk_to_numpy

BOUNDARY_PHYSICAL = 0
BOUNDARY_TO_BLOCK = 1

#----------------------------------------------------------------
# class for block's boundary
#----------------------------------------------------------------

class BlockBoundary:
  def __init__(self, shape, edge, bType = BOUNDARY_PHYSICAL):
    '''
    a boundary can be 'physical', not changed during iterations, or
    'to_block', mapped to an edge of another block
    edge has four poissbilities: i-, j-, i+, j+
    '''
    assert edge in ['i-', 'j-', 'i+', 'j+']
    self.bType   = bType
    self.toBlock = -1
    self.toEdge  = -1
    self.shape   = shape
    # data will be copied from one block's rng1 to self's rng0
    self.rng0    = None
    self.rng1    = None
    # base on the edge set data range0,
    # which is the source for future copy
    if edge   == 'i-':
      self.rng0 = [0, 1, 1, shape[1]+1]
    elif edge == 'j-':
      self.rng0 = [1, 0, shape[0]+1, 1]
    elif edge == 'i+':
      self.rng0 = [shape[0]+1, 1, shape[0]+2, shape[1]+1]
    elif edge == 'j+':
      self.rng0 = [1, shape[1]+1, shape[0]+1, shape[1]+2]


  def set_connection(self, toBlock, toEdge):
    self.bType   = BOUNDARY_TO_BLOCK
    self.toBlock = toBlock
    self.toEdge  = toEdge
    # will copy from rng1 to rng0
    if   toEdge == 'i-':
      self.rng1 = [1, 1, 2, self.shape[1]+1]
    elif toEdge == 'j-':
      self.rng1 = [1, 1, self.shape[0]+1, 2]
    elif toEdge == 'i+':
      self.rng1 = [self.shape[0], 1, self.shape[0]+1, self.shape[1]+1]
    elif toEdge == 'j+':
      self.rng1 = [1, self.shape[1], self.shape[0]+1, self.shape[1]+1]


#----------------------------------------------------------------
# class for block
#---------------------------------------------------------------

class Block:
  def __init__(self, shape, nVar=3, nCoord=2, nGenome=(2,8), Re=500):
    # data size
    self.shape  = shape
    self.nVar   = nVar
    self.nCoord = nCoord
    self.nPoint = self.shape[0] * self.shape[1]
    # allocate variables and coordinates
    self.coords = np.zeros((self.shape[0], self.shape[1], nCoord))
    self.vars   = np.zeros((self.shape[0], self.shape[1], nVar))
    # boundaries, physical and connected
    self.bcs    = {'i-' : BlockBoundary(shape, 'i-'), \
                   'j-' : BlockBoundary(shape, 'j-'), \
                   'i+' : BlockBoundary(shape, 'i+'), \
                   'j+' : BlockBoundary(shape, 'j+')  }
    # Reynolds
    self.Re       = Re
    # number of genomes, used in a different method
    self.nGenome = nGenome


  def add_connection(self, edge, toBlock, toEdge):
    self.bcs[edge].set_connection(toBlock, toEdge)


#----------------------------------------------------------------
# functions work on block objects
#----------------------------------------------------------------

def compute_residual(blocks0, blocks, verbose=False):
  '''
  return the difference between two block lists
  '''
  assert len(blocks0) == len(blocks)

  diffMax  = np.zeros((blocks[0].nVar))
  diffMean = np.zeros((blocks[0].nVar))
  totalPoints = 0
  for b in range(len(blocks)):
    for i in range(blocks[b].shape[0]):
      for j in range(blocks[b].shape[1]):
        error     = np.absolute( blocks[b].vars[i,j,:] \
                               - blocks0[b].vars[i,j,:])
        diffMean += error
        diffMax   = np.maximum(error, diffMax)
    totalPoints += blocks[b].nPoint
  diffMean = diffMean / totalPoints

  if (verbose):
    print('u residual L0 {:e} L1 {:e}'.format(diffMax[0], diffMean[0]))
    print('v residual L0 {:e} L1 {:e}'.format(diffMax[1], diffMean[1]))
    print('p residual L0 {:e} L1 {:e}'.format(diffMax[2], diffMean[2]))

  return diffMean


def plot_velocity_magnitude(blocks, umin=0.0, umax=1.0):
  # plot without frames
  fig = plt.figure(frameon=False, figsize=(5,5))
  ax = plt.Axes(fig, [0., 0., 1., 1.])
  ax.set_axis_off()
  fig.add_axes(ax)
  # plot by block to avoid misleading interpolation
  # use large number of levels to  smooth the plot
  for b in blocks:
    x = b.coords[:,:,0].flatten()
    y = b.coords[:,:,1].flatten()
    u = np.sqrt(b.vars[:,:,0]**2 + b.vars[:,:,1]**2).flatten()
    ax.tricontourf(x, y, u, levels = np.linspace(umin,umax,500))


def read_vtk(blocks, vtkFile, verbose=False):
  '''
  Read x, y, u, v, p from vtk file.
  For now, only supports single block:
  '''
  assert len(blocks) == 1
  # create vtk reader
  reader = vtk.vtkUnstructuredGridReader()
  reader.SetFileName(vtkFile)
  reader.Update()
  vtkData = reader.GetOutput()
  # check # points
  nPnt = int(vtkData.GetNumberOfPoints() / 2)
  if verbose:
    print('Find {} points in {}'.format(nPnt, vtkFile))
    print('Block 0 has {} points.'.format(blocks[0].nPoint))
  assert(nPnt == blocks[0].nPoint)

  # first get vtkPoints ojbect, then get points data array
  # this ib a large array of (x, y, z), including dummay in Z
  pntxyz = vtk_to_numpy(vtkData.GetPoints().GetData())
  # Get points' data including (u,v) and p
  p = vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
  U = vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
  # block 0 coordinates and variables
  s,  e  = 0, blocks[0].nPoint
  ni, nj = blocks[0].shape[0], blocks[0].shape[1]
  blocks[0].coords[...]  = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
  blocks[0].vars[:,:,:2] = np.reshape(U[s:e,:2], (ni,nj,2))
  blocks[0].vars[:,:,2]  = np.reshape(p[s:e], (ni,nj))


def rotate_by_90(blk, n90):
  assert n90 in [1, 2, 3]
  M, N = blk.shape[0], blk.shape[1]
  if n90 == 1:
    b = Block((N, M))
    for i in range(N):
      for j in range(M):
        b.coords[i,j,0] = -blk.coords[M-1-j,i,1]
        b.coords[i,j,1] =  blk.coords[M-1-j,i,0]
        b.vars[i,j,0]   = -blk.vars[M-1-j,i,1]
        b.vars[i,j,1]   =  blk.vars[M-1-j,i,0]
        b.vars[i,j,2]   =  blk.vars[M-1-j,i,2]
  elif n90 == 2:
    b = Block((M, N))
    for i in range(M):
      for j in range(N):
        b.coords[i,j,0] = -blk.coords[M-1-i,N-1-j,0]
        b.coords[i,j,1] = -blk.coords[M-1-i,N-1-j,1]
        b.vars[i,j,0]   = -blk.vars[M-1-i,N-1-j,0]
        b.vars[i,j,1]   = -blk.vars[M-1-i,N-1-j,1]
        b.vars[i,j,2]   =  blk.vars[M-1-i,N-1-j,2]
  elif n90 == 3:
    b = Block((N, M))
    for i in range(N):
      for j in range(M):
        b.coords[i,j,0] =  blk.coords[j,N-1-i,1]
        b.coords[i,j,1] = -blk.coords[j,N-1-i,0]
        b.vars[i,j,0]   =  blk.vars[j,N-1-i,1]
        b.vars[i,j,1]   = -blk.vars[j,N-1-i,0]
        b.vars[i,j,2]   =  blk.vars[j,N-1-i,2]
  # # shift in x is the original height in y
  # xShift = blk.coords[-1,0,1] - blk.coords[0,0,1]
  # b.coords[:,:,0] += xShift
  return b
