"""
@author: Zongyi Li
This file is the Fourier Neural Operator for 2D problem such as the Darcy Flow discussed in Section 5.2 in the [paper](https://arxiv.org/pdf/2010.08895.pdf).
"""

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter

import matplotlib.pyplot as plt

import operator
from functools import reduce
from functools import partial

from timeit import default_timer
from utilities3 import *

from Adam import Adam
import os
import utils as utils
torch.manual_seed(0)
np.random.seed(0)

import argparse
parser = argparse.ArgumentParser()
# Choose nCol and nDat
parser.add_argument('-modes', '--modes', default=12, type=int,
                    help='Number of modes')
parser.add_argument('-width', '--width', default=32, type=int,
                    help='Width size')
parser.add_argument('-nEpochs', '--nEpochs', default=2000, type=int,
                    help='Number of Epochs')
parser.add_argument('-use_cuda', '--use_cuda', default=False,
                    help='Use cuda devices')
parser.add_argument('-nTrain', '--nTrain', default=8000, type=int,
                    help='Number of Epochs')
parser.add_argument('-nVal', '--nVal', default=1000, type=int,
                    help='Number of Epochs')
parser.add_argument('-nTest', '--nTest', default=1000, type=int,
                    help='Number of Epochs')
args = parser.parse_args()

CUDA = args.use_cuda

################################################################
# fourier layer
################################################################


class SpectralConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, modes1, modes2):
        super(SpectralConv2d, self).__init__()

        """
        2D Fourier layer. It does FFT, linear transform, and Inverse FFT.    
        """

        self.in_channels = in_channels
        self.out_channels = out_channels
        # Number of Fourier modes to multiply, at most floor(N/2) + 1
        self.modes1 = modes1
        self.modes2 = modes2

        self.scale = (1 / (in_channels * out_channels))
        self.weights1 = nn.Parameter(
            self.scale * torch.rand(in_channels, out_channels, self.modes1, self.modes2, dtype=torch.cfloat))
        self.weights2 = nn.Parameter(
            self.scale * torch.rand(in_channels, out_channels, self.modes1, self.modes2, dtype=torch.cfloat))

    # Complex multiplication
    def compl_mul2d(self, input, weights):
        # (batch, in_channel, x,y ), (in_channel, out_channel, x,y) -> (batch, out_channel, x,y)
        return torch.einsum("bixy,ioxy->boxy", input, weights)

    def forward(self, x):
        batchsize = x.shape[0]
        # Compute Fourier coeffcients up to factor of e^(- something constant)
        x_ft = torch.fft.rfft2(x)

        # Multiply relevant Fourier modes
        out_ft = torch.zeros(batchsize, self.out_channels,  x.size(-2),
                             x.size(-1) // 2 + 1, dtype=torch.cfloat, device=x.device)
        out_ft[:, :, :self.modes1, :self.modes2] = \
            self.compl_mul2d(
                x_ft[:, :, :self.modes1, :self.modes2], self.weights1)
        out_ft[:, :, -self.modes1:, :self.modes2] = \
            self.compl_mul2d(
                x_ft[:, :, -self.modes1:, :self.modes2], self.weights2)

        # Return to physical space
        x = torch.fft.irfft2(out_ft, s=(x.size(-2), x.size(-1)))
        return x


class FNO2d(nn.Module):
    def __init__(self, modes1, modes2,  width):
        super(FNO2d, self).__init__()

        """
        The overall network. It contains 4 layers of the Fourier layer.
        1. Lift the input to the desire channel dimension by self.fc0 .
        2. 4 layers of the integral operators u' = (W + K)(u).
            W defined by self.w; K defined by self.conv .
        3. Project from the channel space to the output space by self.fc1 and self.fc2 .
        
        input: the solution of the coefficient function and locations (a(x, y), x, y)
        input shape: (batchsize, x=s, y=s, c=3)
        output: the solution 
        output shape: (batchsize, x=s, y=s, c=1)
        """

        self.modes1 = modes1
        self.modes2 = modes2
        self.width = width
        self.padding = 9  # pad the domain if input is non-periodic
        # input channel is 3: (a(x, y), x, y)
        self.fc0 = nn.Linear(3, self.width)

        self.conv0 = SpectralConv2d(
            self.width, self.width, self.modes1, self.modes2)
        self.conv1 = SpectralConv2d(
            self.width, self.width, self.modes1, self.modes2)
        self.conv2 = SpectralConv2d(
            self.width, self.width, self.modes1, self.modes2)
        self.conv3 = SpectralConv2d(
            self.width, self.width, self.modes1, self.modes2)
        self.w0 = nn.Conv2d(self.width, self.width, 1)
        self.w1 = nn.Conv2d(self.width, self.width, 1)
        self.w2 = nn.Conv2d(self.width, self.width, 1)
        self.w3 = nn.Conv2d(self.width, self.width, 1)

        self.fc1 = nn.Linear(self.width, 128)
        self.fc2 = nn.Linear(128, 1)

    def forward(self, x):
        grid = self.get_grid(x.shape, x.device)
        x = torch.cat((x, grid), dim=-1)
        x = self.fc0(x.float())
        x = x.permute(0, 3, 1, 2)
        x = F.pad(x, [0, self.padding, 0, self.padding])

        x1 = self.conv0(x)
        x2 = self.w0(x)
        x = x1 + x2
        x = F.gelu(x)

        x1 = self.conv1(x)
        x2 = self.w1(x)
        x = x1 + x2
        x = F.gelu(x)

        x1 = self.conv2(x)
        x2 = self.w2(x)
        x = x1 + x2
        x = F.gelu(x)

        x1 = self.conv3(x)
        x2 = self.w3(x)
        x = x1 + x2

        x = x[..., :-self.padding, :-self.padding]
        x = x.permute(0, 2, 3, 1)
        x = self.fc1(x)
        x = F.gelu(x)
        x = self.fc2(x)
        return x

    def get_grid(self, shape, device):
        batchsize, size_x, size_y = shape[0], shape[1], shape[2]
        gridx = torch.tensor(np.linspace(0, 1, size_x), dtype=torch.float)
        gridx = gridx.reshape(1, size_x, 1, 1).repeat(
            [batchsize, 1, size_y, 1])
        gridy = torch.tensor(np.linspace(0, 1, size_y), dtype=torch.float)
        gridy = gridy.reshape(1, 1, size_y, 1).repeat(
            [batchsize, size_x, 1, 1])
        return torch.cat((gridx, gridy), dim=-1).to(device)


################################################################
# configs
################################################################
DATA_PATH = '../../CNN/data/Laplace_BC_AMG_ED_31.h5'
Data_File = os.path.join(os.path.dirname(__file__), DATA_PATH)

ntrain = args.nTrain
nval = args.nVal
ntest = args.nTest


batch_size = 20
learning_rate = 0.001

epochs = args.nEpochs
step_size = 100
gamma = 0.5

modes = args.modes
width = args.width
s = 32

################################################################
# load data and data normalization
################################################################
p, nSample, nCell = utils.Load_Data_File(Data_File, Scale=True)
x = (torch.from_numpy(
    p[:ntrain + nval + ntest, :, :].astype(np.float32))).double()
y = (torch.from_numpy(
    p[:ntrain + nval + ntest, :, :].astype(np.float32))).double()
x[:, 1:-1, 1:-1] = 0

x_train = x[:ntrain, :, :]
y_train = y[:ntrain, :, :]

x_test = x[ntrain:ntrain + ntest, :, :]
y_test = y[ntrain:ntrain + ntest, :, :]

x_val = x[ntrain + ntest:ntrain + ntest + nval, :, :]
y_val = y[ntrain + ntest:ntrain + ntest + nval, :, :]


x_normalizer = UnitGaussianNormalizer(x_train)
x_train = x_normalizer.encode(x_train)
x_test = x_normalizer.encode(x_test)
x_val = x_normalizer.encode(x_val)

y_normalizer = UnitGaussianNormalizer(y_train)
y_train_no_normalized = y_train
y_train = y_normalizer.encode(y_train)

x_train = x_train.reshape(ntrain, s, s, 1)
x_test = x_test.reshape(ntest, s, s, 1)
x_val = x_val.reshape(ntest, s, s, 1)

train_loader = torch.utils.data.DataLoader(torch.utils.data.TensorDataset(
    x_train, y_train), batch_size=batch_size, shuffle=True)
test_loader = torch.utils.data.DataLoader(torch.utils.data.TensorDataset(
    x_test, y_test), batch_size=batch_size, shuffle=False)

################################################################
# training and evaluation
################################################################

if(CUDA):
    model = FNO2d(modes, modes, width).cuda()
else:
    model = FNO2d(modes, modes, width)
print(count_params(model))

#%%
optimizer = Adam(model.parameters(), lr=learning_rate, weight_decay=1e-4)
scheduler = torch.optim.lr_scheduler.StepLR(
    optimizer, step_size=step_size, gamma=gamma)

myloss = LpLoss(size_average=False)
Epochs = []
Train_Loss = []
Test_MSE = []
Test_MAE = []
Train_MAE = []
Val_Loss = []

if(CUDA):
    y_val.cuda()
    x_val.cuda()
    y_normalizer.cuda()
for ep in range(epochs):
    model.train()
    t1 = default_timer()
    train_l2 = 0
    for x, y in train_loader:
        if(CUDA):
            x, y = x.cuda(), y.cuda()

        optimizer.zero_grad()
        out = model(x).reshape(batch_size, s, s)
        out = y_normalizer.decode(out)
        y = y_normalizer.decode(y)

        loss = myloss(out.view(batch_size, -1), y.view(batch_size, -1))
        loss.backward()

        optimizer.step()
        train_l2 += loss.item()

    scheduler.step()

    model.eval()
    test_l2 = 0.0
    Train_MAE_item = 0
    Count = 0
    with torch.no_grad():
        for x, y in test_loader:
            if(CUDA):
                x, y = x.cuda(), y.cuda()

            out = model(x).reshape(batch_size, s, s)
            out = y_normalizer.decode(out)

            test_l2 += myloss(out.view(batch_size, -1),
                              y.view(batch_size, -1)).item()
            
            Train_MAE_item += torch.sum(torch.abs(y - out)/(s**2))
        
    Train_MAE_item = Train_MAE_item/ntrain
    train_l2 /= ntrain
    test_l2 /= ntest

    t2 = default_timer()

    if(CUDA):
        y_val_cuda = y_val.cuda()
        x_val_cuda = x_val.cuda()
    else:
        y_val_cuda = y_val
        x_val_cuda = x_val
        
    # Compute the test loss
    out_val = model(x_val_cuda).reshape(nval, s, s)
    out_val = y_normalizer.decode(out_val)        



    Val_MSE = torch.mean((y_val_cuda - out_val)**2)
    Val_MAE = torch.mean(torch.abs(y_val_cuda - out_val))

    Epochs.append(ep)
    Train_Loss.append(train_l2)
    Val_Loss.append(test_l2)
    Test_MSE.append(Val_MSE.item())
    Test_MAE.append(Val_MAE.item())
    Train_MAE.append(Train_MAE_item.item())

    print(ep, t2 - t1, train_l2, test_l2,Train_MAE_item.item() )
#%%
df = pd.DataFrame(list(zip(Epochs, Train_Loss, Val_Loss, Test_MSE, Test_MAE, Train_MAE)),
                  columns=['Epochs', 'Train Loss', 'Val Loss', 'Test MSE', 'Test MAE', 'Train_MAE'])

Path_res = os.path.join(os.path.dirname(__file__), 'res/')
Name = 'Results_Laplace_Equation' + f'_width-{args.width}' + f'_modes-{args.modes}' + f'_data-{args.nTrain}' +  '.csv'
df.to_csv(Path_res + Name)
PATH_MODEL = os.path.join(os.path.dirname(__file__), f'res/FNO_Laplace_width-{args.width}_modes-{args.modes}')
torch.save(model.state_dict(), PATH_MODEL)
